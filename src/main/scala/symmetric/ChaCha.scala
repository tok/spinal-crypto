
package symmetric

import spinal.core._
import spinal.lib._
import spinal.lib.fsm._

// TODO: Consistent rounds/doubleRounds parameters

case class ChaChaCmd() extends Bundle {
  val key = Bits(256 bits)
  val nonce = Bits(96 bits)
  val counter = UInt(32 bits)
  val doubleRounds = UInt(4 bits)
}

class ChaCha extends Component {
  val io = new Bundle {
    val cmd = slave Stream (ChaChaCmd())
    val rsp = master Stream (Bits(512 bits))
  }

  val permutation = new ChaChaPermutation

  val constants = List(0x61707865, 0x3320646e, 0x79622d32, 0x6b206574)

  val counter = UInt(32 bits)
  val keyAsBits = Bits(256 bits)
  val keyAsUInts = Vec(UInt(32 bits), 8)
  val nonceAsBits = Bits(96 bits)
  val nonceAsUInts = Vec(UInt(32 bits), 3)

  keyAsBits := io.cmd.key
  nonceAsBits := io.cmd.nonce
  counter := io.cmd.counter

  def bitsToUInts(b: Bits): Vec[UInt] = {
    val bytes = b.subdivideIn(8 bits).reverse
    Vec(Cat(bytes).subdivideIn(32 bits).map(_.asUInt))
  }

  keyAsUInts := bitsToUInts(keyAsBits)
  nonceAsUInts := bitsToUInts(nonceAsBits)

  val initialState = Vec(UInt(32 bits), 16) // This is the data fed into the permutation.
  val savedKey = Reg(Vec(UInt(32 bits), 16)) // Register for storing the key in order to add it afterwards.
  savedKey.foreach(_ init (0))

  // Assemble the initial state
  for (i <- 0 until 4) initialState(i + 0) := constants(i)
  for (i <- 0 until 8) initialState(i + 4) := keyAsUInts(i)
  initialState(12) := counter
  for (i <- 0 until 3) initialState(i + 13) := nonceAsUInts(i)

  val permutationCmd = ChaChaPermutationCmd()
  permutationCmd.doubleRounds := io.cmd.doubleRounds
  permutationCmd.data := initialState
  permutation.io.cmd << io.cmd.translateWith(permutationCmd)

  when(io.cmd.ready && !io.rsp.valid) {
    when(io.cmd.valid) {
      savedKey := initialState
    } otherwise {
      savedKey.foreach(_ := 0)
    }
  }

  // Add key to output of permutation.
  val outputAsUInts = (permutation.io.rsp.payload, savedKey).zipped.map(_ + _)

  // Output:
  // Interpret UInt vector as a bitstring
  val outputAsBits = outputAsUInts.flatMap(_.subdivideIn(8 bits)).reverse.asBits
  // ... and pipe it to the output stream.
  io.rsp << permutation.io.rsp.translateWith(outputAsBits)

}


/**
  * data:    The input data for the Chacha permutation.
  * rounds:  The number of double rounds to compute. Set this to 10 for ChaCha20
  */
case class ChaChaPermutationCmd() extends Bundle {
  val data = Vec(UInt(32 bits), 16)
  val doubleRounds = UInt(4 bits)
}

class ChaChaPermutation extends Component {

  val io = new Bundle {
    val cmd = slave Stream (ChaChaPermutationCmd())
    val rsp = master Stream (Vec(UInt(32 bits), 16))

  }

  io.cmd.ready := False
  io.rsp.valid := False
  io.rsp.payload.foreach(_ := 0)

  private val roundCore = new ChaChaRound
  private val roundCounter = Reg(UInt(4 bits)) init (0)
  private val chachaState = Reg(Vec(UInt(32 bits), 16))

  private val fsm = new StateMachine {

    roundCore.io.cmd.foreach(_.clearAll())

    val sIdle: State = new State with EntryPoint {
      whenIsActive {
        io.cmd.ready := True
        // Clear state register
        chachaState.foreach(_ := 0)

        when(io.cmd.valid) {
          goto(sComputing)
        }
      }
      onExit {
        // This takes `cmd.rounds` cycles
        roundCounter := io.cmd.doubleRounds - 1
        roundCore.io.cmd := io.cmd.data
        chachaState := roundCore.io.rsp

        //        // Takes rounds+1 cycles
        //        roundCounter := io.cmd.doubleRounds
        //        chachaState := io.cmd.data
      }
    }

    val sComputing: State = new State {
      whenIsActive {

        val nextCounter = roundCounter - 1
        roundCounter := nextCounter
        roundCore.io.cmd := chachaState
        chachaState := roundCore.io.rsp

        when(nextCounter === 0) {
          io.rsp.payload := chachaState
          goto(sValid)
        }
      }
    }

    val sValid: State = new State {
      // Computation is complete.
      whenIsActive {
        io.rsp.valid := True
        io.rsp.payload := chachaState
        when(io.rsp.ready) {
          goto(sIdle)
        }
      }
    }
  }

}

private class ChaChaRound extends Component {

  val io = new Bundle {
    val cmd = in Vec(UInt(32 bits), 16)
    val rsp = out Vec(UInt(32 bits), 16)
  }

  val half = Vec(UInt(32 bits), 16) // Intermediate state between two half rounds.

  def quarterRound(a_in: UInt, b_in: UInt, c_in: UInt, d_in: UInt,
                   a_out: UInt, b_out: UInt, c_out: UInt, d_out: UInt): Unit = {
    //    a += b;  d = rol(d^a, 16)
    //    c += d;  b = rol(b^c, 12)
    //    a += b;  d = rol(d^a, 8);
    //    c += d;  b = rol(b^c, 7);
    val a1 = a_in + b_in
    val d1 = (a1 ^ d_in).rotateLeft(16)
    val c1 = c_in + d1
    val b1 = (b_in ^ c1).rotateLeft(12)
    val a2 = a1 + b1
    val d2 = (a2 ^ d1).rotateLeft(8)
    val c2 = c1 + d2
    val b2 = (b1 ^ c2).rotateLeft(7)

    a_out := a2
    b_out := b2
    c_out := c2
    d_out := d2
  }

  val indices1 = List(
    (0, 4, 8, 12),
    (1, 5, 9, 13),
    (2, 6, 10, 14),
    (3, 7, 11, 15)
  )

  val indices2 = List(
    (0, 5, 10, 15),
    (1, 6, 11, 12),
    (2, 7, 8, 13),
    (3, 4, 9, 14)
  )

  for (idx <- indices1) {
    import io.cmd
    quarterRound(
      cmd(idx._1), cmd(idx._2), cmd(idx._3), cmd(idx._4),
      half(idx._1), half(idx._2), half(idx._3), half(idx._4)
    )
  }

  for (idx <- indices2) {
    import io.rsp
    quarterRound(
      half(idx._1), half(idx._2), half(idx._3), half(idx._4),
      rsp(idx._1), rsp(idx._2), rsp(idx._3), rsp(idx._4)
    )
  }

}

object ChaCha {
  def main(args: Array[String]) {
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new ChaCha)
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new ChaChaPermutation)
  }
}


class ChaChaTest extends Component {

  val chaCha = new ChaCha

  val cmd = new ChaChaCmd
  cmd.doubleRounds := U(10)
  cmd.counter := U(0)
  cmd.nonce := B"96'x000000090000004a00000000"
  cmd.key := B"256'x000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"

  val expectedOutput = B"512'x10f1e7e4d13b5915500fdd1fa32071c4c7d1f4c733c068030422aa9ac3d46c4ed2826446079faa0914c2d705d98b02a2b5129cd1de164eb9cbd083e8a2503c4e"

  chaCha.io.cmd.valid := True
  chaCha.io.cmd.payload := cmd
  chaCha.io.rsp.ready := True

  assert(
    assertion = !chaCha.io.rsp.valid || chaCha.io.rsp.payload === expectedOutput,
    message = "wrong output",
    severity = FAILURE
  )

}

