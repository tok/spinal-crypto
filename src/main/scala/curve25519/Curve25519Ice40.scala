package curve25519

import spinal.core._
import spinal.lib._
import spinal.lib.com.uart._

class Curve25519Ice40 extends Component {

  val io = new Bundle {

  }

  val leds = Bits (8 bits)

  // Create the output signals for led1 - led8.
  for (i <- 0 until leds.getWidth) {
    val led_i = out Bool()
    val idx = i + 1
    led_i.setName(f"led$idx%d")
    led_i := leds(i)
  }

  val core = new Curve25519Core

  val cmd = Curve25519Cmd()
  cmd.lambda := 1
  cmd.basePoint := 9
  cmd.secretKey := 0

  core.io.cmd.payload := cmd
  core.io.cmd.valid := True
  core.io.rsp.ready := True

  val result = RegNextWhen(core.io.rsp.payload, core.io.rsp.valid) init(0)

  leds := result(0 to 7).asBits
}

object Curve25519Ice40 {
  def main(args: Array[String]) {
    val conf = SpinalConfig(defaultClockDomainFrequency = FixedFrequency(12 MHz),
    targetDirectory = "rtl")
    conf.generateVerilog(new Curve25519Ice40)
  }
}