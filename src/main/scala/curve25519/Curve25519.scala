package curve25519

/**
 * Reference paper:
 * Koppermann P. et al. (2016), X25519 Hardware Implementation for Low-Latency Applications
 */

import spinal.core._
import spinal.lib._
import spinal.lib.fsm._

import arithmetic._
import spinal.lib.fsm.StateMachine

trait BinaryEngine256 {
  val inputWidth = 256
  val io = new Bundle {
    val enable = in Bool
    val cmd = in(BinaryOpCmd(inputWidth))
    val rsp = out UInt(inputWidth bits)
  }

  val latency: Int
}

/**
 * Adder modulo 2*p = 2**256 - 38
 */
class ModularAdder extends Component with BinaryEngine256 {
  val a = RegNextWhen(io.cmd.opA, io.enable)
  val b = RegNextWhen(io.cmd.opB, io.enable)

  val sum = RegNextWhen(a.resize(inputWidth + 1) + b, io.enable)
  val carry = sum.msb

  val reduced = RegNextWhen(sum.resize(inputWidth) + (carry ? U(38) | U(0)), io.enable)

  io.rsp := reduced

  val latency = LatencyAnalysis(io.cmd.opA, io.rsp).toInt
  assert(latency == 3)
}

/**
 * Subtract modulo 2*p = 2**256 - 38.
 * TODO: Iterative decomposition to save registers. Goal: 2 cycles. 1 for subtraction, 1 for reduction.
 */
class ModularSubtract() extends Component with BinaryEngine256 {
  val a = RegNextWhen(io.cmd.opA, io.enable)
  val b = RegNextWhen(io.cmd.opB, io.enable)

  val diff = RegNextWhen(a.resize(inputWidth + 1) - (b.resize(inputWidth + 1)), io.enable)
  val borrowBit = diff.msb

  val reduced = RegNextWhen(diff.resize(inputWidth) - (borrowBit ? U(38) | U(0)), io.enable)

  io.rsp := reduced

  val latency = LatencyAnalysis(io.cmd.opA, io.rsp).toInt
  assert(latency == 3)
}

/**
 * Reduction of a 512 bit integer by 2p = 2**256-38.
 * Requires 3 cycles.
 * TODO: Iterative decomposition to save registers.
 */
class ModularReducer512 extends Component {

  val io = new Bundle {
    val enable = in Bool
    val cmd = in UInt(512 bits)
    val rsp = out UInt(256 bits)
  }

  val List(low1, high1): List[UInt] = io.cmd.subdivideIn(2 slices).toList
  val val1 = RegNextWhen(low1.resize(256 + log2Up(38)) + high1 * U(38), io.enable)

  val (low2, high2) = (val1(0 until 256), val1(256 until 256 + log2Up(38)))
  assert(high2.getWidth == log2Up(38))
  val val2 = RegNextWhen(low2.resize(256 + 1) + high2 * 38, io.enable)

  val (low3, high3) = (val2(0 until 256), val2(256))
  val val3 = RegNextWhen(low3 + (high3 ? U(38) | 0), io.enable)

  io.rsp := val3

  val latency = LatencyAnalysis(io.cmd, io.rsp)
  assert(latency == 3)
}

/**
 * Reduction by p = 2**255-19 of a 256 bit integer.
 * TODO: Generalize together with other reducers.
 */
class ModularReducer256 extends Component {
  val io = new Bundle {
    val enable = in Bool
    val cmd = in UInt(256 bits)
    val rsp = out UInt(255 bits)
  }

  val (low, msb) = (io.cmd(0 to 254), io.cmd.msb)
  val reduced = RegNextWhen(low + (msb ? U(19) | 0), io.enable)

  io.rsp := reduced

  val latency = LatencyAnalysis(io.cmd, io.rsp).toInt
  assert(latency == 1)
}

/**
 * Multiplication modulo 2p = 2**256-38.
 */
class ModularMultiplier extends Component with BinaryEngine256 {

  val multiplier = new KaratsubaMultiplier(inputWidth)
  multiplier.io.enable := io.enable
  multiplier.io.cmd := io.cmd

  val reducer = new ModularReducer512
  reducer.io.enable := io.enable
  reducer.io.cmd := multiplier.io.rsp

  io.rsp := reducer.io.rsp

//  val modulus = PseudoMersennePrime(256, 38)
//  val multiplier = new MersennePrimeModMultiplier(modulus)
//
//  multiplier.io.enable := io.enable
//  multiplier.io.cmd := io.cmd
//  io.rsp := multiplier.io.rsp

  val latency = LatencyAnalysis(io.cmd.opA, io.rsp).toInt
}

object Curve25519Core {

  type ResourceMapping = (((UInt, UInt), BinaryEngine256), UInt)

  /** Extract all engines used in this mapping.
    *
    * @param resourceMapping
    * @return Unordered set of engines in this mapping.
    */
  def getEngines(resourceMapping: Seq[ResourceMapping]) = resourceMapping map { case ((_, engine), _) => engine } toSet

//  /**
//   * Create a state that configures the mux to route values in the registers
//   * to the right arithmetic engines.
//   * @param resourceMapping The mapping of input registers to engines and output registers.
//   * @return  The created FSM state.
//   */
//  class IssueState(resourceMapping: Seq[ResourceMapping])(implicit stateMachineAccessor: StateMachineAccessor)
//    extends State with StateCompletionTrait {
//    // Extract all used engines on this stage.
//    val engines = getEngines(resourceMapping)
//    assert(engines forall(_.latency == engines.head.latency), "Latencies must be all the same.")
//
//    whenIsActive {
//        for ((((opA, opB), engine), _) <- resourceMapping) {
//          engine.io.enable := True
//          engine.io.cmd.opA := opA
//          engine.io.cmd.opB := opB
//        }
//
//        doWhenCompletedTasks()
//    }
//  }

  class ExecuteState(resourceMapping: Seq[ResourceMapping])(implicit stateMachineAccessor: StateMachineAccessor)
    extends State with StateCompletionTrait {
    // Extract all used engines on this stage.
    val engines = getEngines(resourceMapping)
    assert(engines forall(_.latency == engines.head.latency), "Latencies must be all the same.")

    val latency = engines.head.latency

    val counter = Reg(UInt(log2Up(latency+1) bits)) init(0)

    onEntry {
      counter := latency
    }

    whenIsActive {

      for ((((opA, opB), engine), _) <- resourceMapping) {
        engine.io.enable := True
      }

      when(counter === latency) {
        // apply inputs
        for ((((opA, opB), engine), _) <- resourceMapping) {
          engine.io.cmd.opA := opA
          engine.io.cmd.opB := opB
        }
      }

      when(counter === 0) {
        // Write back
        for ((((_, _), engine), dest) <- resourceMapping) {
          dest := engine.io.rsp
        }

        doWhenCompletedTasks()
      }

      counter := counter - 1
    }
  }
//
//  /**
//   * Create a state that configures the mux read results from the arithmetic engines and
//   * write them back to the registers as configured in the `resourceMapping`.
//   * @param resourceMapping The mapping of input registers to engines and output registers.
//   * @return  The created FSM state.
//   */
//  class WriteBackState(resourceMapping: Seq[ResourceMapping])(implicit stateMachineAccessor: StateMachineAccessor)
//    extends State with StateCompletionTrait {
//    // Extract all used engines on this stage.
//    val engines = getEngines(resourceMapping)
//    assert(engines forall(_.latency == engines.head.latency), "Latencies must be all the same.")
//
//
//
//    whenIsActive {
//        for ((((_, _), engine), dest) <- resourceMapping) {
//          engine.io.enable := True
//          dest := engine.io.rsp
//        }
//
//        doWhenCompletedTasks()
//
//    }
//  }

  /**
   * Create issue and write back states for one stage.
   * @param resourceMapping
   * @param whenCompletedTask
   * @return
   */
  def createStageControlStates(resourceMapping: Seq[ResourceMapping], whenCompletedTask: => Unit)
                              (implicit stateMachineAccessor: StateMachineAccessor): State = {

//    val sWriteBack: State = new WriteBackState(resourceMapping) {
//      whenCompleted(whenCompletedTask)
//    }
//
//    val sIssue: State = new IssueState(resourceMapping) {
//      whenCompleted(goto(sWriteBack))
//    }
//    (sIssue, sWriteBack)

    new ExecuteState(resourceMapping) {
      whenCompleted(whenCompletedTask)
    }
  }

  def inversionFsm(z2: UInt, t: List[UInt],
                   mul0: BinaryEngine256, mul1: BinaryEngine256) = new StateMachine {

    // Calculates modular inversion of z2.
    // Pre: z2
    // Post: z2 <- z2^(-1)

    val List(t0, t1, t2, t3) = t
    require(!List(t0, t1, t2, t3).contains(z2))

    // TODO: Use this algorithm to save multiplications:
    /*
    input: z

    fe t0;
		fe t1;
		fe t2;
		fe t3;

		t0 = z.sq;
		t1 = t0.sq!2;

		t1 *= z;
		t0 *= t1;
		t2 = t0.sq;
		t1 *= t2;

		t2 = t1.sq!5;

		t1 *= t2;
		t2 = t1.sq!10;

		t2 *= t1;

		t3 = t2.sq!20;

		t2 *= t3;
		t2 = t2.sq!10;

		t1 *= t2;

		t2 = t1.sq!50;

		t2 *= t1;
		t3 = t2.sq!100;

		t2 *= t3;
		t2 = t2.sq!50;

		t1 *= t2;

		t1 = t1.sq!5;

		return t1 * t0;
     */

    val p = BigInt(2).pow(255) - 19
    val exponent = p - 2

    val counter = Reg(UInt(8 bits)) init (0)

    val sInit: State = new State with EntryPoint {
      whenIsActive {
        t0 := 1
        counter := 0
        goto(sLoop)
      }
    }

    val exponentBit = Bool
    switch(counter) {
      for(i <- 0 to 255) {
        if(!exponent.testBit(i)) {
          is(U(i)) {
            exponentBit := Bool(exponent.testBit(i))
          }
        }
      }
      default {
        exponentBit := True
      }
    }

    val sLoop: State = new State {


      whenIsActive {

        when(counter === 255) {
          z2 := t0
          exit()
        } otherwise {
          when(exponentBit) {
            goto(sSquareMult)
          } otherwise {
            goto(sSquare)
          }
        }

        counter := counter + 1
      }
    }

    // square
    val resourceMapping0 = List(
      (z2, z2) -> mul0 -> z2
    )

    // square and multiply
    val resourceMapping1 = List(
      (z2, z2) -> mul0 -> z2,
      (t0, z2) -> mul1 -> t0
    )

    val sSquare = createStageControlStates(resourceMapping0, goto(sLoop))
    val sSquareMult = createStageControlStates(resourceMapping1, goto(sLoop))

  }
}

case class Curve25519Cmd() extends Bundle {
  val secretKey = Bits(256 bits)
  val basePoint = UInt(256 bits)
  val lambda = UInt(256 bits)
}

class Curve25519Core extends Component {

  import Curve25519Core._

  val io = new Bundle {
    val cmd = slave Stream Curve25519Cmd()
    val rsp = master Stream UInt(256 bits)
  }

  io.cmd.ready := False
  io.rsp.valid := False
  io.rsp.payload.clearAll()

  val lambdaReg = Reg(UInt(256 bits)) init (0)

  val dataReg = Vec(Reg(UInt(256 bits)) init (0), 6)
  val secretKeyReg = Reg(Bits(256 bits)) init (0)

  // Variable-to-register mapping
  val x1 = Reg(UInt(256 bits))
  val x2 = dataReg(0)
  val z2 = dataReg(1)
  val x3 = dataReg(2)
  val z3 = dataReg(3)

  val sum = (0 to 1).map(_ => new ModularAdder)
  val sub = (0 to 1).map(_ => new ModularSubtract)
  val mul = (0 to 3).map(_ => new ModularMultiplier)

  val arithEngines = (sum ++ sub ++ mul)

  // Set default values of signals.
  arithEngines foreach (e => {
    import e.io.cmd.{opA, opB}
    import e.io.{cmd, rsp}
    opA.clearAll()
    opB.clearAll()

    e.io.enable := False

  })

  val reducer25519 = new ModularReducer256
  reducer25519.io.enable := False
  reducer25519.io.cmd.clearAll()

  /**
   * This FSM performs one step of the mongomery ladder.
   * Pre-conditions: dataReg = [x2, z2, x3, z3, (rest = don't care)]
   * Post-conditions: dataReg = [x2', z2', x3', z3', (rest = don't care)]
   */
  def ladderStepFsm() = new StateMachine {

    // Temporary variable to register mapping.

    val t1 = dataReg(0)
    val t2 = dataReg(1)
    val t3 = dataReg(2)
    val t4 = dataReg(3)

    val t6 = dataReg(0)
    val t7 = dataReg(4)
    val t9 = dataReg(2)
    val t8 = dataReg(3)

    val t5 = dataReg(1)
    val t10 = dataReg(2)
    val t11 = dataReg(3)

    val t13 = dataReg(5)
    val t12 = dataReg(3)
    val t14 = dataReg(5)


    // Data flow as in [1], Figure 1
    val resourceMapping = List[List[ResourceMapping]] (
      // stage 1
      List(
        // (opA, opB) -> engine -> destination
        (x2, z2) -> sum(0) -> t1,
        (z2, x2) -> sub(0) -> t2,
        (x3, z3) -> sum(1) -> t3,
        (z3, x3) -> sub(1) -> t4
      ),

      // stage 2
      List(
        (t1, t1) -> mul(0) -> t6,
        (t2, t2) -> mul(1) -> t7,
        (t2, t3) -> mul(2) -> t9,
        (t1, t4) -> mul(3) -> t8
      ),

      //stage 3
      List(
        (t6, t7) -> sub(0) -> t5,
        (t9, t8) -> sum(0) -> t10,
        (t8, t9) -> sub(1) -> t11
      ),
      
      // stage 4
      List(
        (U(121666), t5) -> mul(0) -> t13,
        (t6, t7) -> mul(1) -> x2,
        (t10, t10) -> mul(2) -> x3,
        (t11, t11) -> mul(3) -> t12
      ),

      // stage 5
      List(
        (t13, t7) -> sum(0) -> t14
      ),

      // stage 6
      List (
        (t14, t5) -> mul(0) -> z2,
        (lambdaReg, x3) -> mul(2) -> x3,
        (x1, t12) -> mul(3) -> z3
      )
    )


    val sExec1: State = new ExecuteState(resourceMapping(0)) with EntryPoint {
      whenCompleted(goto(sExec2))
    }

    val sExec2 = createStageControlStates(resourceMapping(1), goto(sExec3))
    val sExec3 = createStageControlStates(resourceMapping(2), goto(sExec4))
    val sExec4 = createStageControlStates(resourceMapping(3), goto(sExec5))
    val sExec5 = createStageControlStates(resourceMapping(4), goto(sExec6))

    val sExec6: State = new ExecuteState(resourceMapping(5)) {
      whenCompleted(exit())
    }
  }


  val mainFsm = new StateMachine {

    val ladderCounter = Counter(8 bits)

    val sInit: State = new State with EntryPoint {

      // Enforce requirements on secret key.
      val clampedKey = B"01" ## io.cmd.secretKey(253 downto 3) ## B"000"


      whenIsActive {

        io.cmd.ready := True
        secretKeyReg.clearAll()
        lambdaReg.clearAll()
        dataReg.foreach(_.clearAll())
        x1.clearAll()

        when(io.cmd.valid) {
          secretKeyReg := clampedKey

          val basePoint = io.cmd.basePoint
          x1 := basePoint

          lambdaReg := io.cmd.lambda

          goto(sLambdaMul)
        }
      }


      val lambdaMulMapping = List((x1, lambdaReg) -> mul(0) -> x1)

      val sLambdaMul: State = new ExecuteState(lambdaMulMapping) {
        whenCompleted(goto(sSetup))
      }

      val sSetup: State = new State {

        whenIsActive {
          /**
           * x1 <- lambda*x1
           * x3 <- lambda*x1
           * x2 <- lambda
           * z2 <- 0
           * z3 <- lambda
           */

          x3 := x1
          x2 := lambdaReg
          z2.clearAll()
          z3 := lambdaReg

          ladderCounter.clear()

          goto(sCSwap1)
        }
      }

      val c = secretKeyReg(255 downto 254).xorR
      val swapMask = Vec((0 to 255) map (_ => c)).asBits.asUInt

      val sCSwap1: State = new State {
        whenIsActive {

          x2 := x2 ^ (x3 & swapMask)
          z2 := z2 ^ (z3 & swapMask)

          goto(sCSwap2)
        }
      }

      val sCSwap2: State = new State {
        whenIsActive {

          x3 := x3 ^ (x2 & swapMask)
          z3 := z3 ^ (z2 & swapMask)

          goto(sCSwap3)
        }
      }

      val sCSwap3: State = new State {
        whenIsActive {

          x2 := x2 ^ (x3 & swapMask)
          z2 := z2 ^ (z3 & swapMask)

          goto(sLadderStep)
        }
      }

      val sLadderStep: State = new StateFsm(ladderStepFsm()) {
        whenCompleted {

          ladderCounter.increment()
          secretKeyReg := (secretKeyReg << 1).resized

          when(ladderCounter.valueNext === 255) {
            goto(sInversion)
          } otherwise {
            goto(sCSwap1)
          }
        }
      }


      // X2 must not be touched.
      val registersForInversion = List(dataReg(2), dataReg(3), dataReg(4), dataReg(5))
      require(!registersForInversion.contains(x2))

      val sInversion: State = new StateFsm(inversionFsm(z2, registersForInversion, mul(0), mul(1))) {
        // Z2 <- Z2^(-1)
        whenCompleted {
          goto(sX2MulZ2)
        }
      }

      // Xq = X2*Z2
      val x2MulZ2Mapping = List((x2, z2) -> mul(0) -> x1)
      val sX2MulZ2: State = new ExecuteState(x2MulZ2Mapping) {
        whenCompleted {
          goto(sFinalReduction)
        }
      }

      val sFinalReduction: State = new State {
        // Start reduction by p = 2^255 - 19.
        val latency = reducer25519.latency
        val counter = Reg(UInt(log2Up(latency + 1) bits)) init(0)
        onEntry {
          counter := latency
        }
        whenIsActive {

          reducer25519.io.enable := True

          when(counter === latency) {
            reducer25519.io.cmd := x1
          }

          when(counter === 0) {
            x1 := reducer25519.io.rsp.resized
            goto(sDone)
          }

          counter := counter - 1
        }
      }

//      val sFinalReductionIssue: State = new State {
//        // Start reduction by p = 2^255 - 19.
//        whenIsActive {
//          when(reducer25519.io.cmd.ready) {
//            reducer25519.io.cmd.valid := True
//            reducer25519.io.cmd.payload := x1
//            goto(sFinalReductionWriteBack)
//          }
//        }
//      }
//
//      val sFinalReductionWriteBack: State = new State {
//        // Read result of reduction by p = 2^255 - 19.
//        whenIsActive {
//          when(reducer25519.io.rsp.valid) {
//            reducer25519.io.rsp.ready := True
//            x1 := reducer25519.io.rsp.payload.resized
//            goto(sDone)
//          }
//        }
//      }

      val sDone: State = new State {
        whenIsActive {
          io.rsp.payload := x1
          io.rsp.valid := True

          when(io.rsp.ready) {
            goto(sInit)
          }
        }
      }

    }
  }
}

class Curve25519InversionTest extends Component {

  import Curve25519Core._

  val io = new Bundle {
    val cmd = slave Stream UInt(256 bits)
    val rsp = master Stream UInt(256 bits)
  }

  io.cmd.ready := False
  io.rsp.valid := False
  io.rsp.payload := 0

  val z, t0, t1, t2, t3 = Reg(UInt(256 bits))
  val regs = List(z, t0, t1, t2, t3)

  val mul0, mul1 = new ModularMultiplier
  val arithEngines = List(mul0, mul1)

  arithEngines foreach (_.io.enable := False)
  arithEngines foreach (_.io.cmd.opA.clearAll())
  arithEngines foreach (_.io.cmd.opB.clearAll())

  val fsm = new StateMachine {

    val sIdle: State = new State with EntryPoint {
      whenIsActive {
        io.cmd.ready := True
        regs.foreach(_.clearAll())
        when(io.cmd.valid) {
          z := io.cmd.payload
          goto(sInversion)
        }
      }
    }

    val sInversion: State = new StateFsm(inversionFsm(z, List(t0, t1, t2, t3), mul0, mul1)) {
      whenCompleted {
        goto(sDone)
      }
    }

    val sDone: State = new State {
      whenIsActive {
        io.rsp.payload := z
        io.rsp.valid := True
        when(io.rsp.ready) {
          goto(sIdle)
        }
      }
    }

  }
}

object EmitComponents {
  def main(args: Array[String]) {
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new ModularReducer512())
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new ModularMultiplier())
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new Curve25519Core())
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new Curve25519InversionTest())
  }
}