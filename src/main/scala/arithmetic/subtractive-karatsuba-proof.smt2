; Proove correctness of subtractive Karatsuba multiplication algorithm.

(set-logic ALL)

(declare-const x (_ BitVec 256))
(declare-const y (_ BitVec 256))

(declare-const xl (_ BitVec 128))
(declare-const xh (_ BitVec 128))
(declare-const yl (_ BitVec 128))
(declare-const yh (_ BitVec 128))

(assert (= xl ((_ extract 0 127) x )))
