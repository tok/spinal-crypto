package arithmetic

import spinal.core._
import spinal.lib._

/**
  * Naive multiplier.
  *
  * @param inputWidth Bit width of input operands.
  */
class BasicMultiplier(inputWidth: Int) extends Component {

  val config = MultiplierConfig(inputWidth, inputWidth)

  val io = new Bundle {
    val cmd = slave Stream BinaryOpCmd(inputWidth)
    val rsp = master Stream UInt(config.resultWidth bits)
  }

  io.rsp << io.cmd.translateWith(io.cmd.opA * io.cmd.opB)
}

/**
  * Subtractive Karatsuba multiplier.
  * TODO: Pipelining / Iterative decomposition (7 cycles).
  *
  * @param inputWidth Bit width of input operands.
  */
class KaratsubaMultiplier(inputWidth: Int, recursionDepth: Int = 0) extends Component {

  require(inputWidth % 2 == 0, "Uneven bit widths are not supported.")
  val n = inputWidth / 2

  val config = MultiplierConfig(inputWidth, inputWidth)
  val subMultConfig = MultiplierConfig(n, n)

  val io = new Bundle {
    val enable = in Bool
    val cmd = in(BinaryOpCmd(inputWidth))
    val rsp = out UInt(config.resultWidth bits)
  }

  /**
    * Calculate absolute difference.
    *
    * @param a Operand a
    * @param b Operand b
    * @return Returns a tuple (borrow bit, |a-b|)
    */
  def absDiff(a: UInt, b: UInt): (Bool, UInt) = {
    require(a.getWidth == b.getWidth)
    val width = a.getWidth
    val aExt = a.resize(width + 1)
    val bExt = b.resize(width + 1)
    val diff = aExt - bExt
    val borrowBit = diff.msb // Set if b > a

    // Create a mask of all 0s or 1s.
    val mask = Cat((0 until width).map(_ => borrowBit))

    val diffResized = diff.resize(width)
    (borrowBit, borrowBit ? (0-diffResized) | diffResized)
  }

  // TODO: Spinal bug. io_cmd_payload is not kept when below line is uncommented.
  //val cmd = io.cmd //.stage()

  // Split operands into high and low parts.
  val List(al, ah) = io.cmd.opA.subdivideIn(2 slices).toList
  val List(bl, bh) = io.cmd.opB.subdivideIn(2 slices).toList

  val (borrowA, absDiff_a) = absDiff(al, ah)
  val (borrowB, absDiff_b) = absDiff(bl, bh)

  val borrowXor = RegNextWhen(borrowA ^ borrowB, io.enable)

  // Perform sub-multiplications.

  val l = RegNextWhen(al * bl, io.enable)
  val m = RegNextWhen(absDiff_a * absDiff_b, io.enable)
  val h = RegNextWhen(ah * bh, io.enable)

  val m_resized: UInt = m.resize(3 * n)

  val m_hat = borrowXor ? m_resized | (0 - m_resized)

  val w = RegNextWhen((h << 2 * n) + ((m_hat + h + l) << n) + l, io.enable)

  io.rsp := w

  val latency = LatencyAnalysis(io.cmd.opA, io.rsp)
  println(s"Karatsuba Multiplier Latency = ${latency}")
}

object KaratsubaMultiplier {
  def main(args: Array[String]) {
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new KaratsubaMultiplier(32))

  }
}