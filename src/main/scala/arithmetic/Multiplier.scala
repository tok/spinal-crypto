package arithmetic

import spinal.core._
import spinal.lib._

/**
  * Defines the input bit width of a m*n bit multiplier.
  * @param m
  * @param n
  */
case class MultiplierConfig(m: Int, n: Int) {
  require(m > 0 && n > 0)

  val resultWidth = m + n
}

//case class MultiplierCmd(config: MultiplierConfig) extends BinaryOpCmd(config.m, config.n)