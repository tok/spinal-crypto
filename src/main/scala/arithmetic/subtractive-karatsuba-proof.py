#!/bin/env python3

from pysmt.shortcuts import *
from pysmt.typing import BVType

BV256 = BVType(256)
BV128 = BVType(128)

x = Symbol('x', BV256)
y = Symbol('y', BV256)

xl = Symbol('xl', BV128)
xh = Symbol('xh', BV128)
xl = x[0:127]
xh = x[128:]

yl = Symbol('yl', BV128)
yh = Symbol('yh', BV128)
yl = y[0:127]
yh = y[128:]

l = xl * yl
h = xh * yh

def abs(bv):
    w = bv.bv_width()
    sign = bv[w-1]
    #signExt = BVZExt(sign, w-1)
    #mask = BVZero(w) - signExt
    #return BVXor(bv, mask) + signExt
    return Ite(Equals(sign, BVZero(1)), bv, BVNeg(bv))

m = BVMul(abs(xl-xh), abs(yl-yh))
m_hat = Ite(Equals(m, BVMul(xl-xh, yl-yh)), -m, m)
w = BVConcat(h, BVConcat(l+h-m_hat, l))

naiveMult = BVMul(BVZExt(x, 128), BVZExt(y, 128))

