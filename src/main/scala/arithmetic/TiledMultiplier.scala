package arithmetic

import spinal.core._

class TiledMultiplier(config: MultiplierConfig, subMultConfig: MultiplierConfig) extends Component {

  val io = new Bundle {
    val enable = in Bool
    val cmd = in (BinaryOpCmd(config.m, config.n))
    val rsp = out UInt(config.m + config.n bits)
  }

  import io.cmd.{opA, opB}
  private def roundUp(mod: Int, x: Int): Int = x%mod match {
    case 0 => x
    case _ => x + mod - x%mod
  }
  private def subdivide(x: UInt, slice: Int): IndexedSeq[UInt] = {
    val w = x.getWidth
    (0 until w by slice) map (start => x(start until List(start+slice, w).min))
  }

  val A = opA.resize(roundUp(subMultConfig.m, opA.getWidth)).subdivideIn(subMultConfig.m bits)
  val B = opB.resize(roundUp(subMultConfig.n, opB.getWidth)).subdivideIn(subMultConfig.n bits)

  val subMultipliers = A map (a => {
    B map (b => {
      RegNextWhen(a*b, io.enable) init(0)
    })
  })

}
