package arithmetic

import spinal.core._
import spinal.lib._

class ModReducerBySparseConstant extends Component {
  val io = new Bundle {
    val cmd = slave Stream UInt()
    val rsp = master Stream UInt()
  }
}