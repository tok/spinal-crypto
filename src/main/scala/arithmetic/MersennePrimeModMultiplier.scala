package arithmetic

import spinal.core._
import spinal.lib.LatencyAnalysis

/**
  * Represent a prime of the form `2**p - c`.
  *
  * @param p
  * @param c
  */
case class PseudoMersennePrime(p: Int, c: Int) {
  val value = BigInt(2).pow(p) - c
  require(c >= 0)
  require(p > 2 * BigInt(c).bitLength)
  //require(value.isProbablePrime(128), s"`2**${p} - c` is not a prime!")
}

/**
  * Mersenne-prime modular multiplier as described in [1] extended to Pseudo-Mersenne primes.
  *
  * [1]: Philipp Kopperman et al, Automatic Generation of High-Performance Modular Multipliers for Arbitrary Mersenne Primes on FPGAs.
  *
  * @param modulus
  * @param subMultConfig Specifies the size of the underlying multipliers that shall be used.
  * @param reduceMod2M   When this is set to `true` the reduction is performed modulo 2*M instead of M.
  */
class MersennePrimeModMultiplier(modulus: PseudoMersennePrime,
                                 subMultConfig: MultiplierConfig = MultiplierConfig(17, 24),
                                 reduceMod2M: Boolean = false
                                ) extends Component {

  val p = modulus.p

  val io = new Bundle {
    val enable = in Bool
    val cmd = in(BinaryOpCmd(p, p))
    val rsp = out UInt (p bits)
  }

  import io.cmd.{opA, opB}

  //  private def roundUp(mod: Int, x: Int): Int = x % mod match {
  //    case 0 => x
  //    case _ => x + mod - x % mod
  //  }

  // Append zeros to a UInt to pad its length to an integer multiple of `mod`.
  private def padSizeToMultiple(x: UInt, mod: Int): UInt = {
    x.getWidth % mod match {
      case 0 => x
      case remainder =>
        val padLength = mod - remainder

        (B"0".resize(padLength) ## x).asUInt
    }
  }

  //  private def subdivide(x: UInt, slice: Int): IndexedSeq[UInt] = {
  //    val w = x.getWidth
  //    (0 until w by slice) map (start => x(start until List(start + slice, w).min))
  //  }

  val paddedOpA = padSizeToMultiple(opA, subMultConfig.m)
  val paddedOpB = padSizeToMultiple(opB, subMultConfig.n)

  val A = paddedOpA.subdivideIn(subMultConfig.m bits)
  val B = paddedOpB.subdivideIn(subMultConfig.n bits)

  val subMultipliers = A map (a => {
    B map (b => {
      RegNextWhen(a * b, io.enable) init (0)
    })
  })

  // Create asymmetrically tiled multiplier.
  val tileProducts = MPMMGenerator.getDigitProductPosistions(MultiplierConfig(p, p), subMultConfig)
  assert((tileProducts map (_.i) max) == subMultipliers.length - 1)
  assert((tileProducts map (_.j) max) == subMultipliers(0).length - 1)
  val sliced = MPMMGenerator.sliceToBitsModulo(tileProducts, modulus)
  val rearranged = MPMMGenerator.rearrangeSlicedDigitProducts(sliced)

  val partialProducts = rearranged map (P => {
    val abstractBits = P map (p => p.destPos -> p) toMap
    val bits = abstractBits mapValues (slice => {
      import slice._
      subMultipliers(i)(j)(srcPos)
    })
    val msbOffset = P map (_.destPos) max

    // Assemble UInt from single bits.
    Cat(
      (0 to msbOffset) map (i => bits.getOrElse(i, B"0"))
    ).asUInt
  })

  val numTreeStages = log2Up(partialProducts.length)
  val sum = AdderTree.adderTree(partialProducts, numStages = numTreeStages, enable = io.enable)

  val carries = sum(p until sum.getWidth)
  val reduced1 = RegNextWhen(modulus.c * carries + sum(0 until p).resize(p + 1), io.enable) init (0)

  val carry = if (reduceMod2M) {
    reduced1(p).asUInt
  } else {
    (reduced1 >= modulus.value).asUInt
  }

  // Subtract 2^p-c conditionally on carry2. 2^p is subtracted automatically by an overflow.
  val reduced2 = RegNextWhen(modulus.c * carry + reduced1(0 until p), io.enable) init (0)


  io.rsp := reduced2

  val latency = LatencyAnalysis(io.cmd.opA, io.rsp)
  println("sub multipliers:" + subMultipliers.flatten.length)
  println("partial products:" + partialProducts.length)
  println("adder tree stages: " + numTreeStages)
  println("latency: " + latency)
  //assert(latency == numTreeStages + 3)
  assert(latency == LatencyAnalysis(io.cmd.opB, io.rsp))
}

/**
  * Provides algorithms to generate a asymmetrically tiled Mersenne-prime modular multiplier
  * as described in [1].
  *
  **/
object MPMMGenerator {

  /**
    * Represents a digit product, i.e. the output of a sub-multiplier.
    *
    * @param i         Sub-multiplier index.
    * @param j         Sub-multiplier index.
    * @param bitOffset Target bit offset.
    * @param len       Length of slice. Equals length of sub-multiplier output.
    */
  case class DigitProduct(i: Int, j: Int, bitOffset: Int, len: Int)

  /**
    * Represents a single bit of a digit product.
    * The bit comes from `srcPos` of multiplier `i,j` and has offset `destPos` in a partial product.
    *
    * @param i
    * @param j
    * @param srcPos
    * @param destPos
    */
  case class SlicedDigitProduct(i: Int, j: Int, srcPos: Int, destPos: Int)

  /** Determine the position of digit products for a asymmetrically tiled multiplier.
    * Given the configuration of the target multiplier and the sub-multipliers calculates
    * the target positions of the sub-multiplier results.
    *
    * Corresponds to algorithm 1 in [1].
    *
    * @param multConfig
    * @param subMultConfig
    * @return
    */
  def getDigitProductPosistions(multConfig: MultiplierConfig, subMultConfig: MultiplierConfig): Set[DigitProduct] = {
    val numSlicesA = (multConfig.m + subMultConfig.m - 1) / subMultConfig.m
    val numSlicesB = (multConfig.n + subMultConfig.n - 1) / subMultConfig.n
    val m = subMultConfig.m
    val n = subMultConfig.n

    val r = for (i <- 0 until numSlicesA; j <- 0 until numSlicesB) yield {
      val offset = i * m + j * n
      DigitProduct(i, j, offset, m + n)
    }

    r toSet
  }


  /** Bit-wise modulo operation.
    *
    * Calculates the reduction modulo `modulus` of a single bit.
    * The result can be zero, one or multiple bits depending on whether a reduction took place and depending on the modulus.
    *
    * Corresponds to algorithm 2 in [1], just extended to Pseudo-Mersenne primes.
    *
    * @param bit
    * @param modulus
    * @return
    */
  def bitwiseModulo(bit: SlicedDigitProduct, modulus: PseudoMersennePrime): Seq[SlicedDigitProduct] = {
    modulus.p match {
      case 0 => Seq(bit)
      case _ =>
        bit.destPos match {
          case d if d < modulus.p =>
            // No reduction necessary.
            Seq(bit)
          case d if d < 2 * modulus.p =>
            // Reduction necessary.
            modulus.c match {
              case 0 => Nil // Modular reduction by a power of 2 is just dropping the high bits.
              case _ => {
                assert(bit.destPos >= modulus.p)
                // Multiply one bit with c.
                val c = BigInt(modulus.c)
                val cBitIndices = (0 to c.bitLength) filter c.testBit
                val product = cBitIndices map (i => {
                  bit.copy(destPos = bit.destPos + i - modulus.p)
                })
//                // Again, reduce the bits of the product by the modulus.
//                val reduced = product flatMap (bitwiseModulo(_, modulus))
//                reduced
                product
              }
            }
          case d =>
            // This bit should be zero anyway.
            assert(d >= 2 * modulus.p)
            Nil
        }
    }
  }

  /** Split results of sub-multipliers into single bits and take the reduction modulo 2**p-c into account.
    *
    * @param dps Digit products coming from sub-multipliers.
    * @param modulus
    * @return
    */
  def sliceToBitsModulo(dps: Set[DigitProduct], modulus: PseudoMersennePrime): Set[SlicedDigitProduct] = {
    require(modulus.p > 0)

    val reducedBits = dps.toSeq flatMap (t => {
      import t._
      (0 until t.len)
        .filter(_ + t.bitOffset < 2 * modulus.p)
        .map(k => SlicedDigitProduct(i, j, k, k + t.bitOffset))
        .flatMap(bitwiseModulo(_, modulus))
    })

//    def deduplicateBits(reducedBits: Seq[SlicedDigitProduct]): Seq[SlicedDigitProduct] = {
//      // remove duplicates
//      val grouped = reducedBits.groupBy(identity) mapValues (_.size)
//      val dedup = grouped.toSeq flatMap {
//        case (bit, count) =>
//          val c = BigInt(count)
//          val cBitIndices = (0 to c.bitLength) filter c.testBit
//          val product = cBitIndices map (i => {
//            bit.copy(destPos = bit.destPos + i)
//          })
//          product
//      }
//
//      if (dedup.length == dedup.toSet.size) {
//        dedup
//      } else {
//        deduplicateBits(dedup)
//      }
//    }
//
//    val dedup = deduplicateBits(reducedBits)
//
//    assert(dedup.length == dedup.toSet.size)
    assert(reducedBits.length == reducedBits.toSet.size)

    reducedBits.toSet
  }

  /** Split results of sub-multipliers into single bits.
    *
    * @param dps Digit products coming from sub-multipliers.
    * @return
    */
  def sliceToBits(dps: Set[DigitProduct]): Set[SlicedDigitProduct] = {
    dps flatMap (t => {
      import t._
      (0 until t.len)
        .map(k => SlicedDigitProduct(i, j, k, k + t.bitOffset))
    })
  }

  /**
    * Rearrange bits into equal sized partial products.
    *
    * Corresponds to algorithm 3 in [1].
    *
    * @param z
    * @return
    */
  def rearrangeSlicedDigitProducts(z: Set[SlicedDigitProduct]): Seq[Set[SlicedDigitProduct]] = {
    // Create buckets by destination bit offset.
    val msbOffset = z map (_.destPos) max
    val buckets = (0 to msbOffset) map (
      v => z.filter(_.destPos == v)
        .toSeq
        .sortBy(s => (s.j, s.i))
        .map(Some(_))
      )
    // Create a table by padding rows to equal size.
    val len = buckets map (_.size) max
    val padded = buckets map (_.padTo(len, None))
    // Group by partial products
    val tr = padded.transpose
    // Remove Nones
    val trimmed = tr map (_.flatten toSet)
    trimmed
  }
}