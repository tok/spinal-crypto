package arithmetic

import spinal.core._


object AdderTree {

  def hammingWeight(x: Bits): UInt = {
    adderTree(x.asBools.map(_.asUInt))
  }

  def adderTree(summands: Seq[UInt], numStages: Int = 0, enable: Bool = True): UInt = {
    require(numStages >= 0)
    val nonZeros = summands filter (_.getBitsWidth > 0)
    val withMaxValue = nonZeros map (x => (x, x.maxValue))

    /**
      *
      * @param summands A list of (UInt, maximum value)
      * @param numStages
      * @return (some UInt, the maximum value it can reach)
      */
    def _adderTree(summands: Seq[(UInt, BigInt)], numStages: Int = 0): (UInt, BigInt) = {
      require(numStages >= 0)
      require(summands forall (_._1.getWidth > 0))
      require(summands forall (x => x._2 <= x._1.maxValue))

      val numSuperStages = math.min(numStages, 1)
      val numSubStages = numStages - numSuperStages

      val (result, maxValue: BigInt) = summands match {
        case Nil => (U(0), 0)
        case s +: Nil => s
        case (a, aMax) +: (b, bMax) +: Nil => {
          val maxValue = aMax + bMax
          val resultWidth = log2Up(maxValue + 1)
          val result = a.resize(resultWidth) + b
          (result, maxValue)
        }
        case _ => {
          assert(summands.length > 2)
          val sums = summands.grouped(2).map(l => _adderTree(l, 0)).toSeq
          _adderTree(sums, numSubStages)
        }
      }

      // Insert register stage if allowed.
      numSuperStages match {
        case 0 => (result, maxValue)
        case _ => (RegNextWhen(result, enable) init(0), maxValue)
      }
    }

    _adderTree(withMaxValue, numStages)._1
  }

}
