package arithmetic

import spinal.core._
import spinal.lib._

case class BinaryOpCmd(m: Int, n: Int)  extends Bundle {
  val opA = UInt(m bits)
  val opB = UInt(n bits)
}

object BinaryOpCmd {
  def apply(width: Int): BinaryOpCmd = {
    BinaryOpCmd(width, width)
  }
}