package crypto

import spinal.core
import spinal.core._
import spinal.lib._
import spinal.lib.fsm._
import symmetric.{ChaChaPermutation, ChaChaPermutationCmd}

case class FortunaCmd() extends Bundle {
  val key = Bits(256 bits)
  val nonce = Bits(96 bits)
  val counter = UInt(32 bits)
  val rounds = UInt(4 bits)
}

//object AccCmd extends SpinalEnum {
//    val ABSORB, EXTRACT = newElement()
//}
//
//case class AccumulatorCmd() extends Bundle {
//  val cmd = AccCmd
//}
//
//class Accumulator(numAccumulators: Int = 32, accumulatorWidth: BitCount = 256 bits) extends Component {
//
//}

class Fortuna(wordlength: BitCount = 8 bits) extends Component {

  require(256 % wordlength.value == 0)
  val accumulatorWidth = 256 bits
  val numAccumulators = 32

  val io = new Bundle {
    val entropyInput = slave Stream Bits(wordlength)
    val randomData = master Stream Bits(wordlength)
  }

  val chachaCore = new ChaChaPermutation
  val chachaCmd = new ChaChaPermutationCmd
  chachaCore.io.cmd.payload := chachaCmd
  chachaCmd.data.foreach(_ := 0)
  chachaCmd.doubleRounds := 10
  chachaCore.io.cmd.valid := False
  chachaCore.io.rsp.ready := False

  // Entropy input stream.
  io.entropyInput.ready := False

  // Output stream.
  io.randomData.valid := False
  io.randomData.payload := 0

  //val accumulator = Vec(Reg(Bits(accumulatorWidth)) init(0), numAccumulators)
  val accumulator = Mem(Bits(accumulatorWidth), numAccumulators)
  val currentOutput = Reg(Bits(accumulatorWidth)) init(0)

  val accInputPointer = Reg(UInt(log2Up(numAccumulators) bits)) init(0) // Pointer to the accumulator where input is routed to.
  val accInputOffset = Reg(UInt(log2Up(accumulatorWidth.value / wordlength.value) bits)) init(0)

  val refreshCounter = Reg(UInt(numAccumulators bits)) init (0)


  val outputAsWords = currentOutput.subdivideIn(wordlength)
  val outputOffset = Reg(UInt(log2Up(outputAsWords.length) bits)) init (0)  // Pointer to next output word.
  val outputFresh = RegInit(False) // True iff accumulator[0] has fresh randomness.

  val fsm = new StateMachine {

    val destPointer = Reg(UInt(log2Up(numAccumulators) bits)) init (0)
    val srcPointer = destPointer + 1
    val destPointer_Next = destPointer - 1  // Decrement destination pointer.
    val updateMask = Reg(Bits(numAccumulators bits)) init (0) // One bit for each accumulator. Update the accumulator if the bit is set.

    val sInit: State = new State with EntryPoint {
      onEntry(destPointer := 0)
      whenIsActive {
        // Clear the memory.
        accumulator(destPointer) := B"00".resize(accumulatorWidth.value)
        destPointer := destPointer + 1
        when(destPointer === numAccumulators-1) {
          goto(ready)
        }
      }
      onExit {
        outputFresh := False
        accumulator(numAccumulators-1):= B"x01".resize(256)
      }
    }

    val ready: State = new State {
      whenIsActive {

        when(io.randomData.ready && outputFresh) {
          // Generate output data.
          io.randomData.payload := outputAsWords(outputOffset)
          io.randomData.valid := True

          val outputOffset_Next = (outputOffset + 1) % outputAsWords.length

          // Mark the buffer as used.
          when(outputOffset_Next === 0) {
            outputFresh := False
            goto(setupReseed)
          }

          outputOffset := outputOffset_Next
        }

        io.entropyInput.ready := True
        when(io.entropyInput.valid) {
          // TODO: add input to accumulator
        }

        when(!outputFresh) {
          goto(setupReseed)
        }
      }
    }

    val setupReseed: State = new State {
      whenIsActive {
        // Set pointer and mask.
        destPointer := numAccumulators - 2
        updateMask := ((refreshCounter - 1) ^ refreshCounter)(0 until numAccumulators-1) ## B"1"
        goto(reseedLoop)
      }
    }

    val reseedLoop: State = new State {

      whenIsActive {
        when(srcPointer === 0) {
          // Done
          currentOutput := accumulator(0)
          outputFresh := True
          refreshCounter := refreshCounter + 1
          goto(ready)
        } otherwise {
          when(updateMask(destPointer)) {
            goto(propagateSeed)
          } otherwise {
            // skip
            destPointer := destPointer_Next
          }
        }
      }
    }

    val bufferSrc = Reg(Bits(accumulatorWidth)) init(0)
    val bufferDest = Reg(Bits(accumulatorWidth)) init(0)
    val propagateSeed: State = new State {
      onEntry {
        bufferSrc := accumulator(srcPointer)
        bufferDest := accumulator(destPointer)
      }
      whenIsActive {
        when(chachaCore.io.cmd.ready) {
          val inputVector = Vec(UInt(32 bits), 16)
          //val constant = B"256'x617078653320646e79622d326b20657400000000000000000000000000000000"
          // Fetch seed from next accumulator.
          inputVector := Vec((bufferDest ## bufferSrc).subdivideIn(32 bits).map(_.asUInt))
          chachaCmd.data := inputVector
          chachaCore.io.cmd.valid := True

          goto(getChachaResponse)
        }
      }
    }

    val getChachaResponse: State = new State {
      whenIsActive {
        chachaCore.io.rsp.ready := True
        when(chachaCore.io.rsp.valid) {
          accumulator(destPointer) := chachaCore.io.rsp.payload.asBits(0 until 256)
          goto(reseedLoop)
        }
      }
      onExit {
        destPointer := destPointer_Next
        bufferSrc.clearAll()
        bufferDest.clearAll()
      }
    }

  }
}

object Fortuna {
  def main(args: Array[String]) {
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new Fortuna(wordlength = 8 bits))
  }
}
