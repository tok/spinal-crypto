package crypto

import spinal.core
import spinal.core._

import spinal.lib._
import spinal.lib.fsm._

//import spinal.crypto.hash._

case class HashCoreGeneric(dataWidth: BitCount,
                           hashWidth: BitCount,
                           hashBlockWidth: BitCount)

case class HashCoreCmd(g: HashCoreGeneric) extends Bundle {
  val msg = UInt(g.dataWidth)
  val size = UInt(log2Up(g.dataWidth.value / 8) bits)
}

case class HashCoreRsp(g: HashCoreGeneric) extends Bundle {
  val digest = Bits(g.hashWidth)
}


class Blake(hashWidth: Int, dataWidth: BitCount = 8 bits) extends Component {
  require(dataWidth.value == 8, "Currently only 8 bits data width supported.")
  require(List(224, 256, 384, 512).contains(hashWidth), s"Unsupported hash size ($hashWidth)!")

  val wordSize = hashWidth match {
    case 224 => 32
    case 256 => 32
    case 384 => 64
    case 512 => 64
  }
  require(List(32, 64).contains(wordSize))

  val g = HashCoreGeneric(dataWidth, hashWidth bits, 16 * wordSize bits)

  val io = new Bundle {
    val init = in Bool
    val cmd = slave Stream (Fragment(HashCoreCmd(g)))
    val rsp = master Flow (HashCoreRsp(g))
  }

  io.rsp.digest.clearAll()


  def WORD = UInt(wordSize bits)

  val totalRounds = hashWidth match {
    case 224 => 14
    case 256 => 14
    case 384 => 16
    case 512 => 16
  }

  val iv: Seq[Long] = hashWidth match {
    case 224 => List(
      0xC1059ED8, 0x367CD507, 0x3070DD17, 0xF70E5939,
      0xFFC00B31, 0x68581511, 0x64F98FA7, 0xBEFA4FA4
    )
    case 256 => List(
      0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
      0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
    )
    case 384 => List(
      0xcbbb9d5dc1059ed8L, 0x629a292a367cd507L, 0x9159015a3070dd17L, 0x152fecd8f70e5939L,
      0x67332667ffc00b31L, 0x8eb44a8768581511L, 0xdb0c2e0d64f98fa7L, 0x47b5481dbefa4fa4L
    )
    case 512 => List(
      0x6a09e667f3bcc908L, 0xbb67ae8584caa73bL, 0x3c6ef372fe94f82bL, 0xa54ff53a5f1d36f1L,
      0x510e527fade682d1L, 0x9b05688c2b3e6c1fL, 0x1f83d9abfb41bd6bL, 0x5be0cd19137e2179L
    )
  }


  val engine = new BlakeEngine(wordSize bits)

  val state = Vec(Reg(WORD), 16)
  val h = Reg(Vec(WORD, 8))
  val salt = (0 until 4) map (_ => 0)
  // Currently not used by implementation.
  val counter = Reg(UInt(2 * wordSize bits)) init (0)

  // 16 WORDs as bytes.
  val inputBuf = Vec(Reg(UInt(dataWidth)), 16 * wordSize / dataWidth.value)
  // Convert bytes to big endian 32 or 64 ints.
  val inputBufAsUInts = Vec(Cat(inputBuf).subdivideIn(wordSize bits).map(_.asUInt))
  assert(inputBufAsUInts.length == 16)

  // TODO: Reg(Vec) or Vec(Reg) ?
  val inputPtr = Reg(UInt(log2Up(inputBuf.length) bits))
  assert(isPow2(inputBuf.length))

  io.cmd.ready := False
  io.rsp.valid := False

  val fsm = new StateMachine {

    always {
      when(io.init) {
        // Reset Blake to initial state.
        state.foreach(_ := 0)
        (h, iv).zipped map (_ := S(_).asUInt)
        inputBuf.foreach(_ := 0)
        inputPtr := 0
        counter := 0
        goto(sLoad)
      }
    }

    val sIdle: State = new State with EntryPoint {
      whenIsActive {
      }
    }

    val inputPtr_next = inputPtr + 1

    // Load bytes into buffer.
    val sLoad: State = new State {
      onEntry {
        inputPtr := 0
        inputBuf.foreach(_ := 0)
      }
      when(io.cmd.valid) {

        inputBuf(inputPtr) := io.cmd.msg
        inputPtr := inputPtr_next
        counter := counter + dataWidth.value
        io.cmd.ready := True

        when(io.cmd.last) {
          // add padding, absorb & finish
        } otherwise {
          when(inputPtr_next === 0) {
            // absorb
            goto(sCompress)
          }
        }

      }
    }


    def compressLoopFSM() = new StateMachine {

      val roundCounter = Reg(UInt(log2Up(totalRounds) bits)) init (0)

      val sInit: State = new State with EntryPoint {
        whenIsActive {
          roundCounter := 0
          goto(sCommand)
        }
      }

      val sCommand: State = new State {
        whenIsActive {
          when(engine.io.cmd.ready) {
            val cmd = BlakeEngineCmd(wordSize bits)
            cmd.msg := inputBufAsUInts
            cmd.state := state
            cmd.roundNumber := roundCounter
            engine.io.cmd.payload := cmd
            engine.io.cmd.valid := True
            goto(sGetResp)
          }
        }
      }
      val sGetResp: State = new State {
        whenIsActive {
          engine.io.rsp.ready := True
          when(engine.io.rsp.valid) {
            // Update state with output of the core.
            state := engine.io.rsp.payload
            when(roundCounter === totalRounds - 1) {
              exit()
            } otherwise {
              roundCounter := roundCounter + 1
              goto(sCommand)
            }
          }
        }
      }
    }

    val sCompress: State = new StateFsm(compressLoopFSM()) {
      onEntry {
        for (i <- 0 until 8) state(i + 0) := h(i)
        for (i <- 0 until 4) state(i + 8) := state(i + 8) ^ salt(i)
        state(12) := state(12) ^ counter(31 downto 0)
        state(13) := state(13) ^ counter(31 downto 0)
        state(14) := state(14) ^ counter(63 downto 32)
        state(15) := state(15) ^ counter(63 downto 32)
      }
      whenCompleted {
        goto(sLoad)
      }
      onExit {
        for (i <- 0 until 8) h(i) := h(i) ^ state(i) ^ state(i + 8) ^ salt(i % 4)
        inputPtr := 0
        inputBuf.foreach(_ := 0)
      }
    }
  }

}

case class BlakeEngineCmd(wordSize: BitCount) extends Bundle {
  val msg = Vec(UInt(wordSize), 16)
  val state = Vec(UInt(wordSize), 16)
  val roundNumber = UInt(4 bits)
}

class BlakeEngine(wordSize: BitCount) extends Component {
  require(List(32, 64).contains(wordSize.value))

  val io = new Bundle {
    val cmd = slave Stream BlakeEngineCmd(wordSize)
    val rsp = master Stream Vec(UInt(wordSize), 16)
  }

  // No latency in this implementation.
  io.cmd.ready := io.rsp.ready
  io.rsp.valid := io.cmd.valid

  val cst: Seq[UInt] = wordSize.value match {
    case 32 => List(
      U"x243F6A88", U"x85A308D3", U"x13198A2E", U"x03707344",
      U"xA4093822", U"x299F31D0", U"x082EFA98", U"xEC4E6C89",
      U"x452821E6", U"x38D01377", U"xBE5466CF", U"x34E90C6C",
      U"xC0AC29B7", U"xC97C50DD", U"x3F84D5B5", U"xB5470917"
    )
    case 64 => List(
      U"x243F6A8885A308D3", U"x13198A2E03707344", U"xA4093822299F31D0", U"x082EFA98EC4E6C89",
      U"x452821E638D01377", U"xBE5466CF34E90C6C", U"xC0AC29B7C97C50DD", U"x3F84D5B5B5470917",
      U"x9216D5D98979FB1B", U"xD1310BA698DFB5AC", U"x2FFD72DBD01ADFB7", U"xB8E1AFED6A267E96",
      U"xBA7C9045F12C7F99", U"x24A19947B3916CF7", U"x0801F2E2858EFC16", U"x636920D871574E69"
    )
    case _ => throw new MatchError(s"Invalid word size: ${wordSize.value}")
  }
  //val cstUInt = cst.map(S(_, wordSize)).map(_.asBits).map(_.asUInt)
  val cstROM = Mem(UInt(wordSize), initialContent = cst)


  val permNumbers = Array(
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3,
    11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4,
    7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8,
    9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13,
    2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9,
    12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11,
    13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10,
    6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5,
    10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0
  )
  val permROMSize = permNumbers.length

  // Create a ROM containing the permutations.
  val permROM = Mem(UInt(4 bits), permNumbers.map(U(_)))

  def G(i: UInt,
                 msg: Vec[UInt],
                 round: UInt,
                 a_in: UInt, b_in: UInt, c_in: UInt, d_in: UInt,
                 a_out: UInt, b_out: UInt, c_out: UInt, d_out: UInt): Unit = {

    // Rotation parameters depend on word size.
    val (r0, r1, r2, r3) = wordSize.value match {
      case 32 => (16, 12, 8, 7)
      case 64 => (32, 25, 16, 11)
    }

    /*
      a += b + (msg[perm[r][2*i]] ^ cst[perm[r][2*i+1]]);
      d = ror(d^a, r0);
      c += d;
      b = ror(b^c, r1);
      a += b + (msg[perm[r][2*i+1]] ^ cst[perm[r][2*i]]);
  */

    // Begin to read table from beginning on overflow.
    val roundWrappedAround = round + 0
    when(round >= 10) {
      roundWrappedAround := round - 10
    }
    //    assert(roundWrappedAround < U(10))

    val rowOffset = roundWrappedAround.resize(8) * 16 // Effectively calculates the row in the table.
    val addrA = rowOffset + 2 * i
    val addrB = addrA + 1
    val offsetA = permROM(addrA)
    val offsetB = permROM(addrB)

    val a1 = a_in + b_in + (msg(offsetA) ^ cstROM(offsetB))
    val d1 = (a1 ^ d_in).rotateLeft(r0)
    val c1 = c_in + d1
    val b1 = (b_in ^ c1).rotateLeft(r1)
    val a2 = a1 + b1 + (msg(offsetB) ^ cstROM(offsetA))
    val d2 = (a2 ^ d1).rotateLeft(r2)
    val c2 = c1 + d2
    val b2 = (b1 ^ c2).rotateLeft(r3)

    a_out := a2
    b_out := b2
    c_out := c2
    d_out := d2
  }

  val indices1 = List(
    (0, 4, 8, 12),
    (1, 5, 9, 13),
    (2, 6, 10, 14),
    (3, 7, 11, 15)
  )

  val indices2 = List(
    (0, 5, 10, 15),
    (1, 6, 11, 12),
    (2, 7, 8, 13),
    (3, 4, 9, 14)
  )

  val state_in = io.cmd.state
  val state_half = Vec(UInt(wordSize), 16)
  val state_out = io.rsp.payload
  // TODO: add register stage between two half-rounds. Reuse G.
  for ((i, idx) <- (0 until 3, indices1).zipped) {
    G(i,
      io.cmd.msg,
      io.cmd.roundNumber,
      state_in(idx._1), state_in(idx._2), state_in(idx._3), state_in(idx._4),
      state_half(idx._1), state_half(idx._2), state_half(idx._3), state_half(idx._4)
    )
  }

  for ((i, idx) <- (0 until 3, indices2).zipped) {
    G(i,
      io.cmd.msg,
      io.cmd.roundNumber,
      state_half(idx._1), state_half(idx._2), state_half(idx._3), state_half(idx._4),
      state_out(idx._1), state_out(idx._2), state_out(idx._3), state_out(idx._4)
    )
  }
}

object Blake {
  def main(args: Array[String]) {
    SpinalConfig(targetDirectory = "rtl").generateVerilog(new Blake(256))
  }
}

