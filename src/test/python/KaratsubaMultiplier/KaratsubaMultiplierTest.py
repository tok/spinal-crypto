import cocotb
from cocotb.clock import Clock
from cocotb.result import TestFailure
from cocotb.triggers import FallingEdge, RisingEdge, Timer
from cocotb.binary import BinaryValue

from binascii import hexlify, unhexlify
import re
from random import randint

clockCount = 0

@cocotb.coroutine
def clockCycleCount(dut):
    global clockCount
    while True:
        yield RisingEdge(dut.clk)
        clockCount += 1

@cocotb.test()
def test_karatsuba_multiplier(dut):

    global clockCount

    dut.io_enable = False

    dut.reset = 1
    dut.clk = 0
    yield Timer(1000)
    dut.reset = 0
    yield Timer(1000)

    cocotb.fork(clockCycleCount(dut))
    cocotb.fork(Clock(dut.clk, 1000).start())

    @cocotb.coroutine
    def tick():
        yield Timer(1)
        yield RisingEdge(dut.clk)
        yield Timer(1)

    # Cycle once
    yield tick()
    width = 32

    for i in range(0, 1024):
        a = randint(0, 2**width-1)
        b = randint(0, 2**width-1)
        latency = 2
        print("input: %d * %d"%(a, b))
        dut.io_cmd_opA = a
        dut.io_cmd_opB = b
        yield tick()
        dut.io_enable = True
        for i in range(0, latency):
            yield tick()
        
        #yield Timer(1)
        ab = int(dut.io_rsp.value)
        #print('borrow (a,b): (%s, %s)'%(dut.borrowA.value, dut.borrowB.value)) 
        #print('absDiff (a,b): (%s, %s)'%(dut.absDiff_a.value, dut.absDiff_b.value)) 
        #print('m_hat: %s'%dut.m_hat.value) 
        #print('%s * %s'%(bin(a), bin(b)))
        print('output : %d'%ab)
        if(ab !=  a*b):
            raise TestFailure('Multiplication failed.')
