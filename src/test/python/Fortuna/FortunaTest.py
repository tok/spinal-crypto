import cocotb
from cocotb.clock import Clock
from cocotb.result import TestFailure
from cocotb.triggers import FallingEdge, RisingEdge, Timer
from cocotb.binary import BinaryValue

from binascii import hexlify, unhexlify
import re

clockCount = 0

@cocotb.coroutine
def clockCycleCount(dut):
    global clockCount
    while True:
        yield RisingEdge(dut.clk)
        clockCount += 1

@cocotb.test()
def test_chacha_permute(dut):

    def getVector(dut, name):
        return [int(getattr(dut, '%s_%d'%(name, i))) for i in range(0, 16)]

    global clockCount

    dut.io_entropyInput_valid = False
    dut.io_randomData_ready = False

    dut.reset = 1
    dut.clk = 0
    yield Timer(1000)
    dut.reset = 0
    yield Timer(1000)

    cocotb.fork(clockCycleCount(dut))
    cocotb.fork(Clock(dut.clk, 1000).start())

    # Cycle once
    yield RisingEdge(dut.clk)
    
    dut.io_randomData_ready = True
    for i in range(0, 128):
        clkStart = clockCount
        yield RisingEdge(dut.clk)
        while(not bool(dut.io_randomData_valid.value)):
            yield RisingEdge(dut.clk)
        print("%03d: %s (clk = %d)"%(i, dut.io_randomData_payload.value, clockCount-clkStart))

