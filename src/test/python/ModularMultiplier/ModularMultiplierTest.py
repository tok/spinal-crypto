import cocotb
from cocotb.clock import Clock
from cocotb.result import TestFailure
from cocotb.triggers import FallingEdge, RisingEdge, Timer
from cocotb.binary import BinaryValue

from binascii import hexlify, unhexlify
import re
from random import randint

clockCount = 0

@cocotb.coroutine
def clockCycleCount(dut):
    global clockCount
    while True:
        yield RisingEdge(dut.clk)
        clockCount += 1

@cocotb.test()
def test_modular__multiplier(dut):

    global clockCount

    dut.io_cmd_valid = False
    dut.io_rsp_ready = False

    dut.reset = 1
    dut.clk = 0
    yield Timer(1000)
    dut.reset = 0
    yield Timer(1000)

    cocotb.fork(clockCycleCount(dut))
    cocotb.fork(Clock(dut.clk, 1000).start())

    @cocotb.coroutine
    def tick():
        yield Timer(1)
        yield RisingEdge(dut.clk)
        yield Timer(1)

    # Cycle once
    yield tick()
    width = 256
    modulus = 2*(2**255-19)
    dut.io_rsp_ready = True
    for i in range(0, 1024):
        dut.io_cmd_valid = True
        a = randint(0, 2**width-1)
        b = randint(0, 2**width-1)
        print("input: %d * %d"%(a, b))
        dut.io_cmd_payload_opA = a
        dut.io_cmd_payload_opB = b
        yield tick()
        dut.io_cmd_valid = False
        while dut.io_rsp_valid != True:
            yield tick()
        
        output = int(dut.io_rsp_payload.value)
        if(output !=  (a*b) % modulus):
            raise TestFailure('Modular multiplication failed.')
