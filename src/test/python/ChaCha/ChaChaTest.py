import cocotb
from cocotb.clock import Clock
from cocotb.result import TestFailure
from cocotb.triggers import FallingEdge, RisingEdge, Timer
from cocotb.binary import BinaryValue

from binascii import hexlify, unhexlify
import re

clockCount = 0

@cocotb.coroutine
def clockCycleCount(dut):
    global clockCount
    while True:
        yield RisingEdge(dut.clk)
        clockCount += 1

@cocotb.test()
def test_chacha_permute(dut):

    def getVector(dut, name):
        return [int(getattr(dut, '%s_%d'%(name, i))) for i in range(0, 16)]

    global clockCount

    dut.io_cmd_valid = False
    dut.io_rsp_ready = False

    dut.reset = 1
    dut.clk = 0
    yield Timer(1000)
    dut.reset = 0
    yield Timer(1000)

    cocotb.fork(clockCycleCount(dut))
    cocotb.fork(Clock(dut.clk, 1000).start())

    @cocotb.coroutine
    def tick():
        yield Timer(1)
        yield RisingEdge(dut.clk)
        yield Timer(1)

    # Cycle once
    yield tick()

    expectedInitialState = [ 0x61707865, 0x3320646e, 0x79622d32, 0x6b206574,
            0x03020100, 0x07060504, 0x0b0a0908, 0x0f0e0d0c,
            0x13121110, 0x17161514, 0x1b1a1918, 0x1f1e1d1c,
            0x00000001, 0x09000000, 0x4a000000, 0x00000000]


    expectedOutput = unhexlify("10f1e7e4d13b5915500fdd1fa32071c4c7d1f4c733c068030422aa9ac3d46c4ed2826446079faa0914c2d705d98b02a2b5129cd1de164eb9cbd083e8a2503c4e")
    

    if(dut.io_rsp_valid == True):
        raise TestFailure("output can't yet be valid")

    # Wait for component to get ready.
    while(dut.io_cmd_ready != True):
        print('Wait for: input.ready == 1')
        yield tick()

    nonce = BinaryValue(unhexlify("000000090000004a00000000"))
    key = BinaryValue(unhexlify("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"))

    # Apply input data
    dut.io_cmd_payload_doubleRounds = 10
    dut.io_cmd_payload_key = key
    dut.io_cmd_payload_nonce = nonce
    dut.io_cmd_payload_counter = 1

    dut.io_cmd_valid = True
    yield tick()

    initialState = getVector(dut, 'initialState')
    for s in initialState:
        print('%08x'%s)
    
    clockCount_atStart = clockCount

    # Wait for component to accept input data
    while(dut.io_cmd_ready != False):
        print('Wait for: input.ready === 0')
        yield tick()

    dut.io_cmd_valid = False

    # Destroy input data
    dut.io_cmd_payload_doubleRounds = 0
    dut.io_cmd_payload_key = 0
    dut.io_cmd_payload_nonce = 0
    dut.io_cmd_payload_counter = 0

    while(dut.io_rsp_valid != True):
        yield tick()
        for v in getVector(dut.permutation, 'chachaState'):
            print('%08x'%v)
        print('\n')

    cycles_permutation = clockCount - clockCount_atStart
    print('Clock cycles for full round: %d'%cycles_permutation)

    if(dut.permutation.roundCounter != 0):
        raise TestFailure('Counter must be 0.')

    if(dut.io_rsp_valid != True):
        raise TestFailure('valid should be HIGH')

    # Compare actual output with expected output
    actualOutput = dut.io_rsp_payload.value.buff
    
    dut.io_rsp_ready = True
    yield tick()

    if(dut.io_rsp_valid != False):
        raise TestFailure('valid should be LOW')

    if(expectedOutput != actualOutput):
        raise TestFailure('wrong output')

    dut.io_rsp_ready = False
    yield tick()
    
