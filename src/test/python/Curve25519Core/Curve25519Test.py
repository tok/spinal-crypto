import cocotb
from cocotb.clock import Clock
from cocotb.result import TestFailure
from cocotb.triggers import FallingEdge, RisingEdge, Timer
from cocotb.binary import BinaryValue

from binascii import hexlify, unhexlify
import re
from random import randint
import curve25519
clockCount = 0

@cocotb.coroutine
def clockCycleCount(dut):
    global clockCount
    while True:
        yield RisingEdge(dut.clk)
        clockCount += 1

@cocotb.test()
def test_curve25519(dut):

    global clockCount

    dut.io_cmd_valid = False
    dut.io_rsp_ready = False

    dut.reset = 1
    dut.clk = 0
    yield Timer(1000)
    dut.reset = 0
    yield Timer(1000)

    cocotb.fork(clockCycleCount(dut))
    cocotb.fork(Clock(dut.clk, 1000).start())

    @cocotb.coroutine
    def tick():
        yield RisingEdge(dut.clk)

    # Cycle once
    yield tick()
    width = 256
    modulus = (2**255-19)
    dut.io_rsp_ready = True
    inputs = [randint(0, modulus) for i in range(0, 1)]
    lambdas = [randint(0, modulus) for i in range(0, len(inputs))]
    basePoint = 9
    for i,_lambda in zip(inputs, lambdas):
        clockCount_start = clockCount
        dut.io_cmd_valid = True
        dut.io_cmd_payload_secretKey = i
        dut.io_cmd_payload_lambda = _lambda
        dut.io_cmd_payload_basePoint = basePoint
        yield tick()
        dut.io_cmd_valid = False
        while dut.io_rsp_valid != True:
            #print('x1 = %d'%dut.x1.value.integer)
            #print('secret key = %s'%dut.secretKeyReg.value)
            #print('x2 = %d'%dut.x2.value.integer)
            #print('x3 = %d'%dut.x3.value.integer)
            #print('z2 = %d'%dut.z2.value.integer)
            #print('z3 = %d'%dut.z3.value.integer)
            print('ladder counter = %d'%dut.mainFsm_ladderCounter_value.value.integer)
            print('inversion counter = %d'%dut.mainFsm_sInit_sInversion_fsm_counter.value.integer)
            yield tick()
        
        while dut.io_rsp_valid != True:
            yield tick()
        totalCycles = clockCount - clockCount_start
        print('cycles = %d'%totalCycles)
        output = dut.io_rsp_payload.value.integer
        print('output = %d'%output)
    
        expectedOutput = curve25519.montgomeryLadder(i, _lambda, basePoint)
        if(output != expectedOutput):
            raise TestFailure("Curve25519 scalar multiplication failed.")
