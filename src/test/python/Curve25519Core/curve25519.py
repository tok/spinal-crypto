import binascii, hashlib, itertools

Q = 2**255 - 19
L = 2**252 + 27742317777372353535851937790883648493
Q2 = 2*Q

def inv(x):
    return pow(x, Q-2, Q)

def inv2q(x):
    return pow(x, Q-2, Q2)
    
d = -121665 * inv(121666)
I = pow(2,(Q-1)//4,Q)

def cswap(a, b, cond):
    if(cond):
        return (b, a)
    return (a, b)

def montgomeryLadder(s, l, xp):
    
    s = s & (~(1<<255))
    s = s | (1<<254)
    s = s & (~0x7)
    
    x1 = l*xp % Q2
    x2 = l
    z2 = 0
    x3 = l*xp % Q2
    z3 = l
    
    for i in reversed(range(0, 255)):
        c = ((s >> i) & 1) ^ ((s >> i+1) & 1)
        x2, x3 = cswap(x2, x3, c)
        z2, z3 = cswap(z2, z3, c)
        
        t1 = x2 + z2 % Q2
        t2 = x2 - z2 % Q2
        t3 = x3 + z3 % Q2
        t4 = x3 - z3 % Q2
        t6 = t1**2   % Q2
        t7 = t2**2   % Q2
        t5 = t6 - t7 % Q2
        t8 = t4 * t1 % Q2
        t9 = t3 * t2 % Q2
        t10 = t8 + t9 % Q2
        t11 = t8 - t9 % Q2
        x3 = l * t10**2 % Q2
        t12 = t11**2 % Q2
        t13 = 121666*t5 % Q2
        x2 = t6*t7 % Q2
        t14 = t7 + t13 % Q2
        z3 = x1*t12 % Q2
        z2 = t5 * t14 % Q2
        
    z2 = inv2q(z2)
    xq = x2 * z2 % Q2
    xq = xq % Q
    
    return xq
