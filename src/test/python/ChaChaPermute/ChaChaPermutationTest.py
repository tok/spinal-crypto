import cocotb
from cocotb.result import TestFailure
from cocotb.triggers import FallingEdge, FallingEdge, Timer

clockCount = 0

@cocotb.coroutine
def genClockAndReset(dut):
    global clockCount
    clockCount = 0
    dut.reset = 1
    dut.clk = 0
    yield Timer(1000)
    dut.reset = 0
    yield Timer(1000)
    while True:
        dut.clk = 1
        yield Timer(500)
        dut.clk = 0
        yield Timer(500)
        clockCount += 1

@cocotb.test()
def test_chacha_permute(dut):

    def getVector(dut, name):
        return [int(getattr(dut, '%s_%d'%(name, i))) for i in range(0, 16)]

    global clockCount

    dut.io_cmd_valid = False
    dut.io_rsp_ready = False

    cocotb.fork(genClockAndReset(dut))

    # Cycle once
    yield FallingEdge(dut.clk)
    
    inp = [ 0x61707865, 0x3320646e, 0x79622d32, 0x6b206574,
            0x03020100, 0x07060504, 0x0b0a0908, 0x0f0e0d0c,
            0x13121110, 0x17161514, 0x1b1a1918, 0x1f1e1d1c,
            0x00000001, 0x09000000, 0x4a000000, 0x00000000]


    expectedOutput = [0x837778ab, 0xe238d763, 0xa67ae21e, 0x5950bb2f, 0xc4f2d0c7, 0xfc62bb2f, 0x8fa018fc, 0x3f5ec7b7, 0x335271c2, 0xf29489f3, 0xeabda8fc, 0x82e46ebd, 0xd19c12b4, 0xb04e16de, 0x9e83d0cb, 0x4e3c50a2]
    
    yield FallingEdge(dut.clk)

    if(dut.io_rsp_valid == True):
        raise TestFailure("output can't yet be valid")

    # Wait for component to get ready.
    while(dut.io_cmd_ready != True):
        print('Wait for: input.ready == 1')
        yield FallingEdge(dut.clk)
    
    # Apply input data
    dut.io_cmd_payload_doubleRounds = 10
    for i in range(0, 16):
        setattr(dut, 'io_cmd_payload_data_%d'%i, inp[i])
    dut.io_cmd_valid = True
    
    yield Timer(1)

    clockCount_atStart = clockCount

    # Wait for component to accept input data
    while(dut.io_cmd_ready != False):
        print('Wait for: input.ready === 0')
        yield FallingEdge(dut.clk)

    dut.io_cmd_valid = False

    # Destroy input data
    for i in range(0, 16):
        setattr(dut, 'io_cmd_payload_data_%d'%i, 0)

    while(dut.io_rsp_valid != True):
        yield FallingEdge(dut.clk)
        for v in getVector(dut, 'chachaState'):
            print('%08x'%v)
        print('\n')

    cycles_permutation = clockCount - clockCount_atStart
    print('Clock cycles for full round: %d'%cycles_permutation)

    if(dut.roundCounter != 0):
        raise TestFailure('Counter must be 0.')

    if(dut.io_rsp_valid != True):
        raise TestFailure('valid should be HIGH')

    # Compare actual output with expected output
    actualOutput = getVector(dut, 'io_rsp_payload')

    for o in actualOutput:
        print("%08x"%o)
    
    dut.io_rsp_ready = True
    yield FallingEdge(dut.clk)

    if(dut.io_rsp_valid != False):
        raise TestFailure('valid should be LOW')

    if(expectedOutput != actualOutput):
        raise TestFailure('wrong output')

    dut.io_rsp_ready = False
    yield FallingEdge(dut.clk)

    if(dut.io_rsp_valid != False):
        raise TestFailure('valid should be reset')
    
