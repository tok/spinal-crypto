import cocotb
from cocotb.clock import Clock
from cocotb.result import TestFailure
from cocotb.triggers import FallingEdge, RisingEdge, Timer
from cocotb.binary import BinaryValue

from binascii import hexlify, unhexlify
import re
from random import randint

clockCount = 0

@cocotb.coroutine
def clockCycleCount(dut):
    global clockCount
    while True:
        yield RisingEdge(dut.clk)
        clockCount += 1

@cocotb.test()
def test_curve25519_inversion(dut):

    global clockCount

    dut.io_cmd_valid = False
    dut.io_rsp_ready = False

    dut.reset = 1
    dut.clk = 0
    yield Timer(1000)
    dut.reset = 0
    yield Timer(1000)

    cocotb.fork(clockCycleCount(dut))
    cocotb.fork(Clock(dut.clk, 1000).start())

    @cocotb.coroutine
    def tick():
        yield RisingEdge(dut.clk)

    # Cycle once
    yield tick()
    width = 256
    modulus = (2**255-19)
    dut.io_rsp_ready = True
    inputs = [randint(0, modulus) for i in range(0, 4)]
    for i in inputs:
        assert(i < modulus)
        dut.io_cmd_valid = True
        dut.io_cmd_payload = i
        yield tick()
        dut.io_cmd_valid = False

        while dut.io_rsp_valid != True:
            print('inversion counter = %d'%dut.fsm_sInversion_fsm_counter.value.integer)
            yield tick()
        
        output = dut.io_rsp_payload.value.integer
        print('output = %d'%output)

        product = (i * output) % modulus
        assert(product < modulus)
        if(product != 1):
            print(i)
            raise TestFailure("Inversion failed")
