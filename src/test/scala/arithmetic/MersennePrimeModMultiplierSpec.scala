package arithmetic

import org.scalatest.FlatSpec
import spinal.core._
import spinal.core.sim._

import scala.collection.mutable
import scala.util.Random

import util.Util

class MersennePrimeModMultiplierSpec extends FlatSpec {

  val modulus = PseudoMersennePrime(127, 1)
  val M = modulus.value

  val compiled = SimConfig.compile(new MersennePrimeModMultiplier(modulus))

  "A Mersenne-prime modular multiplier" should "calculate correct products" in {

    val p = modulus.p

    val inputs = (1 to 16).view map (_ => (Util.randomBigInt(M), Util.randomBigInt(M)))
    val max = BigInt(2).pow(p) - 1

    compiled.doSim { dut: MersennePrimeModMultiplier =>

      val test = fork {

        dut.io.cmd.opA #= 0
        dut.io.cmd.opB #= 0
        dut.io.enable #= false

        dut.clockDomain.disassertReset()
        sleep(100)
        dut.clockDomain.assertReset()
        sleep(100)
        dut.clockDomain.disassertReset()
        sleep(100)

        dut.clockDomain.forkStimulus(10)

        dut.clockDomain.waitActiveEdge()

        val it = inputs.iterator

        while (!it.isEmpty) {

          val (a, b) = it.next()

          dut.clockDomain.waitRisingEdge

          dut.io.cmd.opA #= a
          dut.io.cmd.opB #= b
          dut.io.enable #= true

          var cnt = 0
          while (cnt < dut.latency) {
            dut.clockDomain.waitSampling();
            sleep(0)

            dut.io.cmd.opA #= 0
            dut.io.cmd.opB #= 0

            cnt += 1
          }
          assertResult((a * b) % M) {
            dut.io.rsp.toBigInt
          }
        }
      }

      test.join()
    }
  }

  it should "be properly pipelined" in {

    val p = modulus.p

    val max = BigInt(2).pow(p) - 1

    compiled.doSim { dut: MersennePrimeModMultiplier =>

      val test = fork {
        dut.io.cmd.opA #= 0
        dut.io.cmd.opB #= 0
        dut.io.enable #= false

        dut.clockDomain.disassertReset()
        sleep(100)
        dut.clockDomain.assertReset()
        sleep(100)
        dut.clockDomain.disassertReset()
        sleep(100)

        dut.clockDomain.forkStimulus(10)

        dut.clockDomain.waitActiveEdge()

        val inputs = Stream.from(0) map (_ => (Util.randomBigInt(M), Util.randomBigInt(M)))
        val responseQueue = mutable.Queue.empty[BigInt]
        val it = inputs.iterator

        dut.io.enable #= false
        dut.clockDomain.waitRisingEdge

        var count = 0
        while (count < 1000) {

          val enable = Random.nextBoolean()
          dut.io.enable #= enable

          if (enable) {
            val (a, b) = it.next()
            dut.io.cmd.opA #= a
            dut.io.cmd.opB #= b

            responseQueue.enqueue((a * b) % M)
          }

          dut.clockDomain.waitActiveEdge()
          sleep(0)

          assert(responseQueue.size <= dut.latency)

          if (responseQueue.size == dut.latency && enable) {

            assertResult(responseQueue.dequeue())(dut.io.rsp.toBigInt)
            count += 1
          }
        }
      }

      test.join()
    }
  }
}

class PseudoMersennePrimeMultiplierSpec extends FlatSpec {

  "A mersenne prime multiplier with reduction modulo 2M" should "calculate correct products for pseudo Mersenne moduli" in {

    //val modulus = PseudoMersennePrime(255, 19)
    //val subMults = MultiplierConfig(17, 24)

    //val modulus = PseudoMersennePrime(8, 5)
    //val subMults = MultiplierConfig(2, 3)

    val modulus = PseudoMersennePrime(64, 59)
    val subMults = MultiplierConfig(17, 24)

    val p = modulus.p
    val M = modulus.value
    val maxInput = M - 1

    val randomInputs = (1 to 1 << 10) map (_ => (Util.randomBigInt(M), Util.randomBigInt(M)))
    val oneBitHot = (0 until p * 2) map (i => (BigInt(1) << (i % p), BigInt(1) << (i / p)))
    val oneBitHotSwapped = oneBitHot map (_.swap)
    val maxInputs: Seq[(BigInt, BigInt)] = Seq((0, 0), (maxInput, maxInput), (2, M / 2 + 1))
    val inputs: Seq[(BigInt, BigInt)] = oneBitHot ++ oneBitHotSwapped ++ maxInputs ++ randomInputs

    val compiled = SimConfig.compile(new MersennePrimeModMultiplier(modulus, subMults, reduceMod2M = true))
    compiled.doSim { dut: MersennePrimeModMultiplier =>

      val test = fork {

        dut.io.cmd.opA #= 0
        dut.io.cmd.opB #= 0
        dut.io.enable #= false

        dut.clockDomain.disassertReset()
        sleep(100)
        dut.clockDomain.assertReset()
        sleep(100)
        dut.clockDomain.disassertReset()
        sleep(100)

        dut.clockDomain.forkStimulus(10)

        dut.clockDomain.waitActiveEdge()

        val it = inputs.iterator

        while (!it.isEmpty) {

          val (a, b) = it.next()

          dut.io.cmd.opA #= a
          dut.io.cmd.opB #= b
          dut.io.enable #= true

          var cnt = 0
          while (cnt < dut.latency) {
            dut.clockDomain.waitActiveEdge();
            sleep(0)
            dut.io.cmd.opA #= 0
            dut.io.cmd.opB #= 0

            cnt += 1
          }

          //assert(dut.io.rsp.toBigInt < M, "Result not properly reduced.")
          assert(dut.io.rsp.toBigInt < BigInt(2).pow(p), "")

          assertResult((a * b) % M) {
            dut.io.rsp.toBigInt % M
          }
        }
      }

      test.join()
    }
  }

  "A mersenne prime multiplier with full reduction" should "calculate correct products for pseudo Mersenne moduli" in {

    //val modulus = PseudoMersennePrime(255, 19)
    //val subMults = MultiplierConfig(17, 24)

    //val modulus = PseudoMersennePrime(8, 5)
    //val subMults = MultiplierConfig(2, 3)

    val modulus = PseudoMersennePrime(64, 59)
    val subMults = MultiplierConfig(17, 24)

    val p = modulus.p
    val M = modulus.value
    val maxInput = M - 1

    val randomInputs = (1 to 1 << 10) map (_ => (Util.randomBigInt(M), Util.randomBigInt(M)))
    val oneBitHot = (0 until p * 2) map (i => (BigInt(1) << (i % p), BigInt(1) << (i / p)))
    val oneBitHotSwapped = oneBitHot map (_.swap)
    val maxInputs: Seq[(BigInt, BigInt)] = Seq((0, 0), (maxInput, maxInput), (2, M / 2 + 1))
    val inputs: Seq[(BigInt, BigInt)] = oneBitHot ++ oneBitHotSwapped ++ maxInputs ++ randomInputs

    val compiled = SimConfig.compile(new MersennePrimeModMultiplier(modulus, subMults, reduceMod2M = false))
    compiled.doSim { dut: MersennePrimeModMultiplier =>

      val test = fork {

        dut.io.cmd.opA #= 0
        dut.io.cmd.opB #= 0
        dut.io.enable #= false

        dut.clockDomain.disassertReset()
        sleep(100)
        dut.clockDomain.assertReset()
        sleep(100)
        dut.clockDomain.disassertReset()
        sleep(100)

        dut.clockDomain.forkStimulus(10)

        dut.clockDomain.waitActiveEdge()

        val it = inputs.iterator

        while (!it.isEmpty) {

          val (a, b) = it.next()

          dut.io.cmd.opA #= a
          dut.io.cmd.opB #= b
          dut.io.enable #= true

          var cnt = 0
          while (cnt < dut.latency) {
            dut.clockDomain.waitSampling();
            sleep(0)
            dut.io.cmd.opA #= 0
            dut.io.cmd.opB #= 0

            cnt += 1
          }

          assert(dut.io.rsp.toBigInt < M, "Result not properly reduced.")
          assert(dut.io.rsp.toBigInt < BigInt(2).pow(p), "")

          assertResult(a * b % M) {
            dut.io.rsp.toBigInt
          }
        }
      }

      test.join()
    }
  }
}