package arithmetic

import org.scalatest.FlatSpec
import spinal.core.sim._
import spinal.core._
import spinal.lib.LatencyAnalysis
import spinal.sim._

import scala.collection.mutable
import scala.util.Random

class AdderTreeSpec extends FlatSpec {

  class Dut extends Component {
    val io = new Bundle {
      val a = in UInt (8 bits)
      val b = in UInt (0 bits)
      val c = in UInt (2 bits)
      val d = in UInt (8 bits)
      val output = out UInt (10 bits)
      val enablePipeline = in Bool
      val v = in Vec(UInt(8 bits), 16)
      val outputPipelined = out UInt (8+4 bits)
    }

    import io._

    output := AdderTree.adderTree(List(a, b, c, d))
    outputPipelined := AdderTree.adderTree(v, log2Up(v.length), enable = enablePipeline)

    val latency = LatencyAnalysis(v(0), outputPipelined)
  }

  val compiled = SimConfig.compile(SpinalVerilog(new Dut()))

  "An adder tree" should "calculate correct sums" in {
    compiled.doSim(dut => {

      import dut.io._
      val inputs = List(a, b, c, d)
      val maxStimuli = List(inputs map (i => i.maxValue))
      val randomStimuli = (1 to 32) map (_ =>
        inputs map (i =>
          BigInt(Random.nextInt(i.maxValue.toInt + 1))
          ))
      val stimuli = maxStimuli ++ randomStimuli
      val iter = stimuli iterator


      val test = repeatSim(stimuli.length) {
        val stimuli = iter.next()

        val sum = stimuli.sum
        (inputs, stimuli).zipped.foreach(_ #= _)

        sleep(1)
        assertResult(sum) {
          dut.io.output.toBigInt
        }
        ()
      }
    })
  }
  it should "be properly pipelined" in {
    compiled.doSim(dut => {

      import dut.io._

      v.foreach(_ #= 0)
      enablePipeline #= false

      dut.clockDomain.assertReset()
      sleep(10)
      dut.clockDomain.disassertReset()
      sleep(10)
      dut.clockDomain.forkStimulus(10)

      val responseQueue = mutable.Queue.empty[BigInt]

      var cnt = 0
      while (cnt < 1024) {
        val enable = Random.nextBoolean()
        enablePipeline #= enable

        if (enable) {
          val stimuli = v.map(x => BigInt(x.getWidth, Random))
          responseQueue.enqueue(stimuli sum)
          (v, stimuli).zipped foreach (_ #= _)

          cnt += 1
        }
        dut.clockDomain.waitSampling()
        sleep(0)

        assert(responseQueue.size <= dut.latency)

        if (responseQueue.size == dut.latency && enable) {
          assertResult(responseQueue.dequeue())(outputPipelined.toBigInt)
          cnt += 1
        }
      }
    })
  }
}
