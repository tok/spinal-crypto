package arithmetic

import org.scalatest.FlatSpec
import spinal.core._
import spinal.core.sim._


class KaratsubaMultiplierSpec extends FlatSpec {

  val bitWidth = 16

  val compiled = SimConfig.compile(new KaratsubaMultiplier(bitWidth))

  "A karatsuba multiplier" should "calculate correct products" in {

    val max = (BigInt(1) << bitWidth) - 1
    val half = (BigInt(1) << bitWidth / 2) - 1
    val inputs = List[(BigInt, BigInt)]((0, 0), (0, 1), (0, max), (max, 0), (max, 1),
      (1, max), (1, max), (2, max), (max, max),
      (half, half)
    )

    compiled.doSim { dut: KaratsubaMultiplier =>

      val test = fork {

        dut.clockDomain.assertReset()
        sleep(10)
        dut.clockDomain.disassertReset()
        sleep(10)
        dut.clockDomain.forkStimulus(10)

        val it = inputs.iterator

        while (!it.isEmpty) {

          val (a, b) = it.next()

          dut.clockDomain.waitRisingEdge

          dut.io.cmd.opA #= a
          dut.io.cmd.opB #= b
          dut.io.enable #= true

          var cnt = 0
          while (cnt < dut.latency) {
            dut.clockDomain.waitSampling()
            sleep(0)
            cnt += 1
          }

          val mask = (BigInt(1) << 2 * bitWidth) - 1
          assert(dut.io.rsp.toBigInt == ((a * b) & mask))
        }
      }

      test.join()
    }
  }
}
