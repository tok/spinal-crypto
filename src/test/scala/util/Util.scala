package util

import scala.annotation.tailrec
import scala.util.Random

object Util {

  /** Generate a random integer in the range [0, upperBound).
    *
    * @param upperBound
    * @return A BigInt smaller than `upperBound`.
    */
  @tailrec
  def randomBigInt(upperBound: BigInt): BigInt = {
    val r = BigInt(upperBound.bitLength, Random)
    if(r < upperBound)
      r
    else
      randomBigInt(upperBound)
  }

}
