package symmetric

import org.scalatest.FlatSpec

import spinal.core.sim._
import spinal.core._
import spinal.sim._

class ChaChaSpec extends FlatSpec {
  "A ChaCha core's outputs" should "be consistent with test vectors" in {
    val compiled = SimConfig.compile(new ChaCha())
    compiled.doSim(dut => {

      val test = fork {
        dut.clockDomain.assertReset()
        sleep(10)
        dut.clockDomain.disassertReset()
        sleep(10)
        dut.clockDomain.forkStimulus(10)

        import dut.io.cmd

        cmd.doubleRounds #= 10
        cmd.counter #= 0
        cmd.nonce #= BigInt("000000090000004a00000000", 16) //B"96'x000000090000004a00000000"
        cmd.key #= BigInt("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f", 16) //B"256'x000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"

        val expectedOutput = BigInt("10f1e7e4d13b5915500fdd1fa32071c4c7d1f4c733c068030422aa9ac3d46c4ed2826446079faa0914c2d705d98b02a2b5129cd1de164eb9cbd083e8a2503c4e", 16) //B"512'x10f1e7e4d13b5915500fdd1fa32071c4c7d1f4c733c068030422aa9ac3d46c4ed2826446079faa0914c2d705d98b02a2b5129cd1de164eb9cbd083e8a2503c4e"

        cmd.valid #= true
        dut.io.rsp.ready #= true

        dut.clockDomain.waitActiveEdge(); sleep(0)

        while (!dut.io.rsp.valid.toBoolean) {
          dut.clockDomain.waitActiveEdge(); sleep(0)
          println(dut.io.rsp.payload.toBigInt)
        }

        assertResult(expectedOutput) {
          dut.io.rsp.payload.toBigInt
        }

        ()
      }

      test.join()
    })
  }

  "A ChaCha permutation core's outputs" should "be consistent with test vectors" in {
    val compiled = SimConfig.compile(new ChaChaPermutation())
    compiled.doSim(dut => {

      val test = fork {
        dut.clockDomain.assertReset()
        sleep(10)
        dut.clockDomain.disassertReset()
        sleep(10)
        dut.clockDomain.forkStimulus(10)

        import dut.io.{cmd, rsp}
        cmd.payload.doubleRounds #= 10

        val inputs = List(
          0x61707865, 0x3320646e, 0x79622d32, 0x6b206574,
          0x03020100, 0x07060504, 0x0b0a0908, 0x0f0e0d0c,
          0x13121110, 0x17161514, 0x1b1a1918, 0x1f1e1d1c,
          0x00000001, 0x09000000, 0x4a000000, 0x00000000)

        val cipher = List(
          0xe4e7f110, 0x15593bd1, 0x1fdd0f50, 0xc47120a3,
          0xc7f4d1c7, 0x0368c033, 0x9aaa2204, 0x4e6cd4c3,
          0x466482d2, 0x09aa9f07, 0x05d7c214, 0xa2028bd9,
          0xd19c12b5, 0xb94e16de, 0xe883d0cb, 0x4e3c50a2)

        val expectedOutputs = (cipher, inputs).zipped map (_ - _)

        // Apply stimuli.
        (cmd.payload.data, inputs).zipped foreach (_ #= _)

        cmd.valid #= true
        rsp.ready #= true

        while (!rsp.valid.toBoolean) {
          dut.clockDomain.waitActiveEdge(); sleep(0)
        }
        val outputs = rsp.payload map (_.toLong) map (_.toInt)

        assertResult(expectedOutputs) {
          outputs
        }

        ()
      }

      test.join()
    })
  }
}
