package curve25519

import org.scalatest.FlatSpec
import spinal.core._
import spinal.core.sim._

import scala.util.Random

class Curve25519Spec extends FlatSpec {

  "A curve25519 core" should "output correct result" in {

    val compiled = SimConfig.compile(new Curve25519Core)

    compiled.doSim(dut => {

      import dut.io.{cmd, rsp}

      cmd.valid #= false
      rsp.ready #= false

      dut.clockDomain.assertReset()
      sleep(100)
      dut.clockDomain.disassertReset()
      sleep(100)

      dut.clockDomain.forkStimulus(10)
      dut.clockDomain.waitActiveEdge()
      sleep(0)

      var ticks = 0
      val clockCount = fork {
        while(true) {
          dut.clockDomain.waitActiveEdge()
          sleep(0)
          ticks += 1
        }
      }

      var cnt = 0
      while(cnt < 16) {

        while (!cmd.ready.toBoolean) {
          dut.clockDomain.waitActiveEdge()
          sleep(0)
        }

        val basePoint = 9
        val secretKey = BigInt(255, Random)
        val lambda = BigInt(255, Random)

        cmd.basePoint #= basePoint
        cmd.secretKey #= secretKey
        cmd.lambda #= lambda
        cmd.valid #= true
        dut.clockDomain.waitActiveEdge()
        sleep(0)
        cmd.valid #= false
        rsp.ready #= true

        while (!rsp.valid.toBoolean) {
          dut.clockDomain.waitActiveEdge()
          sleep(0)
        }

        println(rsp.payload.toBigInt)
        val expected = GoldenModel.montgomeryLadder(secretKey, lambda, basePoint)
        assertResult(expected) {
          rsp.payload.toBigInt
        }

        cnt += 1
        println("Ticks = " + ticks)
      }
      ()
    })

  }

}


private object GoldenModel {

  val Q = BigInt(2).pow(255) - 19
  val L = BigInt(2).pow(252) + BigInt("27742317777372353535851937790883648493")
  val Q2 = 2 * Q

  def inv(x: BigInt) = x.modInverse(Q)

  def inv2q(x: BigInt) = x.modPow(Q - 2, Q2)

  val d = -121665 * inv(121666)
  val I = BigInt(2).modPow((Q - 1) / 4, Q)

  def cswap[T](a: T, b: T, cond: Boolean): (T, T) = {
    cond match {
      case true => (b, a)
      case false => (a, b)
    }
  }

  /**
    * Performs scalar multiplication of `basePoint` and `secret`.
    * @param secret
    * @param lambda For randomized projective coordinates.
    * @param basePoint
    * @return
    */
  def montgomeryLadder(secret: BigInt, lambda: BigInt, basePoint: BigInt): BigInt = {

    require(secret < BigInt(2).pow(256))

    val s = secret.clearBit(255).setBit(254).clearBit(0).clearBit(1).clearBit(2)

    val x1 = lambda * basePoint % Q2
    var x2 = lambda
    var z2 = BigInt(0)
    var x3 = lambda * basePoint % Q2
    var z3 = lambda

    for (i <- 255 downto 0) {
      val c = ((s >> i) & 1) ^ ((s >> i + 1) & 1)
      val (_x2, _x3) = cswap(x2, x3, c==1)
      val (_z2, _z3) = cswap(z2, z3, c==1)
      x2 = _x2
      x3 = _x3
      z2 = _z2
      z3 = _z3


      val t1 = (x2 + z2) % Q2
      val t2 = (x2 - z2) % Q2
      val t3 = (x3 + z3) % Q2
      val t4 = (x3 - z3) % Q2
      val t6 = (t1 * t1) % Q2
      val t7 = (t2 * t2) % Q2
      val t5 = (t6 - t7) % Q2
      val t8 = (t4 * t1) % Q2
      val t9 = (t3 * t2) % Q2
      val t10 = (t8 + t9) % Q2
      val t11 = (t8 - t9) % Q2
      x3 = (lambda * t10 * t10) % Q2
      val t12 = (t11 * t11) % Q2
      val t13 = (121666 * t5) % Q2
      x2 = (t6 * t7) % Q2
      val t14 = (t7 + t13) % Q2
      z3 = (x1 * t12) % Q2
      z2 = (t5 * t14) % Q2


    }
    z2 = inv2q(z2)
    var xq = (x2 * z2) % Q2
    xq = xq % Q
    xq
  }

}