name := "crypto"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "3.0.4" % "test",
  "com.github.spinalhdl" % "spinalhdl-core_2.11" % "1.1.2",
  "com.github.spinalhdl" % "spinalhdl-lib_2.11" % "1.1.2",
  "com.github.spinalhdl" % "spinalhdl-sim_2.11" % "1.1.2"
)

// For spinal-sim

fork := true
addCompilerPlugin(
  "org.scala-lang.plugins" % "scala-continuations-plugin_2.11.6" % "1.0.2")

libraryDependencies += "org.scala-lang.plugins" %% "scala-continuations-library" % "1.0.2"

scalacOptions += "-P:continuations:enable"
