// Generator : SpinalHDL v0.10.16    git head : e6beb115088ddeefb17ddad77d79e15456c7c1af
// Date      : 04/11/2017, 18:52:59
// Component : FSMTest


module FSMTest
( 
  output  output_1 
);

  wire  _1;
  assign output_1 = _1;
  assign _1 = 1'b0;
endmodule

