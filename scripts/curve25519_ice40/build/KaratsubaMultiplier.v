// Generator : SpinalHDL v0.11.0    git head : 70060064f983e4ff0ba1dd1a886ec515cc842b0c
// Date      : 26/11/2017, 21:44:12
// Component : KaratsubaMultiplier


module BasicMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [15:0] io_cmd_payload_opA,
      input  [15:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [31:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_1;
  wire  zz_2;
  reg  zz_3;
  reg [31:0] zz_4;
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! zz_2)) || io_rsp_ready);
  assign zz_2 = zz_3;
  assign io_rsp_valid = zz_2;
  assign io_rsp_payload = zz_4;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_3 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_3 <= io_cmd_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_4 <= (io_cmd_payload_opA * io_cmd_payload_opB);
    end
  end

endmodule


//BasicMultiplier_1 remplaced by BasicMultiplier


//BasicMultiplier_2 remplaced by BasicMultiplier

module StreamFork (
      input   io_input_valid,
      output  io_input_ready,
      input  [31:0] io_input_payload_opA,
      input  [31:0] io_input_payload_opB,
      output  io_outputs_0_valid,
      input   io_outputs_0_ready,
      output [31:0] io_outputs_0_payload_opA,
      output [31:0] io_outputs_0_payload_opB,
      output  io_outputs_1_valid,
      input   io_outputs_1_ready,
      output [31:0] io_outputs_1_payload_opA,
      output [31:0] io_outputs_1_payload_opB,
      output  io_outputs_2_valid,
      input   io_outputs_2_ready,
      output [31:0] io_outputs_2_payload_opA,
      output [31:0] io_outputs_2_payload_opB,
      input   clk,
      input   reset);
  wire  zz_1;
  wire  zz_2;
  wire  zz_3;
  reg  zz_4;
  reg  linkEnable_0;
  reg  linkEnable_1;
  reg  linkEnable_2;
  assign io_outputs_0_valid = zz_1;
  assign io_outputs_1_valid = zz_2;
  assign io_outputs_2_valid = zz_3;
  assign io_input_ready = zz_4;
  always @ (io_outputs_0_ready or linkEnable_0 or io_outputs_1_ready or linkEnable_1 or io_outputs_2_ready or linkEnable_2)
  begin
    zz_4 = 1'b1;
    if(((! io_outputs_0_ready) && linkEnable_0))begin
      zz_4 = 1'b0;
    end
    if(((! io_outputs_1_ready) && linkEnable_1))begin
      zz_4 = 1'b0;
    end
    if(((! io_outputs_2_ready) && linkEnable_2))begin
      zz_4 = 1'b0;
    end
  end

  assign zz_1 = (io_input_valid && linkEnable_0);
  assign io_outputs_0_payload_opA = io_input_payload_opA;
  assign io_outputs_0_payload_opB = io_input_payload_opB;
  assign zz_2 = (io_input_valid && linkEnable_1);
  assign io_outputs_1_payload_opA = io_input_payload_opA;
  assign io_outputs_1_payload_opB = io_input_payload_opB;
  assign zz_3 = (io_input_valid && linkEnable_2);
  assign io_outputs_2_payload_opA = io_input_payload_opA;
  assign io_outputs_2_payload_opB = io_input_payload_opB;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      linkEnable_0 <= 1'b1;
      linkEnable_1 <= 1'b1;
      linkEnable_2 <= 1'b1;
    end else begin
      if((zz_1 && io_outputs_0_ready))begin
        linkEnable_0 <= 1'b0;
      end
      if((zz_2 && io_outputs_1_ready))begin
        linkEnable_1 <= 1'b0;
      end
      if((zz_3 && io_outputs_2_ready))begin
        linkEnable_2 <= 1'b0;
      end
      if(zz_4)begin
        linkEnable_0 <= 1'b1;
        linkEnable_1 <= 1'b1;
        linkEnable_2 <= 1'b1;
      end
    end
  end

endmodule

module KaratsubaMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [31:0] io_cmd_payload_opA,
      input  [31:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [63:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_4;
  wire  zz_5;
  wire [31:0] zz_6;
  wire  zz_7;
  wire  zz_8;
  wire [31:0] zz_9;
  wire  zz_10;
  wire  zz_11;
  wire [31:0] zz_12;
  wire  zz_13;
  wire  zz_14;
  wire [31:0] zz_15;
  wire [31:0] zz_16;
  wire  zz_17;
  wire [31:0] zz_18;
  wire [31:0] zz_19;
  wire  zz_20;
  wire [31:0] zz_21;
  wire [31:0] zz_22;
  wire [16:0] zz_23;
  wire [16:0] zz_24;
  wire [15:0] zz_25;
  wire [16:0] zz_26;
  wire [16:0] zz_27;
  wire [15:0] zz_28;
  wire [47:0] zz_29;
  wire [63:0] zz_30;
  wire [63:0] zz_31;
  wire [63:0] zz_32;
  wire [47:0] zz_33;
  wire [47:0] zz_34;
  wire [47:0] zz_35;
  wire [47:0] zz_36;
  wire [63:0] zz_37;
  wire [15:0] al;
  wire [15:0] ah;
  wire [15:0] bl;
  wire [15:0] bh;
  wire [16:0] zz_1;
  wire  borrowA;
  wire [15:0] absDiff_a;
  wire [16:0] zz_2;
  wire  borrowB;
  wire [15:0] absDiff_b;
  wire  subMultJoin_valid;
  wire  subMultJoin_ready;
  wire  zz_3;
  wire [15:0] cmdL_opA;
  wire [15:0] cmdL_opB;
  wire [15:0] cmdM_opA;
  wire [15:0] cmdM_opB;
  wire [15:0] cmdH_opA;
  wire [15:0] cmdH_opB;
  wire [47:0] m_resized;
  wire [47:0] m_hat;
  wire [63:0] w;
  assign zz_23 = al;
  assign zz_24 = ah;
  assign zz_25 = borrowA;
  assign zz_26 = bl;
  assign zz_27 = bh;
  assign zz_28 = borrowB;
  assign zz_29 = ((48'b000000000000000000000000000000000000000000000000) - m_resized);
  assign zz_30 = (zz_31 + zz_32);
  assign zz_31 = (zz_12 <<< 32);
  assign zz_32 = (zz_33 <<< 16);
  assign zz_33 = (zz_34 + zz_36);
  assign zz_34 = (m_hat + zz_35);
  assign zz_35 = zz_12;
  assign zz_36 = zz_6;
  assign zz_37 = zz_6;
  BasicMultiplier multL ( 
    .io_cmd_valid(zz_14),
    .io_cmd_ready(zz_4),
    .io_cmd_payload_opA(cmdL_opA),
    .io_cmd_payload_opB(cmdL_opB),
    .io_rsp_valid(zz_5),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_6),
    .clk(clk),
    .reset(reset) 
  );
  BasicMultiplier multM ( 
    .io_cmd_valid(zz_17),
    .io_cmd_ready(zz_7),
    .io_cmd_payload_opA(cmdM_opA),
    .io_cmd_payload_opB(cmdM_opB),
    .io_rsp_valid(zz_8),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_9),
    .clk(clk),
    .reset(reset) 
  );
  BasicMultiplier multH ( 
    .io_cmd_valid(zz_20),
    .io_cmd_ready(zz_10),
    .io_cmd_payload_opA(cmdH_opA),
    .io_cmd_payload_opB(cmdH_opB),
    .io_rsp_valid(zz_11),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_12),
    .clk(clk),
    .reset(reset) 
  );
  StreamFork streamFork_1 ( 
    .io_input_valid(io_cmd_valid),
    .io_input_ready(zz_13),
    .io_input_payload_opA(io_cmd_payload_opA),
    .io_input_payload_opB(io_cmd_payload_opB),
    .io_outputs_0_valid(zz_14),
    .io_outputs_0_ready(zz_4),
    .io_outputs_0_payload_opA(zz_15),
    .io_outputs_0_payload_opB(zz_16),
    .io_outputs_1_valid(zz_17),
    .io_outputs_1_ready(zz_7),
    .io_outputs_1_payload_opA(zz_18),
    .io_outputs_1_payload_opB(zz_19),
    .io_outputs_2_valid(zz_20),
    .io_outputs_2_ready(zz_10),
    .io_outputs_2_payload_opA(zz_21),
    .io_outputs_2_payload_opB(zz_22),
    .clk(clk),
    .reset(reset) 
  );
  assign al = io_cmd_payload_opA[15 : 0];
  assign ah = io_cmd_payload_opA[31 : 16];
  assign bl = io_cmd_payload_opB[15 : 0];
  assign bh = io_cmd_payload_opB[31 : 16];
  assign zz_1 = (zz_23 - zz_24);
  assign borrowA = zz_1[16];
  assign absDiff_a = ((zz_1[15 : 0] ^ {borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,borrowA}}}}}}}}}}}}}}}) + zz_25);
  assign zz_2 = (zz_26 - zz_27);
  assign borrowB = zz_2[16];
  assign absDiff_b = ((zz_2[15 : 0] ^ {borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,borrowB}}}}}}}}}}}}}}}) + zz_28);
  assign zz_3 = (subMultJoin_valid && subMultJoin_ready);
  assign subMultJoin_valid = ((zz_5 && zz_8) && zz_11);
  assign io_cmd_ready = zz_13;
  assign cmdL_opA = al;
  assign cmdL_opB = bl;
  assign cmdM_opA = absDiff_a;
  assign cmdM_opB = absDiff_b;
  assign cmdH_opA = ah;
  assign cmdH_opB = bh;
  assign m_resized = zz_9;
  assign m_hat = ((borrowA ^ borrowB) ? m_resized : zz_29);
  assign w = (zz_30 + zz_37);
  assign subMultJoin_ready = io_rsp_ready;
  assign io_rsp_valid = subMultJoin_valid;
  assign io_rsp_payload = w;
endmodule

