// Generator : SpinalHDL v0.10.16    git head : e6beb115088ddeefb17ddad77d79e15456c7c1af
// Date      : 04/11/2017, 19:39:38
// Component : Blake


module Blake
( 
  input   io_init,
  input   io_cmd_valid,
  output reg  io_cmd_ready,
  input   io_cmd_payload_last,
  input  [7:0] io_cmd_payload_fragment_msg,
  output  io_rsp_valid,
  output [255:0] io_rsp_payload_digest 
);

  wire [255:0] _1;
  wire  _2;
  wire  _3;
  wire  _4;
  assign io_rsp_valid = _3;
  assign io_rsp_payload_digest = _1;
  assign _1 = (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
  assign _2 = 1'b0;
  assign _3 = 1'b0;
  assign _4 = 1'b1;
  always @ (io_cmd_valid or _2 or _4)
  begin
    io_cmd_ready = _2;
    if(io_cmd_valid)begin
      io_cmd_ready = _4;
    end
  end

endmodule

