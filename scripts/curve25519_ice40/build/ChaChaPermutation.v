// Generator : SpinalHDL v0.10.16    git head : e6beb115088ddeefb17ddad77d79e15456c7c1af
// Date      : 23/10/2017, 20:24:02
// Component : ChaChaPermutation


`define fsm_enumDefinition_binary_sequancial_type [1:0]
`define fsm_enumDefinition_binary_sequancial_boot 'b00
`define fsm_enumDefinition_binary_sequancial_fsm_sIdle 'b01
`define fsm_enumDefinition_binary_sequancial_fsm_sComputing 'b10
`define fsm_enumDefinition_binary_sequancial_fsm_sValid 'b11

module ChaChaRound
( 
  input  [31:0] input_0,
  input  [31:0] input_1,
  input  [31:0] input_2,
  input  [31:0] input_3,
  input  [31:0] input_4,
  input  [31:0] input_5,
  input  [31:0] input_6,
  input  [31:0] input_7,
  input  [31:0] input_8,
  input  [31:0] input_9,
  input  [31:0] input_10,
  input  [31:0] input_11,
  input  [31:0] input_12,
  input  [31:0] input_13,
  input  [31:0] input_14,
  input  [31:0] input_15,
  output [31:0] rsp_0,
  output [31:0] rsp_1,
  output [31:0] rsp_2,
  output [31:0] rsp_3,
  output [31:0] rsp_4,
  output [31:0] rsp_5,
  output [31:0] rsp_6,
  output [31:0] rsp_7,
  output [31:0] rsp_8,
  output [31:0] rsp_9,
  output [31:0] rsp_10,
  output [31:0] rsp_11,
  output [31:0] rsp_12,
  output [31:0] rsp_13,
  output [31:0] rsp_14,
  output [31:0] rsp_15 
);

  wire [31:0] half_0;
  wire [31:0] half_1;
  wire [31:0] half_2;
  wire [31:0] half_3;
  wire [31:0] half_4;
  wire [31:0] half_5;
  wire [31:0] half_6;
  wire [31:0] half_7;
  wire [31:0] half_8;
  wire [31:0] half_9;
  wire [31:0] half_10;
  wire [31:0] half_11;
  wire [31:0] half_12;
  wire [31:0] half_13;
  wire [31:0] half_14;
  wire [31:0] half_15;
  wire [31:0] _1;
  wire [31:0] _2;
  wire [15:0] _3;
  wire [15:0] _4;
  wire [31:0] _5;
  wire [31:0] _6;
  wire [31:0] _7;
  wire [19:0] _8;
  wire [11:0] _9;
  wire [31:0] _10;
  wire [31:0] _11;
  wire [31:0] _12;
  wire [23:0] _13;
  wire [7:0] _14;
  wire [31:0] _15;
  wire [31:0] _16;
  wire [31:0] _17;
  wire [24:0] _18;
  wire [6:0] _19;
  wire [31:0] _20;
  wire [31:0] _21;
  wire [31:0] _22;
  wire [15:0] _23;
  wire [15:0] _24;
  wire [31:0] _25;
  wire [31:0] _26;
  wire [31:0] _27;
  wire [19:0] _28;
  wire [11:0] _29;
  wire [31:0] _30;
  wire [31:0] _31;
  wire [31:0] _32;
  wire [23:0] _33;
  wire [7:0] _34;
  wire [31:0] _35;
  wire [31:0] _36;
  wire [31:0] _37;
  wire [24:0] _38;
  wire [6:0] _39;
  wire [31:0] _40;
  wire [31:0] _41;
  wire [31:0] _42;
  wire [15:0] _43;
  wire [15:0] _44;
  wire [31:0] _45;
  wire [31:0] _46;
  wire [31:0] _47;
  wire [19:0] _48;
  wire [11:0] _49;
  wire [31:0] _50;
  wire [31:0] _51;
  wire [31:0] _52;
  wire [23:0] _53;
  wire [7:0] _54;
  wire [31:0] _55;
  wire [31:0] _56;
  wire [31:0] _57;
  wire [24:0] _58;
  wire [6:0] _59;
  wire [31:0] _60;
  wire [31:0] _61;
  wire [31:0] _62;
  wire [15:0] _63;
  wire [15:0] _64;
  wire [31:0] _65;
  wire [31:0] _66;
  wire [31:0] _67;
  wire [19:0] _68;
  wire [11:0] _69;
  wire [31:0] _70;
  wire [31:0] _71;
  wire [31:0] _72;
  wire [23:0] _73;
  wire [7:0] _74;
  wire [31:0] _75;
  wire [31:0] _76;
  wire [31:0] _77;
  wire [24:0] _78;
  wire [6:0] _79;
  wire [31:0] _80;
  wire [31:0] _81;
  wire [31:0] _82;
  wire [15:0] _83;
  wire [15:0] _84;
  wire [31:0] _85;
  wire [31:0] _86;
  wire [31:0] _87;
  wire [19:0] _88;
  wire [11:0] _89;
  wire [31:0] _90;
  wire [31:0] _91;
  wire [31:0] _92;
  wire [23:0] _93;
  wire [7:0] _94;
  wire [31:0] _95;
  wire [31:0] _96;
  wire [31:0] _97;
  wire [24:0] _98;
  wire [6:0] _99;
  wire [31:0] _100;
  wire [31:0] _101;
  wire [31:0] _102;
  wire [15:0] _103;
  wire [15:0] _104;
  wire [31:0] _105;
  wire [31:0] _106;
  wire [31:0] _107;
  wire [19:0] _108;
  wire [11:0] _109;
  wire [31:0] _110;
  wire [31:0] _111;
  wire [31:0] _112;
  wire [23:0] _113;
  wire [7:0] _114;
  wire [31:0] _115;
  wire [31:0] _116;
  wire [31:0] _117;
  wire [24:0] _118;
  wire [6:0] _119;
  wire [31:0] _120;
  wire [31:0] _121;
  wire [31:0] _122;
  wire [15:0] _123;
  wire [15:0] _124;
  wire [31:0] _125;
  wire [31:0] _126;
  wire [31:0] _127;
  wire [19:0] _128;
  wire [11:0] _129;
  wire [31:0] _130;
  wire [31:0] _131;
  wire [31:0] _132;
  wire [23:0] _133;
  wire [7:0] _134;
  wire [31:0] _135;
  wire [31:0] _136;
  wire [31:0] _137;
  wire [24:0] _138;
  wire [6:0] _139;
  wire [31:0] _140;
  wire [31:0] _141;
  wire [31:0] _142;
  wire [15:0] _143;
  wire [15:0] _144;
  wire [31:0] _145;
  wire [31:0] _146;
  wire [31:0] _147;
  wire [19:0] _148;
  wire [11:0] _149;
  wire [31:0] _150;
  wire [31:0] _151;
  wire [31:0] _152;
  wire [23:0] _153;
  wire [7:0] _154;
  wire [31:0] _155;
  wire [31:0] _156;
  wire [31:0] _157;
  wire [24:0] _158;
  wire [6:0] _159;
  wire [31:0] _160;
  assign rsp_0 = _91;
  assign rsp_1 = _111;
  assign rsp_2 = _131;
  assign rsp_3 = _151;
  assign rsp_4 = _160;
  assign rsp_5 = _100;
  assign rsp_6 = _120;
  assign rsp_7 = _140;
  assign rsp_8 = _136;
  assign rsp_9 = _156;
  assign rsp_10 = _96;
  assign rsp_11 = _116;
  assign rsp_12 = _115;
  assign rsp_13 = _135;
  assign rsp_14 = _155;
  assign rsp_15 = _95;
  assign half_0 = _11;
  assign half_1 = _31;
  assign half_2 = _51;
  assign half_3 = _71;
  assign half_4 = _20;
  assign half_5 = _40;
  assign half_6 = _60;
  assign half_7 = _80;
  assign half_8 = _16;
  assign half_9 = _36;
  assign half_10 = _56;
  assign half_11 = _76;
  assign half_12 = _15;
  assign half_13 = _35;
  assign half_14 = _55;
  assign half_15 = _75;
  assign _1 = (input_0 + input_4);
  assign _2 = (_1 ^ input_12);
  assign _3 = _2[15 : 0];
  assign _4 = _2[31 : 16];
  assign _5 = {_3,_4};
  assign _6 = (input_8 + _5);
  assign _7 = (input_4 ^ _6);
  assign _8 = _7[19 : 0];
  assign _9 = _7[31 : 20];
  assign _10 = {_8,_9};
  assign _11 = (_1 + _10);
  assign _12 = (_11 ^ _5);
  assign _13 = _12[23 : 0];
  assign _14 = _12[31 : 24];
  assign _15 = {_13,_14};
  assign _16 = (_6 + _15);
  assign _17 = (_10 ^ _16);
  assign _18 = _17[24 : 0];
  assign _19 = _17[31 : 25];
  assign _20 = {_18,_19};
  assign _21 = (input_1 + input_5);
  assign _22 = (_21 ^ input_13);
  assign _23 = _22[15 : 0];
  assign _24 = _22[31 : 16];
  assign _25 = {_23,_24};
  assign _26 = (input_9 + _25);
  assign _27 = (input_5 ^ _26);
  assign _28 = _27[19 : 0];
  assign _29 = _27[31 : 20];
  assign _30 = {_28,_29};
  assign _31 = (_21 + _30);
  assign _32 = (_31 ^ _25);
  assign _33 = _32[23 : 0];
  assign _34 = _32[31 : 24];
  assign _35 = {_33,_34};
  assign _36 = (_26 + _35);
  assign _37 = (_30 ^ _36);
  assign _38 = _37[24 : 0];
  assign _39 = _37[31 : 25];
  assign _40 = {_38,_39};
  assign _41 = (input_2 + input_6);
  assign _42 = (_41 ^ input_14);
  assign _43 = _42[15 : 0];
  assign _44 = _42[31 : 16];
  assign _45 = {_43,_44};
  assign _46 = (input_10 + _45);
  assign _47 = (input_6 ^ _46);
  assign _48 = _47[19 : 0];
  assign _49 = _47[31 : 20];
  assign _50 = {_48,_49};
  assign _51 = (_41 + _50);
  assign _52 = (_51 ^ _45);
  assign _53 = _52[23 : 0];
  assign _54 = _52[31 : 24];
  assign _55 = {_53,_54};
  assign _56 = (_46 + _55);
  assign _57 = (_50 ^ _56);
  assign _58 = _57[24 : 0];
  assign _59 = _57[31 : 25];
  assign _60 = {_58,_59};
  assign _61 = (input_3 + input_7);
  assign _62 = (_61 ^ input_15);
  assign _63 = _62[15 : 0];
  assign _64 = _62[31 : 16];
  assign _65 = {_63,_64};
  assign _66 = (input_11 + _65);
  assign _67 = (input_7 ^ _66);
  assign _68 = _67[19 : 0];
  assign _69 = _67[31 : 20];
  assign _70 = {_68,_69};
  assign _71 = (_61 + _70);
  assign _72 = (_71 ^ _65);
  assign _73 = _72[23 : 0];
  assign _74 = _72[31 : 24];
  assign _75 = {_73,_74};
  assign _76 = (_66 + _75);
  assign _77 = (_70 ^ _76);
  assign _78 = _77[24 : 0];
  assign _79 = _77[31 : 25];
  assign _80 = {_78,_79};
  assign _81 = (half_0 + half_5);
  assign _82 = (_81 ^ half_15);
  assign _83 = _82[15 : 0];
  assign _84 = _82[31 : 16];
  assign _85 = {_83,_84};
  assign _86 = (half_10 + _85);
  assign _87 = (half_5 ^ _86);
  assign _88 = _87[19 : 0];
  assign _89 = _87[31 : 20];
  assign _90 = {_88,_89};
  assign _91 = (_81 + _90);
  assign _92 = (_91 ^ _85);
  assign _93 = _92[23 : 0];
  assign _94 = _92[31 : 24];
  assign _95 = {_93,_94};
  assign _96 = (_86 + _95);
  assign _97 = (_90 ^ _96);
  assign _98 = _97[24 : 0];
  assign _99 = _97[31 : 25];
  assign _100 = {_98,_99};
  assign _101 = (half_1 + half_6);
  assign _102 = (_101 ^ half_12);
  assign _103 = _102[15 : 0];
  assign _104 = _102[31 : 16];
  assign _105 = {_103,_104};
  assign _106 = (half_11 + _105);
  assign _107 = (half_6 ^ _106);
  assign _108 = _107[19 : 0];
  assign _109 = _107[31 : 20];
  assign _110 = {_108,_109};
  assign _111 = (_101 + _110);
  assign _112 = (_111 ^ _105);
  assign _113 = _112[23 : 0];
  assign _114 = _112[31 : 24];
  assign _115 = {_113,_114};
  assign _116 = (_106 + _115);
  assign _117 = (_110 ^ _116);
  assign _118 = _117[24 : 0];
  assign _119 = _117[31 : 25];
  assign _120 = {_118,_119};
  assign _121 = (half_2 + half_7);
  assign _122 = (_121 ^ half_13);
  assign _123 = _122[15 : 0];
  assign _124 = _122[31 : 16];
  assign _125 = {_123,_124};
  assign _126 = (half_8 + _125);
  assign _127 = (half_7 ^ _126);
  assign _128 = _127[19 : 0];
  assign _129 = _127[31 : 20];
  assign _130 = {_128,_129};
  assign _131 = (_121 + _130);
  assign _132 = (_131 ^ _125);
  assign _133 = _132[23 : 0];
  assign _134 = _132[31 : 24];
  assign _135 = {_133,_134};
  assign _136 = (_126 + _135);
  assign _137 = (_130 ^ _136);
  assign _138 = _137[24 : 0];
  assign _139 = _137[31 : 25];
  assign _140 = {_138,_139};
  assign _141 = (half_3 + half_4);
  assign _142 = (_141 ^ half_14);
  assign _143 = _142[15 : 0];
  assign _144 = _142[31 : 16];
  assign _145 = {_143,_144};
  assign _146 = (half_9 + _145);
  assign _147 = (half_4 ^ _146);
  assign _148 = _147[19 : 0];
  assign _149 = _147[31 : 20];
  assign _150 = {_148,_149};
  assign _151 = (_141 + _150);
  assign _152 = (_151 ^ _145);
  assign _153 = _152[23 : 0];
  assign _154 = _152[31 : 24];
  assign _155 = {_153,_154};
  assign _156 = (_146 + _155);
  assign _157 = (_150 ^ _156);
  assign _158 = _157[24 : 0];
  assign _159 = _157[31 : 25];
  assign _160 = {_158,_159};
endmodule

module ChaChaPermutation
( 
  input   io_cmd_valid,
  output reg  io_cmd_ready,
  input  [31:0] io_cmd_payload_data_0,
  input  [31:0] io_cmd_payload_data_1,
  input  [31:0] io_cmd_payload_data_2,
  input  [31:0] io_cmd_payload_data_3,
  input  [31:0] io_cmd_payload_data_4,
  input  [31:0] io_cmd_payload_data_5,
  input  [31:0] io_cmd_payload_data_6,
  input  [31:0] io_cmd_payload_data_7,
  input  [31:0] io_cmd_payload_data_8,
  input  [31:0] io_cmd_payload_data_9,
  input  [31:0] io_cmd_payload_data_10,
  input  [31:0] io_cmd_payload_data_11,
  input  [31:0] io_cmd_payload_data_12,
  input  [31:0] io_cmd_payload_data_13,
  input  [31:0] io_cmd_payload_data_14,
  input  [31:0] io_cmd_payload_data_15,
  input  [3:0] io_cmd_payload_doubleRounds,
  output reg  io_rsp_valid,
  input   io_rsp_ready,
  output reg [31:0] io_rsp_payload_0,
  output reg [31:0] io_rsp_payload_1,
  output reg [31:0] io_rsp_payload_2,
  output reg [31:0] io_rsp_payload_3,
  output reg [31:0] io_rsp_payload_4,
  output reg [31:0] io_rsp_payload_5,
  output reg [31:0] io_rsp_payload_6,
  output reg [31:0] io_rsp_payload_7,
  output reg [31:0] io_rsp_payload_8,
  output reg [31:0] io_rsp_payload_9,
  output reg [31:0] io_rsp_payload_10,
  output reg [31:0] io_rsp_payload_11,
  output reg [31:0] io_rsp_payload_12,
  output reg [31:0] io_rsp_payload_13,
  output reg [31:0] io_rsp_payload_14,
  output reg [31:0] io_rsp_payload_15,
  input   clk,
  input   reset 
);

  wire  _1;
  wire  _2;
  reg [3:0] roundCounter;
  reg [31:0] chachaState_0;
  reg [31:0] chachaState_1;
  reg [31:0] chachaState_2;
  reg [31:0] chachaState_3;
  reg [31:0] chachaState_4;
  reg [31:0] chachaState_5;
  reg [31:0] chachaState_6;
  reg [31:0] chachaState_7;
  reg [31:0] chachaState_8;
  reg [31:0] chachaState_9;
  reg [31:0] chachaState_10;
  reg [31:0] chachaState_11;
  reg [31:0] chachaState_12;
  reg [31:0] chachaState_13;
  reg [31:0] chachaState_14;
  reg [31:0] chachaState_15;
  wire [31:0] _3;
  wire [31:0] _4;
  wire [31:0] _5;
  wire [31:0] _6;
  wire [31:0] _7;
  wire [31:0] _8;
  wire [31:0] _9;
  wire [31:0] _10;
  wire [31:0] _11;
  wire [31:0] _12;
  wire [31:0] _13;
  wire [31:0] _14;
  wire [31:0] _15;
  wire [31:0] _16;
  wire [31:0] _17;
  wire [31:0] _18;
  reg `fsm_enumDefinition_binary_sequancial_type fsm_stateReg;
  reg `fsm_enumDefinition_binary_sequancial_type fsm_stateNext;
  wire `fsm_enumDefinition_binary_sequancial_type _19;
  wire `fsm_enumDefinition_binary_sequancial_type _20;
  wire `fsm_enumDefinition_binary_sequancial_type _21;
  wire `fsm_enumDefinition_binary_sequancial_type _22;
  wire  _23;
  wire  _24;
  wire `fsm_enumDefinition_binary_sequancial_type _25;
  wire `fsm_enumDefinition_binary_sequancial_type _26;
  wire  _27;
  wire [3:0] _28;
  wire  _29;
  wire `fsm_enumDefinition_binary_sequancial_type _30;
  wire `fsm_enumDefinition_binary_sequancial_type _31;
  wire  _32;
  wire  _33;
  wire `fsm_enumDefinition_binary_sequancial_type _34;
  wire `fsm_enumDefinition_binary_sequancial_type _35;
  wire  _36;
  wire [3:0] _37;
  wire [31:0] roundCore_rsp_0;
  reg [31:0] roundCore_input_15;
  wire [31:0] roundCore_rsp_15;
  reg [31:0] roundCore_input_7;
  wire [31:0] roundCore_rsp_7;
  reg [31:0] roundCore_input_13;
  wire [31:0] roundCore_rsp_13;
  reg [31:0] roundCore_input_5;
  wire [31:0] roundCore_rsp_5;
  reg [31:0] roundCore_input_1;
  wire [31:0] roundCore_rsp_1;
  reg [31:0] roundCore_input_12;
  wire [31:0] roundCore_rsp_12;
  reg [31:0] roundCore_input_4;
  wire [31:0] roundCore_rsp_4;
  reg [31:0] roundCore_input_14;
  wire [31:0] roundCore_rsp_14;
  reg [31:0] roundCore_input_6;
  wire [31:0] roundCore_rsp_6;
  reg [31:0] roundCore_input_2;
  wire [31:0] roundCore_rsp_2;
  reg [31:0] roundCore_input_10;
  wire [31:0] roundCore_rsp_10;
  reg [31:0] roundCore_input_8;
  wire [31:0] roundCore_rsp_8;
  reg [31:0] roundCore_input_9;
  wire [31:0] roundCore_rsp_9;
  reg [31:0] roundCore_input_0;
  reg [31:0] roundCore_input_11;
  wire [31:0] roundCore_rsp_11;
  reg [31:0] roundCore_input_3;
  wire [31:0] roundCore_rsp_3;
  ChaChaRound roundCore ( 
    .input_0(roundCore_input_0),
    .input_1(roundCore_input_1),
    .input_2(roundCore_input_2),
    .input_3(roundCore_input_3),
    .input_4(roundCore_input_4),
    .input_5(roundCore_input_5),
    .input_6(roundCore_input_6),
    .input_7(roundCore_input_7),
    .input_8(roundCore_input_8),
    .input_9(roundCore_input_9),
    .input_10(roundCore_input_10),
    .input_11(roundCore_input_11),
    .input_12(roundCore_input_12),
    .input_13(roundCore_input_13),
    .input_14(roundCore_input_14),
    .input_15(roundCore_input_15),
    .rsp_0(roundCore_rsp_0),
    .rsp_1(roundCore_rsp_1),
    .rsp_2(roundCore_rsp_2),
    .rsp_3(roundCore_rsp_3),
    .rsp_4(roundCore_rsp_4),
    .rsp_5(roundCore_rsp_5),
    .rsp_6(roundCore_rsp_6),
    .rsp_7(roundCore_rsp_7),
    .rsp_8(roundCore_rsp_8),
    .rsp_9(roundCore_rsp_9),
    .rsp_10(roundCore_rsp_10),
    .rsp_11(roundCore_rsp_11),
    .rsp_12(roundCore_rsp_12),
    .rsp_13(roundCore_rsp_13),
    .rsp_14(roundCore_rsp_14),
    .rsp_15(roundCore_rsp_15) 
  );
  assign _1 = 1'b0;
  assign _2 = 1'b0;
  assign _3 = (32'b00000000000000000000000000000000);
  assign _4 = (32'b00000000000000000000000000000000);
  assign _5 = (32'b00000000000000000000000000000000);
  assign _6 = (32'b00000000000000000000000000000000);
  assign _7 = (32'b00000000000000000000000000000000);
  assign _8 = (32'b00000000000000000000000000000000);
  assign _9 = (32'b00000000000000000000000000000000);
  assign _10 = (32'b00000000000000000000000000000000);
  assign _11 = (32'b00000000000000000000000000000000);
  assign _12 = (32'b00000000000000000000000000000000);
  assign _13 = (32'b00000000000000000000000000000000);
  assign _14 = (32'b00000000000000000000000000000000);
  assign _15 = (32'b00000000000000000000000000000000);
  assign _16 = (32'b00000000000000000000000000000000);
  assign _17 = (32'b00000000000000000000000000000000);
  assign _18 = (32'b00000000000000000000000000000000);
  assign _19 = `fsm_enumDefinition_binary_sequancial_boot;
  assign _20 = `fsm_enumDefinition_binary_sequancial_fsm_sIdle;
  assign _21 = `fsm_enumDefinition_binary_sequancial_fsm_sIdle;
  assign _22 = `fsm_enumDefinition_binary_sequancial_fsm_sIdle;
  assign _23 = (fsm_stateReg == _22);
  assign _24 = 1'b1;
  assign _25 = `fsm_enumDefinition_binary_sequancial_fsm_sComputing;
  assign _26 = `fsm_enumDefinition_binary_sequancial_fsm_sComputing;
  assign _27 = (fsm_stateReg == _26);
  assign _28 = (roundCounter - (4'b0001));
  assign _29 = (_28 == (4'b0000));
  assign _30 = `fsm_enumDefinition_binary_sequancial_fsm_sValid;
  assign _31 = `fsm_enumDefinition_binary_sequancial_fsm_sValid;
  assign _32 = (fsm_stateReg == _31);
  assign _33 = 1'b1;
  assign _34 = `fsm_enumDefinition_binary_sequancial_fsm_sIdle;
  assign _35 = `fsm_enumDefinition_binary_sequancial_fsm_sIdle;
  assign _36 = ((fsm_stateReg == _20) && (! (fsm_stateNext == _21)));
  assign _37 = (io_cmd_payload_doubleRounds - (4'b0001));
  always @ (_1 or _23 or _24)
  begin
    io_cmd_ready = _1;
    if(_23)begin
      io_cmd_ready = _24;
    end
  end

  always @ (_2 or fsm_stateReg or _33)
  begin
    io_rsp_valid = _2;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_valid = _33;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_0 or fsm_stateReg or _29)
  begin
    io_rsp_payload_0 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_0 = chachaState_0;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_0 = chachaState_0;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_1 or fsm_stateReg or _29)
  begin
    io_rsp_payload_1 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_1 = chachaState_1;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_1 = chachaState_1;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_2 or fsm_stateReg or _29)
  begin
    io_rsp_payload_2 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_2 = chachaState_2;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_2 = chachaState_2;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_3 or fsm_stateReg or _29)
  begin
    io_rsp_payload_3 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_3 = chachaState_3;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_3 = chachaState_3;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_4 or fsm_stateReg or _29)
  begin
    io_rsp_payload_4 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_4 = chachaState_4;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_4 = chachaState_4;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_5 or fsm_stateReg or _29)
  begin
    io_rsp_payload_5 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_5 = chachaState_5;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_5 = chachaState_5;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_6 or fsm_stateReg or _29)
  begin
    io_rsp_payload_6 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_6 = chachaState_6;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_6 = chachaState_6;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_7 or fsm_stateReg or _29)
  begin
    io_rsp_payload_7 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_7 = chachaState_7;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_7 = chachaState_7;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_8 or fsm_stateReg or _29)
  begin
    io_rsp_payload_8 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_8 = chachaState_8;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_8 = chachaState_8;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_9 or fsm_stateReg or _29)
  begin
    io_rsp_payload_9 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_9 = chachaState_9;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_9 = chachaState_9;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_10 or fsm_stateReg or _29)
  begin
    io_rsp_payload_10 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_10 = chachaState_10;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_10 = chachaState_10;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_11 or fsm_stateReg or _29)
  begin
    io_rsp_payload_11 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_11 = chachaState_11;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_11 = chachaState_11;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_12 or fsm_stateReg or _29)
  begin
    io_rsp_payload_12 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_12 = chachaState_12;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_12 = chachaState_12;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_13 or fsm_stateReg or _29)
  begin
    io_rsp_payload_13 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_13 = chachaState_13;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_13 = chachaState_13;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_14 or fsm_stateReg or _29)
  begin
    io_rsp_payload_14 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_14 = chachaState_14;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_14 = chachaState_14;
      end
      default : begin
      end
    endcase
  end

  always @ (chachaState_15 or fsm_stateReg or _29)
  begin
    io_rsp_payload_15 = (32'b00000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          io_rsp_payload_15 = chachaState_15;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        io_rsp_payload_15 = chachaState_15;
      end
      default : begin
      end
    endcase
  end

  always @ (io_cmd_valid or io_rsp_ready or fsm_stateReg or _25 or _29 or _30 or _34 or _35)
  begin
    fsm_stateNext = fsm_stateReg;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
        if(io_cmd_valid)begin
          fsm_stateNext = _25;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        if(_29)begin
          fsm_stateNext = _30;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sValid : begin
        if(io_rsp_ready)begin
          fsm_stateNext = _34;
        end
      end
      default : begin
        fsm_stateNext = _35;
      end
    endcase
  end

  always @ (io_cmd_payload_data_15 or chachaState_15 or _18 or fsm_stateReg or _36)
  begin
    roundCore_input_15 = _18;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_15 = chachaState_15;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_15 = io_cmd_payload_data_15;
    end
  end

  always @ (io_cmd_payload_data_7 or chachaState_7 or _10 or fsm_stateReg or _36)
  begin
    roundCore_input_7 = _10;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_7 = chachaState_7;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_7 = io_cmd_payload_data_7;
    end
  end

  always @ (io_cmd_payload_data_13 or chachaState_13 or _16 or fsm_stateReg or _36)
  begin
    roundCore_input_13 = _16;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_13 = chachaState_13;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_13 = io_cmd_payload_data_13;
    end
  end

  always @ (io_cmd_payload_data_5 or chachaState_5 or _8 or fsm_stateReg or _36)
  begin
    roundCore_input_5 = _8;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_5 = chachaState_5;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_5 = io_cmd_payload_data_5;
    end
  end

  always @ (io_cmd_payload_data_1 or chachaState_1 or _4 or fsm_stateReg or _36)
  begin
    roundCore_input_1 = _4;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_1 = chachaState_1;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_1 = io_cmd_payload_data_1;
    end
  end

  always @ (io_cmd_payload_data_12 or chachaState_12 or _15 or fsm_stateReg or _36)
  begin
    roundCore_input_12 = _15;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_12 = chachaState_12;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_12 = io_cmd_payload_data_12;
    end
  end

  always @ (io_cmd_payload_data_4 or chachaState_4 or _7 or fsm_stateReg or _36)
  begin
    roundCore_input_4 = _7;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_4 = chachaState_4;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_4 = io_cmd_payload_data_4;
    end
  end

  always @ (io_cmd_payload_data_14 or chachaState_14 or _17 or fsm_stateReg or _36)
  begin
    roundCore_input_14 = _17;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_14 = chachaState_14;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_14 = io_cmd_payload_data_14;
    end
  end

  always @ (io_cmd_payload_data_6 or chachaState_6 or _9 or fsm_stateReg or _36)
  begin
    roundCore_input_6 = _9;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_6 = chachaState_6;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_6 = io_cmd_payload_data_6;
    end
  end

  always @ (io_cmd_payload_data_2 or chachaState_2 or _5 or fsm_stateReg or _36)
  begin
    roundCore_input_2 = _5;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_2 = chachaState_2;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_2 = io_cmd_payload_data_2;
    end
  end

  always @ (io_cmd_payload_data_10 or chachaState_10 or _13 or fsm_stateReg or _36)
  begin
    roundCore_input_10 = _13;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_10 = chachaState_10;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_10 = io_cmd_payload_data_10;
    end
  end

  always @ (io_cmd_payload_data_8 or chachaState_8 or _11 or fsm_stateReg or _36)
  begin
    roundCore_input_8 = _11;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_8 = chachaState_8;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_8 = io_cmd_payload_data_8;
    end
  end

  always @ (io_cmd_payload_data_9 or chachaState_9 or _12 or fsm_stateReg or _36)
  begin
    roundCore_input_9 = _12;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_9 = chachaState_9;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_9 = io_cmd_payload_data_9;
    end
  end

  always @ (io_cmd_payload_data_0 or chachaState_0 or _3 or fsm_stateReg or _36)
  begin
    roundCore_input_0 = _3;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_0 = chachaState_0;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_0 = io_cmd_payload_data_0;
    end
  end

  always @ (io_cmd_payload_data_11 or chachaState_11 or _14 or fsm_stateReg or _36)
  begin
    roundCore_input_11 = _14;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_11 = chachaState_11;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_11 = io_cmd_payload_data_11;
    end
  end

  always @ (io_cmd_payload_data_3 or chachaState_3 or _6 or fsm_stateReg or _36)
  begin
    roundCore_input_3 = _6;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        roundCore_input_3 = chachaState_3;
      end
      default : begin
      end
    endcase
    if(_36)begin
      roundCore_input_3 = io_cmd_payload_data_3;
    end
  end

  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      roundCounter <= (4'b0000);
      fsm_stateReg <= _19;
    end else begin
      case(fsm_stateReg)
        `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
        end
        `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
          roundCounter <= _28;
        end
        default : begin
        end
      endcase
      if(_36)begin
        roundCounter <= _37;
      end
      fsm_stateReg <= fsm_stateNext;
    end
  end

  always @ (posedge clk)
  begin
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
        chachaState_0 <= (32'b00000000000000000000000000000000);
        chachaState_1 <= (32'b00000000000000000000000000000000);
        chachaState_2 <= (32'b00000000000000000000000000000000);
        chachaState_3 <= (32'b00000000000000000000000000000000);
        chachaState_4 <= (32'b00000000000000000000000000000000);
        chachaState_5 <= (32'b00000000000000000000000000000000);
        chachaState_6 <= (32'b00000000000000000000000000000000);
        chachaState_7 <= (32'b00000000000000000000000000000000);
        chachaState_8 <= (32'b00000000000000000000000000000000);
        chachaState_9 <= (32'b00000000000000000000000000000000);
        chachaState_10 <= (32'b00000000000000000000000000000000);
        chachaState_11 <= (32'b00000000000000000000000000000000);
        chachaState_12 <= (32'b00000000000000000000000000000000);
        chachaState_13 <= (32'b00000000000000000000000000000000);
        chachaState_14 <= (32'b00000000000000000000000000000000);
        chachaState_15 <= (32'b00000000000000000000000000000000);
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sComputing : begin
        chachaState_0 <= roundCore_rsp_0;
        chachaState_1 <= roundCore_rsp_1;
        chachaState_2 <= roundCore_rsp_2;
        chachaState_3 <= roundCore_rsp_3;
        chachaState_4 <= roundCore_rsp_4;
        chachaState_5 <= roundCore_rsp_5;
        chachaState_6 <= roundCore_rsp_6;
        chachaState_7 <= roundCore_rsp_7;
        chachaState_8 <= roundCore_rsp_8;
        chachaState_9 <= roundCore_rsp_9;
        chachaState_10 <= roundCore_rsp_10;
        chachaState_11 <= roundCore_rsp_11;
        chachaState_12 <= roundCore_rsp_12;
        chachaState_13 <= roundCore_rsp_13;
        chachaState_14 <= roundCore_rsp_14;
        chachaState_15 <= roundCore_rsp_15;
      end
      default : begin
      end
    endcase
    if(_36)begin
      chachaState_0 <= roundCore_rsp_0;
      chachaState_1 <= roundCore_rsp_1;
      chachaState_2 <= roundCore_rsp_2;
      chachaState_3 <= roundCore_rsp_3;
      chachaState_4 <= roundCore_rsp_4;
      chachaState_5 <= roundCore_rsp_5;
      chachaState_6 <= roundCore_rsp_6;
      chachaState_7 <= roundCore_rsp_7;
      chachaState_8 <= roundCore_rsp_8;
      chachaState_9 <= roundCore_rsp_9;
      chachaState_10 <= roundCore_rsp_10;
      chachaState_11 <= roundCore_rsp_11;
      chachaState_12 <= roundCore_rsp_12;
      chachaState_13 <= roundCore_rsp_13;
      chachaState_14 <= roundCore_rsp_14;
      chachaState_15 <= roundCore_rsp_15;
    end
  end

endmodule

