// Generator : SpinalHDL v0.11.0    git head : 70060064f983e4ff0ba1dd1a886ec515cc842b0c
// Date      : 26/11/2017, 21:41:07
// Component : Curve25519InversionTest


`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_type [2:0]
`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_boot 3'b000
`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 3'b001
`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 3'b010
`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 3'b011
`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 3'b100
`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 3'b101
`define fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 3'b110

`define fsm_enumDefinition_binary_sequancial_type [1:0]
`define fsm_enumDefinition_binary_sequancial_boot 2'b00
`define fsm_enumDefinition_binary_sequancial_fsm_sIdle 2'b01
`define fsm_enumDefinition_binary_sequancial_fsm_sInversion 2'b10
`define fsm_enumDefinition_binary_sequancial_fsm_sDone 2'b11

module BasicMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [127:0] io_cmd_payload_opA,
      input  [127:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_1;
  wire  zz_2;
  reg  zz_3;
  reg [255:0] zz_4;
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! zz_2)) || io_rsp_ready);
  assign zz_2 = zz_3;
  assign io_rsp_valid = zz_2;
  assign io_rsp_payload = zz_4;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_3 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_3 <= io_cmd_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_4 <= (io_cmd_payload_opA * io_cmd_payload_opB);
    end
  end

endmodule


//BasicMultiplier_1 remplaced by BasicMultiplier


//BasicMultiplier_2 remplaced by BasicMultiplier

module StreamFork (
      input   io_input_valid,
      output  io_input_ready,
      input  [255:0] io_input_payload_opA,
      input  [255:0] io_input_payload_opB,
      output  io_outputs_0_valid,
      input   io_outputs_0_ready,
      output [255:0] io_outputs_0_payload_opA,
      output [255:0] io_outputs_0_payload_opB,
      output  io_outputs_1_valid,
      input   io_outputs_1_ready,
      output [255:0] io_outputs_1_payload_opA,
      output [255:0] io_outputs_1_payload_opB,
      output  io_outputs_2_valid,
      input   io_outputs_2_ready,
      output [255:0] io_outputs_2_payload_opA,
      output [255:0] io_outputs_2_payload_opB,
      input   clk,
      input   reset);
  wire  zz_1;
  wire  zz_2;
  wire  zz_3;
  reg  zz_4;
  reg  linkEnable_0;
  reg  linkEnable_1;
  reg  linkEnable_2;
  assign io_outputs_0_valid = zz_1;
  assign io_outputs_1_valid = zz_2;
  assign io_outputs_2_valid = zz_3;
  assign io_input_ready = zz_4;
  always @ (io_outputs_0_ready or linkEnable_0 or io_outputs_1_ready or linkEnable_1 or io_outputs_2_ready or linkEnable_2)
  begin
    zz_4 = 1'b1;
    if(((! io_outputs_0_ready) && linkEnable_0))begin
      zz_4 = 1'b0;
    end
    if(((! io_outputs_1_ready) && linkEnable_1))begin
      zz_4 = 1'b0;
    end
    if(((! io_outputs_2_ready) && linkEnable_2))begin
      zz_4 = 1'b0;
    end
  end

  assign zz_1 = (io_input_valid && linkEnable_0);
  assign io_outputs_0_payload_opA = io_input_payload_opA;
  assign io_outputs_0_payload_opB = io_input_payload_opB;
  assign zz_2 = (io_input_valid && linkEnable_1);
  assign io_outputs_1_payload_opA = io_input_payload_opA;
  assign io_outputs_1_payload_opB = io_input_payload_opB;
  assign zz_3 = (io_input_valid && linkEnable_2);
  assign io_outputs_2_payload_opA = io_input_payload_opA;
  assign io_outputs_2_payload_opB = io_input_payload_opB;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      linkEnable_0 <= 1'b1;
      linkEnable_1 <= 1'b1;
      linkEnable_2 <= 1'b1;
    end else begin
      if((zz_1 && io_outputs_0_ready))begin
        linkEnable_0 <= 1'b0;
      end
      if((zz_2 && io_outputs_1_ready))begin
        linkEnable_1 <= 1'b0;
      end
      if((zz_3 && io_outputs_2_ready))begin
        linkEnable_2 <= 1'b0;
      end
      if(zz_4)begin
        linkEnable_0 <= 1'b1;
        linkEnable_1 <= 1'b1;
        linkEnable_2 <= 1'b1;
      end
    end
  end

endmodule


//BasicMultiplier_3 remplaced by BasicMultiplier


//BasicMultiplier_4 remplaced by BasicMultiplier


//BasicMultiplier_5 remplaced by BasicMultiplier


//StreamFork_1 remplaced by StreamFork

module KaratsubaMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [255:0] io_cmd_payload_opA,
      input  [255:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [511:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_4;
  wire  zz_5;
  wire [255:0] zz_6;
  wire  zz_7;
  wire  zz_8;
  wire [255:0] zz_9;
  wire  zz_10;
  wire  zz_11;
  wire [255:0] zz_12;
  wire  zz_13;
  wire  zz_14;
  wire [255:0] zz_15;
  wire [255:0] zz_16;
  wire  zz_17;
  wire [255:0] zz_18;
  wire [255:0] zz_19;
  wire  zz_20;
  wire [255:0] zz_21;
  wire [255:0] zz_22;
  wire [128:0] zz_23;
  wire [128:0] zz_24;
  wire [127:0] zz_25;
  wire [128:0] zz_26;
  wire [128:0] zz_27;
  wire [127:0] zz_28;
  wire [383:0] zz_29;
  wire [511:0] zz_30;
  wire [511:0] zz_31;
  wire [511:0] zz_32;
  wire [383:0] zz_33;
  wire [383:0] zz_34;
  wire [383:0] zz_35;
  wire [383:0] zz_36;
  wire [511:0] zz_37;
  wire [127:0] al;
  wire [127:0] ah;
  wire [127:0] bl;
  wire [127:0] bh;
  wire [128:0] zz_1;
  wire  borrowA;
  wire [127:0] absDiff_a;
  wire [128:0] zz_2;
  wire  borrowB;
  wire [127:0] absDiff_b;
  wire  subMultJoin_valid;
  wire  subMultJoin_ready;
  wire  zz_3;
  wire [127:0] cmdL_opA;
  wire [127:0] cmdL_opB;
  wire [127:0] cmdM_opA;
  wire [127:0] cmdM_opB;
  wire [127:0] cmdH_opA;
  wire [127:0] cmdH_opB;
  wire [383:0] m_resized;
  wire [383:0] m_hat;
  wire [511:0] w;
  assign zz_23 = al;
  assign zz_24 = ah;
  assign zz_25 = borrowA;
  assign zz_26 = bl;
  assign zz_27 = bh;
  assign zz_28 = borrowB;
  assign zz_29 = ((384'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000) - m_resized);
  assign zz_30 = (zz_31 + zz_32);
  assign zz_31 = (zz_12 <<< 256);
  assign zz_32 = (zz_33 <<< 128);
  assign zz_33 = (zz_34 + zz_36);
  assign zz_34 = (m_hat + zz_35);
  assign zz_35 = zz_12;
  assign zz_36 = zz_6;
  assign zz_37 = zz_6;
  BasicMultiplier multL ( 
    .io_cmd_valid(zz_14),
    .io_cmd_ready(zz_4),
    .io_cmd_payload_opA(cmdL_opA),
    .io_cmd_payload_opB(cmdL_opB),
    .io_rsp_valid(zz_5),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_6),
    .clk(clk),
    .reset(reset) 
  );
  BasicMultiplier multM ( 
    .io_cmd_valid(zz_17),
    .io_cmd_ready(zz_7),
    .io_cmd_payload_opA(cmdM_opA),
    .io_cmd_payload_opB(cmdM_opB),
    .io_rsp_valid(zz_8),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_9),
    .clk(clk),
    .reset(reset) 
  );
  BasicMultiplier multH ( 
    .io_cmd_valid(zz_20),
    .io_cmd_ready(zz_10),
    .io_cmd_payload_opA(cmdH_opA),
    .io_cmd_payload_opB(cmdH_opB),
    .io_rsp_valid(zz_11),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_12),
    .clk(clk),
    .reset(reset) 
  );
  StreamFork streamFork_2 ( 
    .io_input_valid(io_cmd_valid),
    .io_input_ready(zz_13),
    .io_input_payload_opA(io_cmd_payload_opA),
    .io_input_payload_opB(io_cmd_payload_opB),
    .io_outputs_0_valid(zz_14),
    .io_outputs_0_ready(zz_4),
    .io_outputs_0_payload_opA(zz_15),
    .io_outputs_0_payload_opB(zz_16),
    .io_outputs_1_valid(zz_17),
    .io_outputs_1_ready(zz_7),
    .io_outputs_1_payload_opA(zz_18),
    .io_outputs_1_payload_opB(zz_19),
    .io_outputs_2_valid(zz_20),
    .io_outputs_2_ready(zz_10),
    .io_outputs_2_payload_opA(zz_21),
    .io_outputs_2_payload_opB(zz_22),
    .clk(clk),
    .reset(reset) 
  );
  assign al = io_cmd_payload_opA[127 : 0];
  assign ah = io_cmd_payload_opA[255 : 128];
  assign bl = io_cmd_payload_opB[127 : 0];
  assign bh = io_cmd_payload_opB[255 : 128];
  assign zz_1 = (zz_23 - zz_24);
  assign borrowA = zz_1[128];
  assign absDiff_a = ((zz_1[127 : 0] ^ {borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,borrowA}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}) + zz_25);
  assign zz_2 = (zz_26 - zz_27);
  assign borrowB = zz_2[128];
  assign absDiff_b = ((zz_2[127 : 0] ^ {borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,borrowB}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}) + zz_28);
  assign zz_3 = (subMultJoin_valid && subMultJoin_ready);
  assign subMultJoin_valid = ((zz_5 && zz_8) && zz_11);
  assign io_cmd_ready = zz_13;
  assign cmdL_opA = al;
  assign cmdL_opB = bl;
  assign cmdM_opA = absDiff_a;
  assign cmdM_opB = absDiff_b;
  assign cmdH_opA = ah;
  assign cmdH_opB = bh;
  assign m_resized = zz_9;
  assign m_hat = ((borrowA ^ borrowB) ? m_resized : zz_29);
  assign w = (zz_30 + zz_37);
  assign subMultJoin_ready = io_rsp_ready;
  assign io_rsp_valid = subMultJoin_valid;
  assign io_rsp_payload = w;
endmodule

module ModularReducer512 (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [511:0] io_cmd_payload,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire [261:0] zz_11;
  wire [261:0] zz_12;
  wire [256:0] zz_13;
  wire [256:0] zz_14;
  wire [11:0] zz_15;
  wire [255:0] zz_16;
  wire [255:0] low1;
  wire [255:0] high1;
  wire [261:0] val1;
  wire  zz_1;
  wire  round1_valid;
  wire  round1_ready;
  wire [261:0] round1_payload;
  reg  zz_2;
  reg [261:0] zz_3;
  wire [255:0] low2;
  wire [5:0] high2;
  wire [256:0] val2;
  wire  zz_4;
  wire  round2_valid;
  wire  round2_ready;
  wire [256:0] round2_payload;
  reg  zz_5;
  reg [256:0] zz_6;
  wire [255:0] low3;
  wire  high3;
  wire [255:0] val3;
  wire  zz_7;
  wire  zz_8;
  reg  zz_9;
  reg [255:0] zz_10;
  assign zz_11 = low1;
  assign zz_12 = (high1 * (6'b100110));
  assign zz_13 = low2;
  assign zz_14 = zz_15;
  assign zz_15 = (high2 * (6'b100110));
  assign zz_16 = (high3 ? (6'b100110) : (6'b000000));
  assign low1 = io_cmd_payload[255 : 0];
  assign high1 = io_cmd_payload[511 : 256];
  assign val1 = (zz_11 + zz_12);
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! round1_valid)) || round1_ready);
  assign round1_valid = zz_2;
  assign round1_payload = zz_3;
  assign low2 = round1_payload[255 : 0];
  assign high2 = round1_payload[261 : 256];
  assign val2 = (zz_13 + zz_14);
  assign round1_ready = zz_4;
  assign zz_4 = ((1'b1 && (! round2_valid)) || round2_ready);
  assign round2_valid = zz_5;
  assign round2_payload = zz_6;
  assign low3 = round2_payload[255 : 0];
  assign high3 = round2_payload[256];
  assign val3 = (low3 + zz_16);
  assign round2_ready = zz_7;
  assign zz_7 = ((1'b1 && (! zz_8)) || io_rsp_ready);
  assign zz_8 = zz_9;
  assign io_rsp_valid = zz_8;
  assign io_rsp_payload = zz_10;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_2 <= 1'b0;
      zz_5 <= 1'b0;
      zz_9 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_2 <= io_cmd_valid;
      end
      if(zz_4)begin
        zz_5 <= round1_valid;
      end
      if(zz_7)begin
        zz_9 <= round2_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_3 <= val1;
    end
    if(zz_4)begin
      zz_6 <= val2;
    end
    if(zz_7)begin
      zz_10 <= val3;
    end
  end

endmodule


//KaratsubaMultiplier_1 remplaced by KaratsubaMultiplier


//ModularReducer512_1 remplaced by ModularReducer512

module ModularMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [255:0] io_cmd_payload_opA,
      input  [255:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_4;
  wire  zz_5;
  wire  zz_6;
  wire [511:0] zz_7;
  wire  zz_8;
  wire  zz_9;
  wire [255:0] zz_10;
  wire  zz_1;
  reg  zz_2;
  reg [511:0] zz_3;
  KaratsubaMultiplier multiplier ( 
    .io_cmd_valid(io_cmd_valid),
    .io_cmd_ready(zz_5),
    .io_cmd_payload_opA(io_cmd_payload_opA),
    .io_cmd_payload_opB(io_cmd_payload_opB),
    .io_rsp_valid(zz_6),
    .io_rsp_ready(zz_4),
    .io_rsp_payload(zz_7),
    .clk(clk),
    .reset(reset) 
  );
  ModularReducer512 reducer ( 
    .io_cmd_valid(zz_1),
    .io_cmd_ready(zz_8),
    .io_cmd_payload(zz_3),
    .io_rsp_valid(zz_9),
    .io_rsp_ready(io_rsp_ready),
    .io_rsp_payload(zz_10),
    .clk(clk),
    .reset(reset) 
  );
  assign io_cmd_ready = zz_5;
  assign zz_4 = ((1'b1 && (! zz_1)) || zz_8);
  assign zz_1 = zz_2;
  assign io_rsp_valid = zz_9;
  assign io_rsp_payload = zz_10;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_2 <= 1'b0;
    end else begin
      if(zz_4)begin
        zz_2 <= zz_6;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_4)begin
      zz_3 <= zz_7;
    end
  end

endmodule


//ModularMultiplier_1 remplaced by ModularMultiplier

module Curve25519InversionTest (
      input   io_cmd_valid,
      output reg  io_cmd_ready,
      input  [255:0] io_cmd_payload,
      output reg  io_rsp_valid,
      input   io_rsp_ready,
      output reg [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  reg  zz_1;
  reg [255:0] zz_2;
  reg [255:0] zz_3;
  reg  zz_4;
  reg  zz_5;
  reg [255:0] zz_6;
  reg [255:0] zz_7;
  reg  zz_8;
  wire  zz_9;
  wire  zz_10;
  wire [255:0] zz_11;
  wire  zz_12;
  wire  zz_13;
  wire [255:0] zz_14;
  wire  zz_15;
  reg [255:0] z;
  reg [255:0] t0;
  reg [255:0] t1;
  reg [255:0] t2;
  reg [255:0] t3;
  wire  fsm_wantExit;
  reg  fsm_sInversion_fsm_wantExit;
  reg [7:0] fsm_sInversion_fsm_counter;
  reg  fsm_sInversion_fsm_exponentBit;
  wire  fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid;
  wire  fsm_sInversion_fsm_sSquareMultIssue_enginesReady;
  reg `fsm_sInversion_fsm_enumDefinition_binary_sequancial_type fsm_sInversion_fsm_stateReg;
  reg `fsm_sInversion_fsm_enumDefinition_binary_sequancial_type fsm_sInversion_fsm_stateNext;
  reg `fsm_enumDefinition_binary_sequancial_type fsm_stateReg;
  reg `fsm_enumDefinition_binary_sequancial_type fsm_stateNext;
  assign zz_15 = (fsm_sInversion_fsm_counter == (8'b11111111));
  ModularMultiplier mul0 ( 
    .io_cmd_valid(zz_1),
    .io_cmd_ready(zz_9),
    .io_cmd_payload_opA(zz_2),
    .io_cmd_payload_opB(zz_3),
    .io_rsp_valid(zz_10),
    .io_rsp_ready(zz_4),
    .io_rsp_payload(zz_11),
    .clk(clk),
    .reset(reset) 
  );
  ModularMultiplier mul1 ( 
    .io_cmd_valid(zz_5),
    .io_cmd_ready(zz_12),
    .io_cmd_payload_opA(zz_6),
    .io_cmd_payload_opB(zz_7),
    .io_rsp_valid(zz_13),
    .io_rsp_ready(zz_8),
    .io_rsp_payload(zz_14),
    .clk(clk),
    .reset(reset) 
  );
  always @ (fsm_stateReg)
  begin
    io_cmd_ready = 1'b0;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
        io_cmd_ready = 1'b1;
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sInversion : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sDone : begin
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_stateReg)
  begin
    io_rsp_valid = 1'b0;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sInversion : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sDone : begin
        io_rsp_valid = 1'b1;
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_stateReg or z)
  begin
    io_rsp_payload = (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sInversion : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sDone : begin
        io_rsp_payload = z;
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_stateReg or zz_9 or fsm_sInversion_fsm_sSquareMultIssue_enginesReady)
  begin
    zz_1 = 1'b0;
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
        if(zz_9)begin
          zz_1 = 1'b1;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        if(fsm_sInversion_fsm_sSquareMultIssue_enginesReady)begin
          zz_1 = 1'b1;
        end
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_stateReg or fsm_sInversion_fsm_sSquareMultIssue_enginesReady)
  begin
    zz_5 = 1'b0;
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        if(fsm_sInversion_fsm_sSquareMultIssue_enginesReady)begin
          zz_5 = 1'b1;
        end
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_stateReg or zz_10 or fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid)
  begin
    zz_4 = 1'b0;
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
        if(zz_10)begin
          zz_4 = 1'b1;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
        if(fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid)begin
          zz_4 = 1'b1;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_stateReg or fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid)
  begin
    zz_8 = 1'b0;
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
        if(fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid)begin
          zz_8 = 1'b1;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
      end
      default : begin
      end
    endcase
  end

  assign fsm_wantExit = 1'b0;
  always @ (fsm_sInversion_fsm_stateReg or zz_15)
  begin
    fsm_sInversion_fsm_wantExit = 1'b0;
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
        if(zz_15)begin
          fsm_sInversion_fsm_wantExit = 1'b1;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_counter)
  begin
    case(fsm_sInversion_fsm_counter)
      8'b00000010 : begin
        fsm_sInversion_fsm_exponentBit = 1'b0;
      end
      8'b00000100 : begin
        fsm_sInversion_fsm_exponentBit = 1'b0;
      end
      8'b11111111 : begin
        fsm_sInversion_fsm_exponentBit = 1'b0;
      end
      default : begin
        fsm_sInversion_fsm_exponentBit = 1'b1;
      end
    endcase
  end

  assign fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid = (zz_10 && zz_13);
  assign fsm_sInversion_fsm_sSquareMultIssue_enginesReady = (zz_9 && zz_12);
  always @ (fsm_sInversion_fsm_stateReg or zz_15 or fsm_sInversion_fsm_exponentBit or zz_10 or zz_9 or fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid or fsm_sInversion_fsm_sSquareMultIssue_enginesReady or fsm_stateReg or fsm_stateNext)
  begin
    fsm_sInversion_fsm_stateNext = fsm_sInversion_fsm_stateReg;
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
        fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2;
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
        if(zz_15)begin
          fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_boot;
        end else begin
          if(fsm_sInversion_fsm_exponentBit)begin
            fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6;
          end else begin
            fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4;
          end
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
        if(zz_10)begin
          fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
        if(zz_9)begin
          fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
        if(fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid)begin
          fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        if(fsm_sInversion_fsm_sSquareMultIssue_enginesReady)begin
          fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5;
        end
      end
      default : begin
      end
    endcase
    if(((! (fsm_stateReg == `fsm_enumDefinition_binary_sequancial_fsm_sInversion)) && (fsm_stateNext == `fsm_enumDefinition_binary_sequancial_fsm_sInversion)))begin
      fsm_sInversion_fsm_stateNext = `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1;
    end
  end

  always @ (fsm_sInversion_fsm_stateReg or zz_9 or z or fsm_sInversion_fsm_sSquareMultIssue_enginesReady)
  begin
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
        if(zz_9)begin
          zz_2 = z;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        if(fsm_sInversion_fsm_sSquareMultIssue_enginesReady)begin
          zz_2 = z;
        end
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_stateReg or zz_9 or z or fsm_sInversion_fsm_sSquareMultIssue_enginesReady)
  begin
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
        if(zz_9)begin
          zz_3 = z;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        if(fsm_sInversion_fsm_sSquareMultIssue_enginesReady)begin
          zz_3 = z;
        end
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_stateReg or fsm_sInversion_fsm_sSquareMultIssue_enginesReady or t0)
  begin
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        if(fsm_sInversion_fsm_sSquareMultIssue_enginesReady)begin
          zz_6 = t0;
        end
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_sInversion_fsm_stateReg or fsm_sInversion_fsm_sSquareMultIssue_enginesReady or z)
  begin
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        if(fsm_sInversion_fsm_sSquareMultIssue_enginesReady)begin
          zz_7 = z;
        end
      end
      default : begin
      end
    endcase
  end

  always @ (fsm_stateReg or io_cmd_valid or fsm_sInversion_fsm_wantExit or io_rsp_ready)
  begin
    fsm_stateNext = fsm_stateReg;
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
        if(io_cmd_valid)begin
          fsm_stateNext = `fsm_enumDefinition_binary_sequancial_fsm_sInversion;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sInversion : begin
        if(fsm_sInversion_fsm_wantExit)begin
          fsm_stateNext = `fsm_enumDefinition_binary_sequancial_fsm_sDone;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sDone : begin
        if(io_rsp_ready)begin
          fsm_stateNext = `fsm_enumDefinition_binary_sequancial_fsm_sIdle;
        end
      end
      default : begin
        fsm_stateNext = `fsm_enumDefinition_binary_sequancial_fsm_sIdle;
      end
    endcase
  end

  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      fsm_sInversion_fsm_counter <= (8'b00000000);
      fsm_sInversion_fsm_stateReg <= `fsm_sInversion_fsm_enumDefinition_binary_sequancial_boot;
      fsm_stateReg <= `fsm_enumDefinition_binary_sequancial_boot;
    end else begin
      fsm_sInversion_fsm_stateReg <= fsm_sInversion_fsm_stateNext;
      case(fsm_sInversion_fsm_stateReg)
        `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
          fsm_sInversion_fsm_counter <= (8'b00000000);
        end
        `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
          fsm_sInversion_fsm_counter <= (fsm_sInversion_fsm_counter + (8'b00000001));
        end
        `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
        end
        `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
        end
        `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
        end
        `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
        end
        default : begin
        end
      endcase
      fsm_stateReg <= fsm_stateNext;
    end
  end

  always @ (posedge clk)
  begin
    case(fsm_sInversion_fsm_stateReg)
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e1 : begin
        t0 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001);
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e2 : begin
        if(zz_15)begin
          z <= t0;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e3 : begin
        if(zz_10)begin
          z <= zz_11;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e5 : begin
        if(fsm_sInversion_fsm_sSquareMultWriteBack_engineRspsValid)begin
          z <= zz_11;
          t0 <= zz_14;
        end
      end
      `fsm_sInversion_fsm_enumDefinition_binary_sequancial_e6 : begin
      end
      default : begin
      end
    endcase
    case(fsm_stateReg)
      `fsm_enumDefinition_binary_sequancial_fsm_sIdle : begin
        z <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
        t0 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
        t1 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
        t2 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
        t3 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
        if(io_cmd_valid)begin
          z <= io_cmd_payload;
        end
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sInversion : begin
      end
      `fsm_enumDefinition_binary_sequancial_fsm_sDone : begin
      end
      default : begin
      end
    endcase
  end

endmodule

