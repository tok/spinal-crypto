// Generator : SpinalHDL v0.11.0    git head : 70060064f983e4ff0ba1dd1a886ec515cc842b0c
// Date      : 27/11/2017, 09:37:57
// Component : Curve25519Ice40


`define mainFsm_enumDefinition_binary_sequancial_type [3:0]
`define mainFsm_enumDefinition_binary_sequancial_boot 4'b0000
`define mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit 4'b0001
`define mainFsm_enumDefinition_binary_sequancial_e2 4'b0010
`define mainFsm_enumDefinition_binary_sequancial_e3 4'b0011
`define mainFsm_enumDefinition_binary_sequancial_e4 4'b0100
`define mainFsm_enumDefinition_binary_sequancial_e5 4'b0101
`define mainFsm_enumDefinition_binary_sequancial_e6 4'b0110
`define mainFsm_enumDefinition_binary_sequancial_e7 4'b0111
`define mainFsm_enumDefinition_binary_sequancial_e8 4'b1000
`define mainFsm_enumDefinition_binary_sequancial_e9 4'b1001
`define mainFsm_enumDefinition_binary_sequancial_e10 4'b1010
`define mainFsm_enumDefinition_binary_sequancial_e11 4'b1011
`define mainFsm_enumDefinition_binary_sequancial_e12 4'b1100
`define mainFsm_enumDefinition_binary_sequancial_e13 4'b1101
`define mainFsm_enumDefinition_binary_sequancial_e14 4'b1110

module BasicMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [127:0] io_cmd_payload_opA,
      input  [127:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_1;
  wire  zz_2;
  reg  zz_3;
  reg [255:0] zz_4;
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! zz_2)) || io_rsp_ready);
  assign zz_2 = zz_3;
  assign io_rsp_valid = zz_2;
  assign io_rsp_payload = zz_4;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_3 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_3 <= io_cmd_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_4 <= (io_cmd_payload_opA * io_cmd_payload_opB);
    end
  end

endmodule


//BasicMultiplier_1 remplaced by BasicMultiplier


//BasicMultiplier_2 remplaced by BasicMultiplier

module StreamFork (
      input   io_input_valid,
      output  io_input_ready,
      input  [255:0] io_input_payload_opA,
      input  [255:0] io_input_payload_opB,
      output  io_outputs_0_valid,
      input   io_outputs_0_ready,
      output [255:0] io_outputs_0_payload_opA,
      output [255:0] io_outputs_0_payload_opB,
      output  io_outputs_1_valid,
      input   io_outputs_1_ready,
      output [255:0] io_outputs_1_payload_opA,
      output [255:0] io_outputs_1_payload_opB,
      output  io_outputs_2_valid,
      input   io_outputs_2_ready,
      output [255:0] io_outputs_2_payload_opA,
      output [255:0] io_outputs_2_payload_opB,
      input   clk,
      input   reset);
  wire  zz_1;
  wire  zz_2;
  wire  zz_3;
  reg  zz_4;
  reg  linkEnable_0;
  reg  linkEnable_1;
  reg  linkEnable_2;
  assign io_outputs_0_valid = zz_1;
  assign io_outputs_1_valid = zz_2;
  assign io_outputs_2_valid = zz_3;
  assign io_input_ready = zz_4;
  always @ (io_outputs_0_ready or linkEnable_0 or io_outputs_1_ready or linkEnable_1 or io_outputs_2_ready or linkEnable_2)
  begin
    zz_4 = 1'b1;
    if(((! io_outputs_0_ready) && linkEnable_0))begin
      zz_4 = 1'b0;
    end
    if(((! io_outputs_1_ready) && linkEnable_1))begin
      zz_4 = 1'b0;
    end
    if(((! io_outputs_2_ready) && linkEnable_2))begin
      zz_4 = 1'b0;
    end
  end

  assign zz_1 = (io_input_valid && linkEnable_0);
  assign io_outputs_0_payload_opA = io_input_payload_opA;
  assign io_outputs_0_payload_opB = io_input_payload_opB;
  assign zz_2 = (io_input_valid && linkEnable_1);
  assign io_outputs_1_payload_opA = io_input_payload_opA;
  assign io_outputs_1_payload_opB = io_input_payload_opB;
  assign zz_3 = (io_input_valid && linkEnable_2);
  assign io_outputs_2_payload_opA = io_input_payload_opA;
  assign io_outputs_2_payload_opB = io_input_payload_opB;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      linkEnable_0 <= 1'b1;
      linkEnable_1 <= 1'b1;
      linkEnable_2 <= 1'b1;
    end else begin
      if((zz_1 && io_outputs_0_ready))begin
        linkEnable_0 <= 1'b0;
      end
      if((zz_2 && io_outputs_1_ready))begin
        linkEnable_1 <= 1'b0;
      end
      if((zz_3 && io_outputs_2_ready))begin
        linkEnable_2 <= 1'b0;
      end
      if(zz_4)begin
        linkEnable_0 <= 1'b1;
        linkEnable_1 <= 1'b1;
        linkEnable_2 <= 1'b1;
      end
    end
  end

endmodule


//BasicMultiplier_3 remplaced by BasicMultiplier


//BasicMultiplier_4 remplaced by BasicMultiplier


//BasicMultiplier_5 remplaced by BasicMultiplier


//StreamFork_1 remplaced by StreamFork


//BasicMultiplier_6 remplaced by BasicMultiplier


//BasicMultiplier_7 remplaced by BasicMultiplier


//BasicMultiplier_8 remplaced by BasicMultiplier


//StreamFork_2 remplaced by StreamFork


//BasicMultiplier_9 remplaced by BasicMultiplier


//BasicMultiplier_10 remplaced by BasicMultiplier


//BasicMultiplier_11 remplaced by BasicMultiplier


//StreamFork_3 remplaced by StreamFork

module KaratsubaMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [255:0] io_cmd_payload_opA,
      input  [255:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [511:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_8;
  wire  zz_9;
  wire [255:0] zz_10;
  wire  zz_11;
  wire  zz_12;
  wire [255:0] zz_13;
  wire  zz_14;
  wire  zz_15;
  wire [255:0] zz_16;
  wire  zz_17;
  wire  zz_18;
  wire [255:0] zz_19;
  wire [255:0] zz_20;
  wire  zz_21;
  wire [255:0] zz_22;
  wire [255:0] zz_23;
  wire  zz_24;
  wire [255:0] zz_25;
  wire [255:0] zz_26;
  wire [128:0] zz_27;
  wire [128:0] zz_28;
  wire [127:0] zz_29;
  wire [128:0] zz_30;
  wire [128:0] zz_31;
  wire [127:0] zz_32;
  wire [383:0] zz_33;
  wire [511:0] zz_34;
  wire [511:0] zz_35;
  wire [511:0] zz_36;
  wire [383:0] zz_37;
  wire [383:0] zz_38;
  wire [383:0] zz_39;
  wire [383:0] zz_40;
  wire [511:0] zz_41;
  wire [127:0] al;
  wire [127:0] ah;
  wire [127:0] bl;
  wire [127:0] bh;
  wire [128:0] zz_1;
  wire  borrowA;
  wire [127:0] absDiff_a;
  wire [128:0] zz_2;
  wire  borrowB;
  wire [127:0] absDiff_b;
  wire  subMultJoin_valid;
  wire  subMultJoin_ready;
  wire  zz_3;
  wire [127:0] cmdL_opA;
  wire [127:0] cmdL_opB;
  wire [127:0] cmdM_opA;
  wire [127:0] cmdM_opB;
  wire [127:0] cmdH_opA;
  wire [127:0] cmdH_opB;
  wire [383:0] m_resized;
  wire [383:0] m_hat;
  wire [511:0] w;
  wire  zz_4;
  wire  zz_5;
  reg  zz_6;
  reg [511:0] zz_7;
  assign zz_27 = al;
  assign zz_28 = ah;
  assign zz_29 = borrowA;
  assign zz_30 = bl;
  assign zz_31 = bh;
  assign zz_32 = borrowB;
  assign zz_33 = ((384'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000) - m_resized);
  assign zz_34 = (zz_35 + zz_36);
  assign zz_35 = (zz_16 <<< 256);
  assign zz_36 = (zz_37 <<< 128);
  assign zz_37 = (zz_38 + zz_40);
  assign zz_38 = (m_hat + zz_39);
  assign zz_39 = zz_16;
  assign zz_40 = zz_10;
  assign zz_41 = zz_10;
  BasicMultiplier multL ( 
    .io_cmd_valid(zz_18),
    .io_cmd_ready(zz_8),
    .io_cmd_payload_opA(cmdL_opA),
    .io_cmd_payload_opB(cmdL_opB),
    .io_rsp_valid(zz_9),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_10),
    .clk(clk),
    .reset(reset) 
  );
  BasicMultiplier multM ( 
    .io_cmd_valid(zz_21),
    .io_cmd_ready(zz_11),
    .io_cmd_payload_opA(cmdM_opA),
    .io_cmd_payload_opB(cmdM_opB),
    .io_rsp_valid(zz_12),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_13),
    .clk(clk),
    .reset(reset) 
  );
  BasicMultiplier multH ( 
    .io_cmd_valid(zz_24),
    .io_cmd_ready(zz_14),
    .io_cmd_payload_opA(cmdH_opA),
    .io_cmd_payload_opB(cmdH_opB),
    .io_rsp_valid(zz_15),
    .io_rsp_ready(zz_3),
    .io_rsp_payload(zz_16),
    .clk(clk),
    .reset(reset) 
  );
  StreamFork streamFork_4 ( 
    .io_input_valid(io_cmd_valid),
    .io_input_ready(zz_17),
    .io_input_payload_opA(io_cmd_payload_opA),
    .io_input_payload_opB(io_cmd_payload_opB),
    .io_outputs_0_valid(zz_18),
    .io_outputs_0_ready(zz_8),
    .io_outputs_0_payload_opA(zz_19),
    .io_outputs_0_payload_opB(zz_20),
    .io_outputs_1_valid(zz_21),
    .io_outputs_1_ready(zz_11),
    .io_outputs_1_payload_opA(zz_22),
    .io_outputs_1_payload_opB(zz_23),
    .io_outputs_2_valid(zz_24),
    .io_outputs_2_ready(zz_14),
    .io_outputs_2_payload_opA(zz_25),
    .io_outputs_2_payload_opB(zz_26),
    .clk(clk),
    .reset(reset) 
  );
  assign al = io_cmd_payload_opA[127 : 0];
  assign ah = io_cmd_payload_opA[255 : 128];
  assign bl = io_cmd_payload_opB[127 : 0];
  assign bh = io_cmd_payload_opB[255 : 128];
  assign zz_1 = (zz_27 - zz_28);
  assign borrowA = zz_1[128];
  assign absDiff_a = ((zz_1[127 : 0] ^ {borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,{borrowA,borrowA}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}) + zz_29);
  assign zz_2 = (zz_30 - zz_31);
  assign borrowB = zz_2[128];
  assign absDiff_b = ((zz_2[127 : 0] ^ {borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,{borrowB,borrowB}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}) + zz_32);
  assign zz_3 = (subMultJoin_valid && subMultJoin_ready);
  assign subMultJoin_valid = ((zz_9 && zz_12) && zz_15);
  assign io_cmd_ready = zz_17;
  assign cmdL_opA = al;
  assign cmdL_opB = bl;
  assign cmdM_opA = absDiff_a;
  assign cmdM_opB = absDiff_b;
  assign cmdH_opA = ah;
  assign cmdH_opB = bh;
  assign m_resized = zz_13;
  assign m_hat = ((borrowA ^ borrowB) ? m_resized : zz_33);
  assign w = (zz_34 + zz_41);
  assign subMultJoin_ready = zz_4;
  assign zz_4 = ((1'b1 && (! zz_5)) || io_rsp_ready);
  assign zz_5 = zz_6;
  assign io_rsp_valid = zz_5;
  assign io_rsp_payload = zz_7;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_6 <= 1'b0;
    end else begin
      if(zz_4)begin
        zz_6 <= subMultJoin_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_4)begin
      zz_7 <= w;
    end
  end

endmodule

module ModularReducer512 (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [511:0] io_cmd_payload,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire [261:0] zz_11;
  wire [261:0] zz_12;
  wire [256:0] zz_13;
  wire [256:0] zz_14;
  wire [11:0] zz_15;
  wire [255:0] zz_16;
  wire [255:0] low1;
  wire [255:0] high1;
  wire [261:0] val1;
  wire  zz_1;
  wire  round1_valid;
  wire  round1_ready;
  wire [261:0] round1_payload;
  reg  zz_2;
  reg [261:0] zz_3;
  wire [255:0] low2;
  wire [5:0] high2;
  wire [256:0] val2;
  wire  zz_4;
  wire  round2_valid;
  wire  round2_ready;
  wire [256:0] round2_payload;
  reg  zz_5;
  reg [256:0] zz_6;
  wire [255:0] low3;
  wire  high3;
  wire [255:0] val3;
  wire  zz_7;
  wire  zz_8;
  reg  zz_9;
  reg [255:0] zz_10;
  assign zz_11 = low1;
  assign zz_12 = (high1 * (6'b100110));
  assign zz_13 = low2;
  assign zz_14 = zz_15;
  assign zz_15 = (high2 * (6'b100110));
  assign zz_16 = (high3 ? (6'b100110) : (6'b000000));
  assign low1 = io_cmd_payload[255 : 0];
  assign high1 = io_cmd_payload[511 : 256];
  assign val1 = (zz_11 + zz_12);
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! round1_valid)) || round1_ready);
  assign round1_valid = zz_2;
  assign round1_payload = zz_3;
  assign low2 = round1_payload[255 : 0];
  assign high2 = round1_payload[261 : 256];
  assign val2 = (zz_13 + zz_14);
  assign round1_ready = zz_4;
  assign zz_4 = ((1'b1 && (! round2_valid)) || round2_ready);
  assign round2_valid = zz_5;
  assign round2_payload = zz_6;
  assign low3 = round2_payload[255 : 0];
  assign high3 = round2_payload[256];
  assign val3 = (low3 + zz_16);
  assign round2_ready = zz_7;
  assign zz_7 = ((1'b1 && (! zz_8)) || io_rsp_ready);
  assign zz_8 = zz_9;
  assign io_rsp_valid = zz_8;
  assign io_rsp_payload = zz_10;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_2 <= 1'b0;
      zz_5 <= 1'b0;
      zz_9 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_2 <= io_cmd_valid;
      end
      if(zz_4)begin
        zz_5 <= round1_valid;
      end
      if(zz_7)begin
        zz_9 <= round2_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_3 <= val1;
    end
    if(zz_4)begin
      zz_6 <= val2;
    end
    if(zz_7)begin
      zz_10 <= val3;
    end
  end

endmodule


//KaratsubaMultiplier_1 remplaced by KaratsubaMultiplier


//ModularReducer512_1 remplaced by ModularReducer512


//KaratsubaMultiplier_2 remplaced by KaratsubaMultiplier


//ModularReducer512_2 remplaced by ModularReducer512


//KaratsubaMultiplier_3 remplaced by KaratsubaMultiplier


//ModularReducer512_3 remplaced by ModularReducer512

module ModularAdder (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [255:0] a,
      input  [255:0] b,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire [256:0] zz_5;
  wire [256:0] zz_6;
  wire [255:0] zz_7;
  wire [255:0] zz_8;
  wire [256:0] sum;
  wire  carry;
  wire [255:0] reduced;
  wire  zz_1;
  wire  zz_2;
  reg  zz_3;
  reg [255:0] zz_4;
  assign zz_5 = a;
  assign zz_6 = b;
  assign zz_7 = sum[255:0];
  assign zz_8 = (carry ? (6'b100110) : (6'b000000));
  assign sum = (zz_5 + zz_6);
  assign carry = sum[256];
  assign reduced = (zz_7 + zz_8);
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! zz_2)) || io_rsp_ready);
  assign zz_2 = zz_3;
  assign io_rsp_valid = zz_2;
  assign io_rsp_payload = zz_4;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_3 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_3 <= io_cmd_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_4 <= reduced;
    end
  end

endmodule


//ModularAdder_1 remplaced by ModularAdder

module ModularSubtract (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [255:0] a,
      input  [255:0] b,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire [256:0] zz_5;
  wire [256:0] zz_6;
  wire [255:0] zz_7;
  wire [255:0] zz_8;
  wire [256:0] diff;
  wire  borrowBit;
  wire [255:0] reduced;
  wire  zz_1;
  wire  zz_2;
  reg  zz_3;
  reg [255:0] zz_4;
  assign zz_5 = a;
  assign zz_6 = b;
  assign zz_7 = diff[255:0];
  assign zz_8 = (borrowBit ? (6'b100110) : (6'b000000));
  assign diff = (zz_5 - zz_6);
  assign borrowBit = diff[256];
  assign reduced = (zz_7 - zz_8);
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! zz_2)) || io_rsp_ready);
  assign zz_2 = zz_3;
  assign io_rsp_valid = zz_2;
  assign io_rsp_payload = zz_4;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_3 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_3 <= io_cmd_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_4 <= reduced;
    end
  end

endmodule


//ModularSubtract_1 remplaced by ModularSubtract

module ModularMultiplier (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [255:0] io_cmd_payload_opA,
      input  [255:0] io_cmd_payload_opB,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_4;
  wire  zz_5;
  wire  zz_6;
  wire [511:0] zz_7;
  wire  zz_8;
  wire  zz_9;
  wire [255:0] zz_10;
  wire  zz_1;
  reg  zz_2;
  reg [511:0] zz_3;
  KaratsubaMultiplier multiplier ( 
    .io_cmd_valid(io_cmd_valid),
    .io_cmd_ready(zz_5),
    .io_cmd_payload_opA(io_cmd_payload_opA),
    .io_cmd_payload_opB(io_cmd_payload_opB),
    .io_rsp_valid(zz_6),
    .io_rsp_ready(zz_4),
    .io_rsp_payload(zz_7),
    .clk(clk),
    .reset(reset) 
  );
  ModularReducer512 reducer ( 
    .io_cmd_valid(zz_1),
    .io_cmd_ready(zz_8),
    .io_cmd_payload(zz_3),
    .io_rsp_valid(zz_9),
    .io_rsp_ready(io_rsp_ready),
    .io_rsp_payload(zz_10),
    .clk(clk),
    .reset(reset) 
  );
  assign io_cmd_ready = zz_5;
  assign zz_4 = ((1'b1 && (! zz_1)) || zz_8);
  assign zz_1 = zz_2;
  assign io_rsp_valid = zz_9;
  assign io_rsp_payload = zz_10;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_2 <= 1'b0;
    end else begin
      if(zz_4)begin
        zz_2 <= zz_6;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_4)begin
      zz_3 <= zz_7;
    end
  end

endmodule


//ModularMultiplier_1 remplaced by ModularMultiplier


//ModularMultiplier_2 remplaced by ModularMultiplier


//ModularMultiplier_3 remplaced by ModularMultiplier

module ModularReducer256 (
      input   io_cmd_valid,
      output  io_cmd_ready,
      input  [255:0] io_cmd_payload,
      output  io_rsp_valid,
      input   io_rsp_ready,
      output [254:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire [254:0] zz_5;
  wire [254:0] low;
  wire  msb;
  wire [254:0] reduced;
  wire  zz_1;
  wire  zz_2;
  reg  zz_3;
  reg [254:0] zz_4;
  assign zz_5 = (msb ? (5'b10011) : (5'b00000));
  assign low = io_cmd_payload[254 : 0];
  assign msb = io_cmd_payload[255];
  assign reduced = (low + zz_5);
  assign io_cmd_ready = zz_1;
  assign zz_1 = ((1'b1 && (! zz_2)) || io_rsp_ready);
  assign zz_2 = zz_3;
  assign io_rsp_valid = zz_2;
  assign io_rsp_payload = zz_4;
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      zz_3 <= 1'b0;
    end else begin
      if(zz_1)begin
        zz_3 <= io_cmd_valid;
      end
    end
  end

  always @ (posedge clk)
  begin
    if(zz_1)begin
      zz_4 <= reduced;
    end
  end

endmodule

module Curve25519Core (
      input   io_cmd_valid,
      output reg  io_cmd_ready,
      input  [255:0] io_cmd_payload_secretKey,
      input  [255:0] io_cmd_payload_basePoint,
      input  [255:0] io_cmd_payload_lambda,
      output reg  io_rsp_valid,
      input   io_rsp_ready,
      output reg [255:0] io_rsp_payload,
      input   clk,
      input   reset);
  wire  zz_2;
  wire [255:0] zz_3;
  wire [255:0] zz_4;
  wire  zz_5;
  wire  zz_6;
  wire [255:0] zz_7;
  wire [255:0] zz_8;
  wire  zz_9;
  wire  zz_10;
  wire [255:0] zz_11;
  wire [255:0] zz_12;
  wire  zz_13;
  wire  zz_14;
  wire [255:0] zz_15;
  wire [255:0] zz_16;
  wire  zz_17;
  reg  zz_18;
  reg [255:0] zz_19;
  reg [255:0] zz_20;
  reg  zz_21;
  wire  zz_22;
  wire [255:0] zz_23;
  wire [255:0] zz_24;
  wire  zz_25;
  wire  zz_26;
  wire [255:0] zz_27;
  wire [255:0] zz_28;
  wire  zz_29;
  wire  zz_30;
  wire [255:0] zz_31;
  wire [255:0] zz_32;
  wire  zz_33;
  reg  zz_34;
  reg [255:0] zz_35;
  reg  zz_36;
  wire  zz_37;
  wire  zz_38;
  wire [255:0] zz_39;
  wire  zz_40;
  wire  zz_41;
  wire [255:0] zz_42;
  wire  zz_43;
  wire  zz_44;
  wire [255:0] zz_45;
  wire  zz_46;
  wire  zz_47;
  wire [255:0] zz_48;
  wire  zz_49;
  wire  zz_50;
  wire [255:0] zz_51;
  wire  zz_52;
  wire  zz_53;
  wire [255:0] zz_54;
  wire  zz_55;
  wire  zz_56;
  wire [255:0] zz_57;
  wire  zz_58;
  wire  zz_59;
  wire [255:0] zz_60;
  wire  zz_61;
  wire  zz_62;
  wire [254:0] zz_63;
  wire [7:0] zz_64;
  wire [256:0] zz_65;
  reg [255:0] lambdaReg;
  reg [255:0] x2;
  reg [255:0] z2;
  reg [255:0] x3;
  reg [255:0] z3;
  reg [255:0] dataReg_4;
  reg [255:0] dataReg_5;
  reg [255:0] secretKeyReg;
  reg [255:0] x1;
  wire  mainFsm_wantExit;
  reg  mainFsm_ladderCounter_willIncrement;
  reg  mainFsm_ladderCounter_willClear;
  reg [7:0] mainFsm_ladderCounter_valueNext;
  reg [7:0] mainFsm_ladderCounter_value;
  wire  mainFsm_ladderCounter_willOverflowIfInc;
  wire  mainFsm_ladderCounter_willOverflow;
  wire [255:0] mainFsm_sInit_clampedKey;
  wire [1:0] zz_1;
  wire  mainFsm_sInit_c;
  wire [255:0] mainFsm_sInit_swapMask;
  reg `mainFsm_enumDefinition_binary_sequancial_type mainFsm_stateReg;
  reg `mainFsm_enumDefinition_binary_sequancial_type mainFsm_stateNext;
  assign zz_64 = mainFsm_ladderCounter_willIncrement;
  assign zz_65 = (secretKeyReg <<< 1);
  ModularAdder sum_0 ( 
    .io_cmd_valid(zz_2),
    .io_cmd_ready(zz_37),
    .a(zz_3),
    .b(zz_4),
    .io_rsp_valid(zz_38),
    .io_rsp_ready(zz_5),
    .io_rsp_payload(zz_39),
    .clk(clk),
    .reset(reset) 
  );
  ModularAdder sum_1 ( 
    .io_cmd_valid(zz_6),
    .io_cmd_ready(zz_40),
    .a(zz_7),
    .b(zz_8),
    .io_rsp_valid(zz_41),
    .io_rsp_ready(zz_9),
    .io_rsp_payload(zz_42),
    .clk(clk),
    .reset(reset) 
  );
  ModularSubtract sub_0 ( 
    .io_cmd_valid(zz_10),
    .io_cmd_ready(zz_43),
    .a(zz_11),
    .b(zz_12),
    .io_rsp_valid(zz_44),
    .io_rsp_ready(zz_13),
    .io_rsp_payload(zz_45),
    .clk(clk),
    .reset(reset) 
  );
  ModularSubtract sub_1 ( 
    .io_cmd_valid(zz_14),
    .io_cmd_ready(zz_46),
    .a(zz_15),
    .b(zz_16),
    .io_rsp_valid(zz_47),
    .io_rsp_ready(zz_17),
    .io_rsp_payload(zz_48),
    .clk(clk),
    .reset(reset) 
  );
  ModularMultiplier mul_0 ( 
    .io_cmd_valid(zz_18),
    .io_cmd_ready(zz_49),
    .io_cmd_payload_opA(zz_19),
    .io_cmd_payload_opB(zz_20),
    .io_rsp_valid(zz_50),
    .io_rsp_ready(zz_21),
    .io_rsp_payload(zz_51),
    .clk(clk),
    .reset(reset) 
  );
  ModularMultiplier mul_1 ( 
    .io_cmd_valid(zz_22),
    .io_cmd_ready(zz_52),
    .io_cmd_payload_opA(zz_23),
    .io_cmd_payload_opB(zz_24),
    .io_rsp_valid(zz_53),
    .io_rsp_ready(zz_25),
    .io_rsp_payload(zz_54),
    .clk(clk),
    .reset(reset) 
  );
  ModularMultiplier mul_2 ( 
    .io_cmd_valid(zz_26),
    .io_cmd_ready(zz_55),
    .io_cmd_payload_opA(zz_27),
    .io_cmd_payload_opB(zz_28),
    .io_rsp_valid(zz_56),
    .io_rsp_ready(zz_29),
    .io_rsp_payload(zz_57),
    .clk(clk),
    .reset(reset) 
  );
  ModularMultiplier mul_3 ( 
    .io_cmd_valid(zz_30),
    .io_cmd_ready(zz_58),
    .io_cmd_payload_opA(zz_31),
    .io_cmd_payload_opB(zz_32),
    .io_rsp_valid(zz_59),
    .io_rsp_ready(zz_33),
    .io_rsp_payload(zz_60),
    .clk(clk),
    .reset(reset) 
  );
  ModularReducer256 reducer25519 ( 
    .io_cmd_valid(zz_34),
    .io_cmd_ready(zz_61),
    .io_cmd_payload(zz_35),
    .io_rsp_valid(zz_62),
    .io_rsp_ready(zz_36),
    .io_rsp_payload(zz_63),
    .clk(clk),
    .reset(reset) 
  );
  always @ (mainFsm_stateReg)
  begin
    io_cmd_ready = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
        io_cmd_ready = 1'b1;
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (mainFsm_stateReg)
  begin
    io_rsp_valid = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
        io_rsp_valid = 1'b1;
      end
      default : begin
      end
    endcase
  end

  always @ (mainFsm_stateReg or x1)
  begin
    io_rsp_payload = (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
        io_rsp_payload = x1;
      end
      default : begin
      end
    endcase
  end

  assign zz_2 = 1'b0;
  assign zz_6 = 1'b0;
  assign zz_10 = 1'b0;
  assign zz_14 = 1'b0;
  always @ (mainFsm_stateReg or zz_49)
  begin
    zz_18 = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
        if(zz_49)begin
          zz_18 = 1'b1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
        if(zz_49)begin
          zz_18 = 1'b1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  assign zz_22 = 1'b0;
  assign zz_26 = 1'b0;
  assign zz_30 = 1'b0;
  assign zz_5 = 1'b0;
  assign zz_9 = 1'b0;
  assign zz_13 = 1'b0;
  assign zz_17 = 1'b0;
  always @ (mainFsm_stateReg or zz_50)
  begin
    zz_21 = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
        if(zz_50)begin
          zz_21 = 1'b1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
        if(zz_50)begin
          zz_21 = 1'b1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  assign zz_25 = 1'b0;
  assign zz_29 = 1'b0;
  assign zz_33 = 1'b0;
  always @ (mainFsm_stateReg or zz_61)
  begin
    zz_34 = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
        if(zz_61)begin
          zz_34 = 1'b1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (mainFsm_stateReg or zz_62)
  begin
    zz_36 = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
        if(zz_62)begin
          zz_36 = 1'b1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  assign mainFsm_wantExit = 1'b0;
  always @ (mainFsm_stateReg)
  begin
    mainFsm_ladderCounter_willIncrement = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
        mainFsm_ladderCounter_willIncrement = 1'b1;
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (mainFsm_stateReg)
  begin
    mainFsm_ladderCounter_willClear = 1'b0;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
        mainFsm_ladderCounter_willClear = 1'b1;
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  assign mainFsm_ladderCounter_willOverflowIfInc = (mainFsm_ladderCounter_value == (8'b11111111));
  assign mainFsm_ladderCounter_willOverflow = (mainFsm_ladderCounter_willOverflowIfInc && mainFsm_ladderCounter_willIncrement);
  always @ (mainFsm_ladderCounter_value or zz_64 or mainFsm_ladderCounter_willClear)
  begin
    mainFsm_ladderCounter_valueNext = (mainFsm_ladderCounter_value + zz_64);
    if(mainFsm_ladderCounter_willClear)begin
      mainFsm_ladderCounter_valueNext = (8'b00000000);
    end
  end

  assign mainFsm_sInit_clampedKey = {{(2'b01),io_cmd_payload_secretKey[253 : 3]},(3'b000)};
  assign zz_1 = secretKeyReg[255 : 254];
  assign mainFsm_sInit_c = (zz_1[0] ^ zz_1[1]);
  assign mainFsm_sInit_swapMask = {mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,{mainFsm_sInit_c,mainFsm_sInit_c}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}};
  always @ (mainFsm_stateReg or io_cmd_valid or zz_49 or zz_50 or mainFsm_ladderCounter_valueNext or zz_61 or zz_62 or io_rsp_ready)
  begin
    mainFsm_stateNext = mainFsm_stateReg;
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
        if(io_cmd_valid)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e2;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
        if(zz_49)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e3;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
        if(zz_50)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e4;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
        mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e5;
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
        mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e6;
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
        mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e7;
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
        mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e8;
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
        if((mainFsm_ladderCounter_valueNext == (8'b11111111)))begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e9;
        end else begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e5;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
        mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e10;
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
        if(zz_49)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e11;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
        if(zz_50)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e12;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
        if(zz_61)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e13;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
        if(zz_62)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_e14;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
        if(io_rsp_ready)begin
          mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit;
        end
      end
      default : begin
        mainFsm_stateNext = `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit;
      end
    endcase
  end

  always @ (mainFsm_stateReg or zz_49 or x1 or x2)
  begin
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
        if(zz_49)begin
          zz_19 = x1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
        if(zz_49)begin
          zz_19 = x2;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (mainFsm_stateReg or zz_49 or lambdaReg or z2)
  begin
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
        if(zz_49)begin
          zz_20 = lambdaReg;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
        if(zz_49)begin
          zz_20 = z2;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (mainFsm_stateReg or zz_61 or x1)
  begin
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
        if(zz_61)begin
          zz_35 = x1;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      lambdaReg <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      x2 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      z2 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      x3 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      z3 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      dataReg_4 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      dataReg_5 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      secretKeyReg <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
      mainFsm_ladderCounter_value <= (8'b00000000);
      mainFsm_stateReg <= `mainFsm_enumDefinition_binary_sequancial_boot;
    end else begin
      mainFsm_ladderCounter_value <= mainFsm_ladderCounter_valueNext;
      mainFsm_stateReg <= mainFsm_stateNext;
      case(mainFsm_stateReg)
        `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
          secretKeyReg <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          lambdaReg <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          x2 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          z2 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          x3 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          z3 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          dataReg_4 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          dataReg_5 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          if(io_cmd_valid)begin
            secretKeyReg <= mainFsm_sInit_clampedKey;
            lambdaReg <= io_cmd_payload_lambda;
          end
        end
        `mainFsm_enumDefinition_binary_sequancial_e2 : begin
        end
        `mainFsm_enumDefinition_binary_sequancial_e3 : begin
        end
        `mainFsm_enumDefinition_binary_sequancial_e4 : begin
          x3 <= x1;
          x2 <= lambdaReg;
          z2 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
          z3 <= lambdaReg;
        end
        `mainFsm_enumDefinition_binary_sequancial_e5 : begin
          x2 <= (x2 ^ (x3 & mainFsm_sInit_swapMask));
          z2 <= (z2 ^ (z3 & mainFsm_sInit_swapMask));
        end
        `mainFsm_enumDefinition_binary_sequancial_e6 : begin
          x3 <= (x3 ^ (x2 & mainFsm_sInit_swapMask));
          z3 <= (z3 ^ (z2 & mainFsm_sInit_swapMask));
        end
        `mainFsm_enumDefinition_binary_sequancial_e7 : begin
          x2 <= (x2 ^ (x3 & mainFsm_sInit_swapMask));
          z2 <= (z2 ^ (z3 & mainFsm_sInit_swapMask));
        end
        `mainFsm_enumDefinition_binary_sequancial_e8 : begin
          secretKeyReg <= zz_65[255:0];
        end
        `mainFsm_enumDefinition_binary_sequancial_e9 : begin
        end
        `mainFsm_enumDefinition_binary_sequancial_e10 : begin
        end
        `mainFsm_enumDefinition_binary_sequancial_e11 : begin
        end
        `mainFsm_enumDefinition_binary_sequancial_e12 : begin
        end
        `mainFsm_enumDefinition_binary_sequancial_e13 : begin
        end
        `mainFsm_enumDefinition_binary_sequancial_e14 : begin
        end
        default : begin
        end
      endcase
    end
  end

  always @ (posedge clk)
  begin
    case(mainFsm_stateReg)
      `mainFsm_enumDefinition_binary_sequancial_mainFsm_sInit : begin
        x1 <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
        if(io_cmd_valid)begin
          x1 <= io_cmd_payload_basePoint;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e2 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e3 : begin
        if(zz_50)begin
          x1 <= zz_51;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e4 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e5 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e6 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e7 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e8 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e9 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e10 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e11 : begin
        if(zz_50)begin
          x1 <= zz_51;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e12 : begin
      end
      `mainFsm_enumDefinition_binary_sequancial_e13 : begin
        if(zz_62)begin
          x1 <= zz_63;
        end
      end
      `mainFsm_enumDefinition_binary_sequancial_e14 : begin
      end
      default : begin
      end
    endcase
  end

endmodule

module Curve25519Ice40 (
      output  led1,
      output  led2,
      output  led3,
      output  led4,
      output  led5,
      output  led6,
      output  led7,
      output  led8,
      input   clk,
      input   reset);
  wire  zz_1;
  wire  zz_2;
  wire  zz_3;
  wire  zz_4;
  wire [255:0] zz_5;
  wire [7:0] leds;
  wire [255:0] cmd_secretKey;
  wire [255:0] cmd_basePoint;
  wire [255:0] cmd_lambda;
  reg [255:0] result;
  Curve25519Core core ( 
    .io_cmd_valid(zz_1),
    .io_cmd_ready(zz_3),
    .io_cmd_payload_secretKey(cmd_secretKey),
    .io_cmd_payload_basePoint(cmd_basePoint),
    .io_cmd_payload_lambda(cmd_lambda),
    .io_rsp_valid(zz_4),
    .io_rsp_ready(zz_2),
    .io_rsp_payload(zz_5),
    .clk(clk),
    .reset(reset) 
  );
  assign led1 = leds[0];
  assign led2 = leds[1];
  assign led3 = leds[2];
  assign led4 = leds[3];
  assign led5 = leds[4];
  assign led6 = leds[5];
  assign led7 = leds[6];
  assign led8 = leds[7];
  assign cmd_lambda = (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001);
  assign cmd_basePoint = (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001001);
  assign cmd_secretKey = (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
  assign zz_1 = 1'b1;
  assign zz_2 = 1'b1;
  assign leds = result[7 : 0];
  always @ (posedge clk or posedge reset)
  begin
    if (reset) begin
      result <= (256'b0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);
    end else begin
      if(zz_4)begin
        result <= zz_5;
      end
    end
  end

endmodule

